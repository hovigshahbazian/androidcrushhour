﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class StageInfo : MonoBehaviour {

    public enum Collectables { None = -1 , Acorn, Conch, IceBox, WaterBottle, Tire, MoneyBag, CandyCane, Star };
    public enum Obstacles { None = -1, Car, Aligator, Deer, Icicles, Shark, Twister, GiantRock, Bush, Octopus};
    public enum Destructables { None = -1, Cone, Squirrel, Crab, Ram, Snake, SnowMan, Rock , SlotMachine, Robot};

    public Sprite[] collectablesSprites;
    public Sprite[] obstaclesSprites;
    public Sprite[] destructablesSprites;


    public Image[] collectableImages;
    public Image[] obstaclesImages;
    public Image[] destructableImages;

    public GameObject InfoUI;

    CollectableSet[] EditCollectableSet = new CollectableSet[] {
        new CollectableSet( Collectables.Tire, Collectables.None), //Highway 1.1
        new CollectableSet( Collectables.Acorn, Collectables.None), //HighWay 1.2
        new CollectableSet( Collectables.Tire, Collectables.None), //HighWay 1.3
        new CollectableSet( Collectables.Conch, Collectables.None), //Coast 2.1
        new CollectableSet( Collectables.Conch, Collectables.None), //coast 2.2
        new CollectableSet( Collectables.Conch, Collectables.None), //Coast 2.3
        new CollectableSet( Collectables.IceBox, Collectables.None), //City 3.1
        new CollectableSet( Collectables.IceBox, Collectables.None), //City 3.2
        new CollectableSet( Collectables.IceBox, Collectables.None), //City 3.3
        new CollectableSet( Collectables.Tire, Collectables.None), //Raceway 4.1
        new CollectableSet( Collectables.Tire, Collectables.None), //Raceway 4.2
        new CollectableSet( Collectables.Tire, Collectables.None), //Raceway 4.3
        new CollectableSet( Collectables.WaterBottle, Collectables.None), //canyon 5.1
        new CollectableSet( Collectables.WaterBottle, Collectables.None), //canyon 5.2
        new CollectableSet( Collectables.WaterBottle, Collectables.None), //canyon 5.3
        new CollectableSet( Collectables.WaterBottle, Collectables.None), //desert 6.1
        new CollectableSet( Collectables.WaterBottle, Collectables.None), //desert 6.2
        new CollectableSet( Collectables.WaterBottle, Collectables.None), //desert 6.3
        new CollectableSet( Collectables.Acorn, Collectables.None), //Alpine 7.1
        new CollectableSet( Collectables.Acorn, Collectables.None), //Alpine 7.1
        new CollectableSet( Collectables.Acorn, Collectables.None), //Alpine 7.1
        new CollectableSet( Collectables.WaterBottle, Collectables.None), //route 8.1
        new CollectableSet( Collectables.WaterBottle, Collectables.None), //route 8.2
        new CollectableSet( Collectables.WaterBottle, Collectables.None), //route 8.3
        new CollectableSet( Collectables.MoneyBag, Collectables.None), //vegas 9.1
        new CollectableSet( Collectables.MoneyBag, Collectables.None), //vegas 9.2
        new CollectableSet( Collectables.MoneyBag, Collectables.None), //vegas 9.3
        new CollectableSet( Collectables.Conch, Collectables.None), //atlantic 10.1
        new CollectableSet( Collectables.Conch, Collectables.None), //atlantic 10.2
        new CollectableSet( Collectables.Conch, Collectables.None), //atlantic 10.3
        new CollectableSet( Collectables.CandyCane, Collectables.None), //winter 11.1
        new CollectableSet( Collectables.CandyCane, Collectables.None), //winter 11.2
        new CollectableSet( Collectables.CandyCane, Collectables.None), //winter 11.3
        new CollectableSet( Collectables.Star, Collectables.None), //star Road 12.1
        new CollectableSet( Collectables.Star, Collectables.None), //star Road 12.2
        new CollectableSet( Collectables.Star, Collectables.None), //star Road 12.3
        new CollectableSet( Collectables.Conch, Collectables.None), //Atlantis 13.1
         new CollectableSet( Collectables.Conch, Collectables.None), //Atlantis 13.2
          new CollectableSet( Collectables.Conch, Collectables.None), //Atlantis 13.3
        new CollectableSet( Collectables.None, Collectables.None), //WipeOut 14.1
        new CollectableSet( Collectables.None, Collectables.None), //WipeOut 14.2
        new CollectableSet( Collectables.None, Collectables.None) //WipeOut 14.3
    };

    ObstacleSet[] EditObstacleSet = new ObstacleSet[] {
        new ObstacleSet( Obstacles.Car, Obstacles.None), //Highway 1.1
        new ObstacleSet( Obstacles.Car, Obstacles.Bush), //HighWay 1.2
        new ObstacleSet( Obstacles.Car, Obstacles.None), //HighWay 1.3
        new ObstacleSet( Obstacles.Car, Obstacles.None), //Coast 2.1
        new ObstacleSet( Obstacles.None, Obstacles.None), //coast 2.2
        new ObstacleSet( Obstacles.None, Obstacles.None), //Coast 2.3
        new ObstacleSet( Obstacles.Car, Obstacles.Aligator), //City 3.1
        new ObstacleSet( Obstacles.Car, Obstacles.Aligator), //City 3.2
        new ObstacleSet( Obstacles.Car, Obstacles.Aligator), //City 3.3
        new ObstacleSet( Obstacles.Car, Obstacles.None), //Raceway 4.1
        new ObstacleSet( Obstacles.Car, Obstacles.None), //Raceway 4.2
        new ObstacleSet( Obstacles.Car, Obstacles.None), //Raceway 4.3
        new ObstacleSet( Obstacles.Car, Obstacles.Twister), //canyon 5.1
        new ObstacleSet( Obstacles.Car, Obstacles.Twister), //canyon 5.2
        new ObstacleSet( Obstacles.Car, Obstacles.Twister), //canyon 5.3
        new ObstacleSet( Obstacles.Car, Obstacles.None), //desert 6.1
        new ObstacleSet( Obstacles.Car, Obstacles.None), //desert 6.2
        new ObstacleSet( Obstacles.Car, Obstacles.None), //desert 6.3
        new ObstacleSet( Obstacles.Car, Obstacles.Deer), //Alpine 7.1
        new ObstacleSet( Obstacles.Car, Obstacles.Deer), //Alpine 7.2
        new ObstacleSet( Obstacles.Car, Obstacles.Deer), //Alpine 7.3
        new ObstacleSet( Obstacles.Car, Obstacles.GiantRock), //route 8.1
        new ObstacleSet( Obstacles.Car, Obstacles.None), //route 8.2
        new ObstacleSet( Obstacles.Car, Obstacles.None), //route 8.3
        new ObstacleSet( Obstacles.Car, Obstacles.None), //vegas 9.1
        new ObstacleSet( Obstacles.Car, Obstacles.None), //vegas 9.2
        new ObstacleSet( Obstacles.Car, Obstacles.None), //vegas 9.3
        new ObstacleSet( Obstacles.Octopus, Obstacles.Shark), //atlantic 10.1
        new ObstacleSet( Obstacles.Octopus, Obstacles.Shark), //atlantic 10.2
        new ObstacleSet( Obstacles.Octopus, Obstacles.Shark), //atlantic 10.3
        new ObstacleSet( Obstacles.Deer, Obstacles.Icicles), //winter 11.1
        new ObstacleSet( Obstacles.Deer, Obstacles.Icicles), //winter 11.2
        new ObstacleSet( Obstacles.Deer, Obstacles.Icicles), //winter 11.3
        new ObstacleSet( Obstacles.Car, Obstacles.GiantRock), //star Road 12.1
        new ObstacleSet( Obstacles.Car, Obstacles.GiantRock), //star Road 12.2
        new ObstacleSet( Obstacles.Car, Obstacles.GiantRock), //star Road 12.3
        new ObstacleSet( Obstacles.Octopus, Obstacles.Shark), //Atlantis 13.1
        new ObstacleSet( Obstacles.Octopus, Obstacles.Shark), //Atlantis 13.2
        new ObstacleSet( Obstacles.Octopus, Obstacles.Shark), //Atlantis 13.3
        new ObstacleSet( Obstacles.Car, Obstacles.None), //WipeOut 14.1
        new ObstacleSet( Obstacles.Car, Obstacles.None), //WipeOut 14.2
        new ObstacleSet( Obstacles.Car, Obstacles.None), //WipeOut 14.3


    };

    DestructableSet[] EditDestructableSet =  new DestructableSet[] {

         new DestructableSet(Destructables.Cone, Destructables.None), //Highway 1.1
        new DestructableSet( Destructables.Squirrel, Destructables.None), //HighWay 1.2
        new DestructableSet( Destructables.Cone, Destructables.None), //HighWay 1.3
        new DestructableSet( Destructables.Crab, Destructables.None), //Coast 2.1
        new DestructableSet( Destructables.Crab, Destructables.None), //coast 2.2
        new DestructableSet( Destructables.Crab, Destructables.None), //Coast 2.3
        new DestructableSet( Destructables.Snake, Destructables.None), //City 3.1
        new DestructableSet( Destructables.Snake, Destructables.None), //City 3.2
        new DestructableSet( Destructables.Snake, Destructables.None), //City 3.3
        new DestructableSet( Destructables.Cone, Destructables.None), //Raceway 4.1
        new DestructableSet( Destructables.Cone, Destructables.None), //Raceway 4.2
        new DestructableSet( Destructables.Cone, Destructables.None), //Raceway 4.3
        new DestructableSet( Destructables.Ram, Destructables.None), //canyon 5.1
        new DestructableSet( Destructables.Ram, Destructables.None), //canyon 5.2
        new DestructableSet( Destructables.Ram, Destructables.None), //canyon 5.3
        new DestructableSet( Destructables.Snake, Destructables.None), //desert 6.1
         new DestructableSet( Destructables.Snake, Destructables.None), //desert 6.2
          new DestructableSet( Destructables.Snake, Destructables.None), //desert 6.3
        new DestructableSet( Destructables.SnowMan, Destructables.None), //Alpine 7.1
        new DestructableSet( Destructables.SnowMan, Destructables.None), //Alpine 7.2
        new DestructableSet( Destructables.SnowMan, Destructables.None), //Alpine 7.3
        new DestructableSet( Destructables.Ram, Destructables.None), //route 8.1
        new DestructableSet( Destructables.Ram, Destructables.None), //route 8.2
        new DestructableSet( Destructables.Ram, Destructables.None), //route 8.3
        new DestructableSet( Destructables.SlotMachine, Destructables.None), //vegas 9.1
         new DestructableSet( Destructables.SlotMachine, Destructables.None), //vegas 9.2
          new DestructableSet( Destructables.SlotMachine, Destructables.None), //vegas 9.3
         new DestructableSet( Destructables.Crab, Destructables.None), //atlantic 10.1
         new DestructableSet( Destructables.Crab, Destructables.None), //atlantic 10.2
         new DestructableSet( Destructables.Crab, Destructables.None), //atlantic 10.3
        new DestructableSet( Destructables.SnowMan, Destructables.None), //winter 11.1
        new DestructableSet( Destructables.SnowMan, Destructables.None), //winter 11.2
        new DestructableSet( Destructables.SnowMan, Destructables.None), //winter 11.3
        new DestructableSet( Destructables.Rock, Destructables.None), //star Road 12.1
        new DestructableSet( Destructables.Rock, Destructables.None), //star Road 12.2
        new DestructableSet( Destructables.Rock, Destructables.None), //star Road 12.3
        new DestructableSet( Destructables.Crab, Destructables.None), //Atlantis 13.1
        new DestructableSet( Destructables.Crab, Destructables.None), //Atlantis 13.2
        new DestructableSet( Destructables.Crab, Destructables.None), //Atlantis 13.3
        new DestructableSet( Destructables.Cone, Destructables.None), //WipeOut 14.1
        new DestructableSet( Destructables.Cone, Destructables.None), //WipeOut 14.2
        new DestructableSet( Destructables.Cone, Destructables.None), //WipeOut 14.3
    };



    [System.Serializable]
    public struct CollectableSet
    {
       public Collectables first;
       public Collectables second;

        public CollectableSet(Collectables f, Collectables s)
        {
            first = f;
            second = s;
        }

      
    }
    [System.Serializable]
    public struct ObstacleSet
    {
       public Obstacles first;
       public Obstacles second;

        public ObstacleSet(Obstacles f, Obstacles s)
        {
            first = f;
            second = s;
        }

    }
    [System.Serializable]
    public struct DestructableSet
    {
      public Destructables first;
      public Destructables second;

        public DestructableSet(Destructables f, Destructables s)
        {
            first = f;
            second = s;
        }
    }

    public struct StageSet
    {
        public CollectableSet collectableSet;
        public ObstacleSet obstacleSet;
        public  DestructableSet destructableSet;


        public StageSet(CollectableSet cSet, ObstacleSet oSet, DestructableSet dSet )
        {

            collectableSet.first = cSet.first;
            collectableSet.second = cSet.second;

            obstacleSet.first = oSet.first;
            obstacleSet.second = oSet.second;

            destructableSet.first = dSet.first;
            destructableSet.second = dSet.second;

        }
            


        public void SetCollectable(CollectableSet otherCollectableSet)
        {

            collectableSet.first = otherCollectableSet.first;
            collectableSet.second = otherCollectableSet.second;
        }

        public void SetObstacles(ObstacleSet otherObstacleSet )
        {

            obstacleSet = otherObstacleSet;
        }

        public void SetDestructables(DestructableSet otherDestructableSet)
        {

            destructableSet = otherDestructableSet;
        }


    }


    public Dictionary<Stage, StageSet> StageDataMap;


    // Use this for initialization
    void Start () {


        StageDataMap = new Dictionary<Stage, StageSet>();

        int i = 0;
        foreach ( Stage s in GameDataManager.instance.LevelList)
        {
            StageDataMap.Add(s, new StageSet(EditCollectableSet[i],EditObstacleSet[i],EditDestructableSet[i]));
            i++;
        }

        InfoUI.SetActive(false);
        Debug.Log("Close uo");

    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log(StageDataMap[Stage.HighWay_1].collectableSet.first);
        Debug.Log(StageDataMap[Stage.HighWay_1].collectableSet.second);
    }


    public void RefreshStageInfo(Stage s)
    {
        Debug.Log("Refreshmenu");
        // collectableImages[0].color = Color.white;

        //set collectable images
        Debug.Log(s);
        Debug.Log(StageDataMap[s].collectableSet.first);
        Debug.Log(StageDataMap[s].collectableSet.second);


        if (StageDataMap[s].collectableSet.first == Collectables.None)
        {
            collectableImages[0].color = Color.clear;
        }
        else
        {
            collectableImages[0].sprite = collectablesSprites[(int)StageDataMap[s].collectableSet.first];
            collectableImages[0].color = Color.white;
        }

       
        if (StageDataMap[s].collectableSet.second == Collectables.None)
        {
            collectableImages[1].color = Color.clear;
        }
        else
        {
            collectableImages[1].color = Color.white;
            collectableImages[1].sprite = collectablesSprites[(int)StageDataMap[s].collectableSet.second];
        }


        //set Obstacle images
        if (StageDataMap[s].obstacleSet.first == Obstacles.None)
        {
            obstaclesImages[0].color = Color.clear;
        }
        else
        {
            obstaclesImages[0].color = Color.white;
            obstaclesImages[0].sprite = obstaclesSprites[((int)StageDataMap[s].obstacleSet.first)];
        }


        if (StageDataMap[s].obstacleSet.second  == Obstacles.None)
        {
            obstaclesImages[1].color = Color.clear;
        }
        else
        {
            obstaclesImages[1].color = Color.white;
            obstaclesImages[1].sprite = obstaclesSprites[(int)StageDataMap[s].obstacleSet.second];
        }


        //set destructable images
        if (StageDataMap[s].destructableSet.first == Destructables.None)
        {
            destructableImages[0].color = Color.clear;
        }
        else
        {
            destructableImages[0].color = Color.white;
            destructableImages[0].sprite = destructablesSprites[(int)StageDataMap[s].destructableSet.first];
        }


        if (StageDataMap[s].destructableSet.second == Destructables.None)
        {
            destructableImages[1].color = Color.clear;
        }
        else
        {
            destructableImages[1].color = Color.white;
            destructableImages[1].sprite = destructablesSprites[(int)StageDataMap[s].destructableSet.second];
        }
        InfoUI.SetActive(true);
    }


    public void OpenUI()
    {
        InfoUI.SetActive(true);
    }

    public void CloseUI()
    {
        InfoUI.SetActive(false);
    }

}
