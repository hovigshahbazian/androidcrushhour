﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {

    public GameObject obstacle;
    public Vector3 SpawnPoint;
    public float SpawnTimer = 7;
    public float SpawnTimerDif = 5;
    [SerializeField]
    bool RandomTime = false;
	// Use this for initialization
	void Start () {

        if (RandomTime)
        {
            SpawnTimer = Random.Range(SpawnTimer, SpawnTimer + SpawnTimerDif);
        }

        InvokeRepeating("Spawn", SpawnTimer, SpawnTimer);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Spawn()
    {
        obstacle.SetActive(false);
        obstacle.transform.SetPositionAndRotation(this.transform.position, Quaternion.identity);
        obstacle.SetActive(true);
    }

    public void ResetPosition()
    {
       
    }
    
}
