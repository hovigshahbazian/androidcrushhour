﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour {

	//public static ObjectPool instance;

	public GameObject pooledObject;
	public int pooledAmount = 20;
	public bool willGrow = true;
	public bool parentedObjects = false;
	 List<GameObject> pooledObjects;


	void Awake(){
		//instance = this;
	}
		
	// Use this for initialization
	void Start () {

		pooledObjects = new List<GameObject> ();
		for (int i = 0; i < pooledAmount; i++) {
			GameObject obj = (GameObject)Instantiate (pooledObject);
			obj.SetActive (false);
			pooledObjects.Add (obj);

			if (parentedObjects) {
				obj.transform.parent = this.transform;
			}
		}
	}


	public GameObject GetPooledObject(){

		for (int i = 0; i < pooledObjects.Count; i++) {

			//Debug.Log (pooledObjects [i].name );

			if (!pooledObjects [i].activeInHierarchy) {
			//	Debug.Log (pooledObjects [i].name + " Spawning");
				return pooledObjects [i];
			} 
		}

		if (willGrow) {
			GameObject obj = (GameObject)Instantiate (pooledObject);
			pooledObjects.Add (obj);
			if (parentedObjects) {
				obj.transform.parent = this.transform;
			}
			return obj;
		}


		return null;
	}





}
