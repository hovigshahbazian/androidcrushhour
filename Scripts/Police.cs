﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Police : MonoBehaviour {


    public Chaser chaser;
    public string EventName;
    private Collider col;
	// Use this for initialization
	void Start () {
        EventManager.StartListening(EventName,ActivatePoliceChaser);
       col =  GetComponent<Collider>();
        col.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void ActivatePoliceChaser()
    {
        chaser.Active = true;
        col.enabled = true;
    }


    public void OnTriggerEnter(Collider col)
    {
        
        if (col.CompareTag("Player"))
        {
            col.GetComponentInParent<PlayerHealthController>().Crash();
        }
    }
}
