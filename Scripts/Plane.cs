using System;
using UnityEngine;

public class Plane : MonoBehaviour
{

    private Renderer m_Renderer;

	public int timer;

	public Vector3 temp;

	static public float zval;
	static public float zvaladd;



    private void Start()
    {
        m_Renderer = GetComponent<Renderer>();
        m_Renderer.enabled = true;

		zval=0;
		zvaladd=0;
    }


    private void Update()
    {
		temp=transform.position;

		//print("zval "+zval);
		if (zval > 150) {
			zvaladd+=zval;
			temp.z+=zval;

			transform.position=temp;
		}
    }
}

