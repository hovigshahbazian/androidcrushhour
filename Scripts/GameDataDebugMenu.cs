﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDataDebugMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ClearAllData()
    {
        GameDataManager.instance.ErasePlayerData();
        GameDataManager.instance.EraseAllLevelData();
        GameDataManager.instance.Save();
    }

    public void UnlockAllLevels()
    {
        GameDataManager.instance.playerData.Stars += 100;  
        GameDataManager.instance.Save();
    }


}
