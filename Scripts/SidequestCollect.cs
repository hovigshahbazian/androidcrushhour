﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SidequestCollect : MonoBehaviour {
    public string CollectableName;
    public string CollectEvent;
    public string CompleteEvent;
    public int CollectCounter;
    public int CollectGoal;
    
    public bool Complete;
  
	// Use this for initialization
	void Start () {
        

        if(CollectEvent != "")
            EventManager.StartListening(CollectEvent, AddCollectable);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddCollectable()
    {
        if (!Complete) {
            CollectCounter++;
            if (CollectCounter >= CollectGoal)
            {
                Complete = true;
                if (CollectEvent != "") {
                    EventManager.TriggerEvent(CompleteEvent);
                }
            }
        }
    }


   
}
