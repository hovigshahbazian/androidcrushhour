﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSizer : MonoBehaviour {

    public float minSize = 1;
    public float maxSize = 2;

    public void ChangeScaleUniformly()
    {
        float newSize = Random.Range(minSize, maxSize);
        transform.localScale = new Vector3(newSize, newSize, newSize);
    }
}
