﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleRacer : MonoBehaviour {

    [HideInInspector]
    public VehicleObstacle vehicle;

    public float Timer;
    public bool ChangingLane = false;
 
    public bool Boosting;
    public float VisionRange;
    private SplineWalker playerWalker;
    void Start()
    {
        vehicle = GetComponent<VehicleObstacle>();
        playerWalker = GameObject.FindGameObjectWithTag("PlayerLaneWalker").GetComponent<SplineWalker>();
        if (vehicle.curAI == AIState.Racer)
        {
            EventManager.StartListening("StartLevel", EnterRacerAI);
        }
    }

    void Update()
    {
       

    }

    public void CheckRubberBandingAI()
    {
        if(vehicle.walker.progress > playerWalker.progress + 0.05f)
        {
            vehicle.SlowDown(5f, 5f);
            Debug.Log("SlowingDown");
        }
        else if(vehicle.walker.progress < playerWalker.progress - 0.05f)
        {
            vehicle.SpeedUp(5f,5f);
            Debug.Log("SpeedingUp");
        }
        
    }

    void FixedUpdate()
    {
        Vector3 forward = transform.forward;

        //Debug.DrawRay(transform.position + Vector3.up * 0.5f, forward*VisionRange, Color.red,0.001f);
        RaycastHit raycastHitInfo;
        if (Physics.Raycast(transform.position + Vector3.up * 0.5f, forward, out raycastHitInfo, VisionRange))
        {
           // Debug.Log(gameObject.name + " Touching" + raycastHitInfo.collider.gameObject.name);
            if (raycastHitInfo.collider.transform.CompareTag("Player"))
                {
                    BackDraftSpeedUp();
                   // Debug.Log("Speeding");
                }
            else
            {
              //  Debug.Log("Not Hitting");
            }
          
        }
    }


    public void StartLaneChanging()
    {
        if (!ChangingLane)
        {
            ChangingLane = true;
            Debug.Log("Changing Lane");
            float lane = Random.Range(-5f, 5f);
            if (lane > vehicle.RelativeX)
            {
               
                StartCoroutine(ChangeLaneRight(1f));

            }
            else if (lane < vehicle.RelativeX)
            {
           
                StartCoroutine(ChangeLaneLeft(1f));
            }
        }

        CheckRubberBandingAI();

    }

    public void EnterRacerAI()
    {
          InvokeRepeating("StartLaneChanging", 0, Timer);
         
    }


    void OnCollisionEnter(Collision col)
    {

        if (col.gameObject.CompareTag("Racer"))
        {
            VehicleRacer otherRacer = col.gameObject.GetComponent<VehicleRacer>();
          //  Debug.Log(col.gameObject.name);
           // Vector3 contact = transform.InverseTransformPoint(col.contacts[0].point);


            float bumpforce = 10f;
            float otherbumpforce = 10f;

            if (ChangingLane && otherRacer.ChangingLane)
            {

                StopChangingLane();
           
            }
            else if(!ChangingLane && otherRacer.ChangingLane)
            {
                bumpforce = 20f;
                otherbumpforce = 10f;
                otherRacer.StopChangingLane();
            }



            if (vehicle.RelativeX > otherRacer.vehicle.RelativeX)
            {
                otherRacer.vehicle.CarBumpLeftward(otherbumpforce);
                vehicle.CarBumpRightward(bumpforce);
            }
            else if (vehicle.RelativeX < otherRacer.vehicle.RelativeX)
            {
                otherRacer.vehicle.CarBumpRightward(otherbumpforce);
                vehicle.CarBumpLeftward(bumpforce);
            }

        }

    }

    public void StopChangingLane()
    {

        vehicle.pullOverSpeed = 0;
        ChangingLane = false;

    }


    public void BackDraftSpeedUp()
    {
        if (!Boosting)
            StartCoroutine(SpeedUp());
    }

    IEnumerator SpeedUp()
    {
        Boosting = true;
        vehicle.walker.Accleration += 0.05f;
        vehicle.walker.MaximumVelocity += 5;
        yield return new WaitForSeconds(5f);
        vehicle.walker.MaximumVelocity -= 5;
        vehicle.walker.Accleration -= 0.05f;
        Boosting = false;
    }


    IEnumerator ChangeLaneRight(float speed)
    {
        //ChangingLane = true;
        vehicle.pullOverSpeed += speed;
        yield return new WaitForSeconds(2f);
        StopChangingLane();
    }

    IEnumerator ChangeLaneLeft(float speed)
    {
        //ChangingLane = true;
        vehicle.pullOverSpeed -= speed;
        yield return new WaitForSeconds(2f);
        StopChangingLane();
    }
}
