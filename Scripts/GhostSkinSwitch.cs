﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostSkinSwitch : MonoBehaviour {

    public bool Active;
    public Material GhostSkin;
    private Material originalMat;
    public MeshRenderer renderer;

	// Use this for initialization
	void Start () {


        EventManager.StartListening("GhostGrab", GhostSkinOn);
        EventManager.StartListening("GhostEnd", GhostSkinOff);
        EventManager.StartListening("StopCurrentPowerUp", GhostSkinOff);

	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void GhostSkinOn()
    {
        if (!Active)
        {
            Debug.Log("Starting Ghost");
            PlayerCarController playerController = GetComponentInParent<PlayerCarController>();
            playerController.ResetPowerState();
            playerController.CollisionEventsEnabled = false;
            playerController.CurrentPowerUp = PlayerCarController.PowerUps.Ghost;
            Active = true;
            originalMat = renderer.material;
            renderer.material = GhostSkin;
        }
    }

    public void GhostSkinOff()
    {
        if (Active)
        {
            Debug.Log("Ending Ghost");
            PlayerCarController playerController = GetComponentInParent<PlayerCarController>();
            playerController.CollisionEventsEnabled = true;
            playerController.CurrentPowerUp = PlayerCarController.PowerUps.None;
            Active = false;
            renderer.material = originalMat;
        }
    }
}
