﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CarControlOptions : MonoBehaviour {

    private Dropdown dropdown;
    public static int controlType;
	// Use this for initialization
	void Start () {
        dropdown = GetComponent<Dropdown>();


        dropdown.onValueChanged.AddListener(ChangeControl);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void AccOn(bool accelState)
    {
        GM.accelerometer = accelState;
        SteeringWheel.SteeringWheelDisplay(!accelState);
    }

    
    public void ChangeControl(int Type)
    {
        controlType = Type;
        switch (controlType)
        {
            case 0:
                AccOn(true);
                
                break;
            case 1:
                AccOn(false);
                break;
        }

        SaveData();
    }


    static public void SaveData()
    {
        PlayerPrefs.SetInt("ControlType", controlType);
        Debug.Log("DataSaved Cotrol Type " + controlType);
    }

    static public void LoadData()
    {
        controlType = PlayerPrefs.GetInt("ControlType");

        Debug.Log("Data Loaded Cotrol Type" + controlType);
    
        switch (controlType)
        {
            case 1:
                AccOn(true);
                break;
            case 2:
                AccOn(false);
                break;
        }
    }

}
