﻿using UnityEngine;
using System.Collections;

public class WaveMovement : MonoBehaviour {

    [SerializeField]
	private float dist;
    [SerializeField]
	private float fequency;
	public float magnitude;
	private Vector3 axis;
	private Vector3 pos;
    private Transform myTransform;
	private float time = 0;

    public bool UseYAxis = false;
    public bool UseZAxis = false;


	// Use this for initialization
	void Start () {
        dist = Random.Range(0.1f, 1f);
        fequency = Random.Range(0.5f, 1f);
      

		time = dist;
        myTransform = transform;
		pos = myTransform.position;


        if (UseYAxis)
        {
            axis = myTransform.up;
        }
        else if (UseZAxis)
        {
            axis = myTransform.forward;
        }
        else
        {
           axis = myTransform.right;
        }
        



	}

	// Update is called once per frame
	void FixedUpdate () {		
		time += Time.deltaTime;		
		myTransform.position = pos + axis * Mathf.Sin (time * fequency) * magnitude;
	}


	

}
