﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerActiveBehaviour : MonoBehaviour
{
    public MonoBehaviour behavior;
    public bool ActiveState;
    public bool UseTag;
    public string Tag;
   
 

    public void OnTriggerEnter(Collider other)
    {
        if (UseTag)
        {
            if (other.CompareTag(Tag))
            {
                behavior.enabled = ActiveState;
            }
        }else
        {
            behavior.enabled = ActiveState;
        }
      
    }
}
