using UnityEngine;
using System.Collections;	 
using UnityEngine.UI;

public class Score : MonoBehaviour {

	public enum ScoreType { CrushedCar, DistanceTraveled, Diamonds, Score }

	private Text scoret;
    //public LevelGameData levelData;
    public Stage levelIndex;
	public ScoreType scoretype;
    private RoadRageController rrc;
    private TreasureCollector tsc;
    public Distance distanceUI;


	// Use this for initialization
	void Start () {
        rrc = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<RoadRageController>();
        tsc = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<TreasureCollector>();

        scoret = gameObject.GetComponent<Text>(); 
	}


	void Update () { 

		UpdateScoreText ();
		UpdateData ();
	}

	public void UpdateScoreText(){
		
		switch(scoretype){
			case ScoreType.CrushedCar:
				scoret.text = rrc.RoadRageCounter.ToString ();
				break;
		case ScoreType.DistanceTraveled:
			scoret.text = (distanceUI.distance).ToString ("F0");
				break;
			case ScoreType.Diamonds:
				scoret.text = tsc.TreasureScore.ToString();
				break;
			case ScoreType.Score:
				//scoret.text = GM.score.ToString("###,###");
				break;
			default:
				break;
		}
    }
	public void UpdateData(){

		switch (scoretype) {

		case ScoreType.CrushedCar:
			if (rrc.RoadRageCounter > GameDataManager.instance.loadedLevelDataMap[levelIndex].RoadRageScore) {
                    GameDataManager.instance.loadedLevelDataMap[levelIndex].RoadRageScore = (int)rrc.RoadRageCounter;
			}
			break;
		case ScoreType.DistanceTraveled:
			if (distanceUI.distance > GameDataManager.instance.loadedLevelDataMap[levelIndex].DistanceRecord) {
                    GameDataManager.instance.loadedLevelDataMap[levelIndex].DistanceRecord = distanceUI.distance;
			}
			break;
		//case ScoreType.Diamonds:
			//if (GM.Diamonds > GameDataManager.instance.loadedLevelData[(int)levelIndex].Diamonds) {
           //         GameDataManager.instance.loadedLevelData[(int)levelIndex].Diamonds = GM.Diamonds;
			//}
	
			//break;
		/*case ScoreType.Score:
			if (GM.score > GameDataManager.instance.loadedLevelData[(int)levelIndex].Score) {
                    GameDataManager.instance.loadedLevelData[(int)levelIndex].Score = GM.score;
			}
			break;*/
		default:
			break;
		}

	}


		
}
