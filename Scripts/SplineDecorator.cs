﻿using UnityEngine;
//using UnityEditor;

public class SplineDecorator : MonoBehaviour {

	private BezierSpline spline;
    public GameObject Lane;
	public int frequency;
    public Vector3 offset;
    [Range(0,1)]
    public float minRange = 0;
    [Range(0,1)]
    public float maxRange = 1;
    [Range(0, 1)]
    public float customOffset = 0;
	public bool lookForward;
    public bool lookLeft;
    public bool lookRight;
    public bool lookDownward;
    public bool RandomPlacement;
    public bool RandomRotation;
    public bool RandomObjects;
    public float RandomSpawnRange;
    public float RandomRotateRange;
	public GameObject[] items;
    public bool ConstructOnWake = true;
	private void Awake () {

        if (ConstructOnWake)
        {
            ConstructDecoration();
        }

	}
    public void ClearChildren()
    {
     
       foreach (Transform child in transform)
       {
            DestroyImmediate(child.gameObject);
            
        }
        
    }

    public void ConstructDecoration()
    {

        spline = Lane.GetComponent<BezierSpline>();

        if (frequency <= 0 || items == null || items.Length == 0)
        {
            return;
        }
        float stepSize = frequency * items.Length;
        if (spline.Loop || stepSize == 1)
        {
            stepSize = 1f / stepSize;
        }
        else
        {
            stepSize = 1f / (stepSize - 1);
        }
        for (int p = 0, f = 0; f < frequency; f++)
        {
            for (int i = 0; i < items.Length; i++, p++)
            {

                float location = (p * stepSize) ;



                if (location >= minRange && location <= maxRange)
                {
                    int temp = i;
                    if (RandomObjects)
                    {
                        temp = Random.Range(0, items.Length);
                    }
                   

                   GameObject item = Instantiate(items[temp]) as GameObject;
                    //GameObject item = PrefabUtility.InstantiatePrefab(items[temp]) as GameObject;



                    Vector3 position = spline.GetPoint((p + customOffset) * stepSize);
                    item.transform.localPosition = position;

                    if (lookForward)
                    {
                        item.transform.LookAt(position + spline.GetDirection(p * stepSize));
                    }

                    
                    Vector3 newOffset;

                    if (offset.x >= 0)
                    {

                         newOffset = Vector3.Cross(Vector3.up, spline.GetDirection(p * stepSize));
                    }
                    else
                    {
                        newOffset = Vector3.Cross(Vector3.down, spline.GetDirection(p * stepSize));
                    }

                    newOffset *= offset.magnitude;

                    newOffset +=  new Vector3(0f, offset.y,0f);

                    newOffset += spline.GetDirection(p * stepSize) * offset.z;

                    item.transform.Translate(newOffset, item.transform);






                    if (lookDownward)
                    {
                        item.transform.Rotate(Vector3.up, 180f, Space.Self);
                    }
                    else if (lookLeft)
                    {
                        item.transform.Rotate(Vector3.up, 90f, Space.Self);
                    }
                    else if (lookRight)
                    {
                        item.transform.Rotate(Vector3.up, -90f, Space.Self);
                    }

                    


                    if (RandomPlacement)
                        item.transform.Translate(new Vector3(Random.Range(-RandomSpawnRange, RandomSpawnRange), 0f, Random.Range(-RandomSpawnRange, RandomSpawnRange)));

                    if (RandomRotation)
                        item.transform.Rotate(Vector3.up, Random.Range(-RandomRotateRange, RandomRotateRange));

                    item.transform.parent = transform;
                }
            }
        }
    }

}