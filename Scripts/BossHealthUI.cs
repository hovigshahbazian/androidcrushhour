﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthUI : MonoBehaviour {


    public VehicleHealth vehicleHealth;

    private Text text;

    private bool setUp = false;

    public Image healthImage;

    public Text numberText;
    // Use this for initialization
    void Awake()
    {
      
    }


    private void Start()
    {
        SetUpUI();
        text = GetComponent<Text>();
        // Debug.Log("PlayerSpawned setting  health UI");


        EventManager.StartListening("BossDamaged", UpdateUI);

        UpdateUI();
    }

    // Update is called once per frame
    void Update()
    {



        if (setUp || vehicleHealth != null)
        {
            text.text = "Health: " + vehicleHealth.HitPoints.ToString();

        }

        if (!setUp) { 
            GameObject bossCar = GameObject.FindGameObjectWithTag("Boss");
            vehicleHealth = bossCar.GetComponent<VehicleHealth>();
            Debug.Log(setUp);
            Debug.Log(bossCar);
            Debug.Log(vehicleHealth);
        }


    }


    void SetUpUI()
    {
        GameObject bossCar = GameObject.FindGameObjectWithTag("Boss");
        vehicleHealth = bossCar.GetComponent<VehicleHealth>();

        if(vehicleHealth != null)
        {
            setUp = true;
        }

        Debug.Log(setUp);
        Debug.Log(bossCar);
        Debug.Log(vehicleHealth);
    }

    public void UpdateUI()
    {
        // healthImage.sprite = Healthsprites[vehicleHealth.HitPoints];
        if (vehicleHealth != null)
        {
            float temp = (float)vehicleHealth.HitPoints / (float)vehicleHealth.MaxHealth;
            numberText.text = vehicleHealth.HitPoints + "/" + vehicleHealth.MaxHealth;
            healthImage.fillAmount = temp;
        }
    }
}
