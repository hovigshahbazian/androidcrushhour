using System.Collections;
using UnityEngine;

public class Pup : MonoBehaviour
{
    public float RoadRageTime = 15;
    public Renderer render;
    public Collider col;

    public void OnEnable()
    {
       render.enabled = true;
       col.enabled = true;
    }

   

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player")) 
        {      
           StartCoroutine(PickUp());
        }
    }

    IEnumerator PickUp()
    {
        render.enabled = false;
        col.enabled = false;

        EventManager.TriggerEvent("RoadRageGrab");
       
        yield return new WaitForSeconds(RoadRageTime);
      
        EventManager.TriggerEvent("RoadRageEnd");
        
        gameObject.SetActive(false);

    }

   

 

}

