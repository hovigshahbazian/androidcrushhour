﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour {

    public float Range;
    public Vector3 forward;
    public Vector3 position;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        forward = transform.forward;
        position = transform.position;
        Debug.DrawLine(position + (Vector3.up * 0.5f), position + (forward) + (Vector3.up * 0.5f), Color.red);
    }


    void FixedUpdate()
    {
        RaycastHit raycastHitInfo;
        if (Physics.Raycast(position + (Vector3.up * 0.5f), position + (forward * Range) + (Vector3.up * 0.5f), out raycastHitInfo, Range))
        {
            
        }
    }
}
