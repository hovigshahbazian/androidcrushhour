﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class StageStats : MonoBehaviour {


   
    public Image[] SurvivalStarsUI;
    public Image[] TreasureStarsUI;
    public Image[] DestructionStarsUI;


    public GameObject StatsUI;


    // Use this for initialization
    void Start () {
        CloseUI();
	}
	
	


    public void RefreshStarRanks(Stage stage)
    {
        switch (GameDataManager.instance.loadedLevelDataMap[stage].DestructionRank)
        {
            case Rank.None:
                DestructionStarsUI[0].color = Color.black;
                DestructionStarsUI[1].color = Color.black;
                DestructionStarsUI[2].color = Color.black;
                break;
            case Rank.First:
                DestructionStarsUI[0].color = Color.white;
                DestructionStarsUI[1].color = Color.black;
                DestructionStarsUI[2].color = Color.black;
                break;
            case Rank.Second:
                DestructionStarsUI[0].color = Color.white;
                DestructionStarsUI[1].color = Color.white;
                DestructionStarsUI[2].color = Color.black;
                break;
            case Rank.Third:
                DestructionStarsUI[0].color = Color.white;
                DestructionStarsUI[1].color = Color.white;
                DestructionStarsUI[2].color = Color.white;
                break;
        }

        switch (GameDataManager.instance.loadedLevelDataMap[stage].SurvivalRank)
        {
            case Rank.None:
                SurvivalStarsUI[0].color = Color.black;
                SurvivalStarsUI[1].color = Color.black;
                SurvivalStarsUI[2].color = Color.black;
                break;
            case Rank.First:
                SurvivalStarsUI[0].color = Color.white;
                SurvivalStarsUI[1].color = Color.black;
                SurvivalStarsUI[2].color = Color.black;
                break;
            case Rank.Second:
                SurvivalStarsUI[0].color = Color.white;
                SurvivalStarsUI[1].color = Color.white;
                SurvivalStarsUI[2].color = Color.black;
                break;
            case Rank.Third:
                SurvivalStarsUI[0].color = Color.white;
                SurvivalStarsUI[1].color = Color.white;
                SurvivalStarsUI[2].color = Color.white;
                break;
        }


        switch (GameDataManager.instance.loadedLevelDataMap[stage].TreasureRank)
        {
            case Rank.None:
                TreasureStarsUI[0].color = Color.black;
                TreasureStarsUI[1].color = Color.black;
                TreasureStarsUI[2].color = Color.black;
                break;
            case Rank.First:
                TreasureStarsUI[0].color = Color.white;
                TreasureStarsUI[1].color = Color.black;
                TreasureStarsUI[2].color = Color.black;
                break;
            case Rank.Second:
                TreasureStarsUI[0].color = Color.white;
                TreasureStarsUI[1].color = Color.white;
                TreasureStarsUI[2].color = Color.black;
                break;
            case Rank.Third:
                TreasureStarsUI[0].color = Color.white;
                TreasureStarsUI[1].color = Color.white;
                TreasureStarsUI[2].color = Color.white;
                break;
        }

        OpenUI();
    }
    public void OpenUI()
    {
        StatsUI.SetActive(true);
    }

    public void CloseUI()
    {
        StatsUI.SetActive(false);
    }


}
