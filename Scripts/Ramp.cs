﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ramp : MonoBehaviour {

    public float jumpPower = 25;

    public GameObject JumpToLane;
    private BezierSpline spline;

    public float ProgressPoint = 0;
    // Use this for initialization
    void Start () {
        spline = JumpToLane.GetComponent<BezierSpline>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {
      //  Debug.Log(col.name + "Triggered");

        if(col.CompareTag("Player"))
        {
            PlayerCarController player = col.GetComponentInParent<PlayerCarController>();
          //  Debug.Log(player + "Triggered");
            if (player != null)
            {
                player.Jump(jumpPower);
                Debug.Log("Jumped");

                player.walker.SetSpline(spline,ProgressPoint);
            }
            
        }
    }
}
