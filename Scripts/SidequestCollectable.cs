﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SidequestCollectable : MonoBehaviour
{

    public string CollectEvent;
    private AudioSource sound;
    public Renderer collectRenderer;
    public GameObject CollectModel;
    public Collider collectCollider;
    // Use this for initialization
    void Start()
    {

        sound = GetComponent<AudioSource>();
        sound.volume = SM.audioVolume;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            sound.volume = SM.audioVolume;
            sound.Play();

            if (CollectEvent != "")
                EventManager.TriggerEvent(CollectEvent);

            StartCoroutine(Pickup());
        }

     
    }



    IEnumerator Pickup()
    {

        //play animation
        if (collectRenderer != null)
        {
            collectRenderer.enabled = false;
        }

        if(CollectModel != null)
        {
            CollectModel.SetActive(false);
        }

        collectCollider.enabled = false;

        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);
    }
}
