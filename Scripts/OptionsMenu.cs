﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenu : MonoBehaviour {


    public AudioSource OptionSound;  

    public AudioClip menuSelectionSD;
    public AudioClip MenuMusic;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    

    public void PlayButtonSelection()
    {
        OptionSound.PlayOneShot(menuSelectionSD,SM.audioVolume);
    }
}
