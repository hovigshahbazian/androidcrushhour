﻿using UnityEngine;

public class SplineWalker : MonoBehaviour {


	public BezierSpline currentSpline;
    public GameObject StartingLane;
	public float distanceTraveledInCurrentLane;
    public float totalDistanceTraveled;
	public bool lookForward;
    public bool FindStartLaneOnWake = true;
	public SplineWalkerMode mode;

    public float distance;
	public float progress;
	private bool goingForward = true;

    public float Speed;
    public float curVelocity;
    public float MaximumVelocity;
    public float MinimumVelocity;

    public float Accleration;
    public bool Slowing;

    public int Point;
    public int inputProgress;
    public bool Active;
    public bool Switching = false;

    public Vector3 SplineVelocity;
    public float SplineVelocityMagnitude;

    void Awake()
    {
        if(FindStartLaneOnWake)
        StartingLane = GameObject.FindGameObjectWithTag("StartLane");
    }

    void Start()
    {
        EventManager.StartListening("StartLevel", StartWalker);
       
        SetUp();
        Place();
      
    }


    public void SetUp()
    {
        if (StartingLane != null)
        {
            currentSpline = StartingLane.GetComponent<BezierSpline>();
            distance = currentSpline.FindMagnitudeOfSpline();
        }
    }
    public void SetCurrentLane(BezierSpline spline)
    {
        currentSpline = spline;
        distance = currentSpline.FindMagnitudeOfSpline();
    }

    public void SetSpline(BezierSpline otherspline, float progressPoint)
    {
        currentSpline = otherspline;
        distance = otherspline.FindMagnitudeOfSpline();
        distanceTraveledInCurrentLane = progressPoint * distance;

    }

    public void Place()
    {
        if (currentSpline != null)
        {
            Vector3 position = currentSpline.GetPoint(progress);
            transform.localPosition = position;
            if (lookForward)
            {
                transform.LookAt(position + currentSpline.GetDirection(progress));
            }
        }

    }

    public void CopyWalkerProgress(SplineWalker walker)
    {
        distanceTraveledInCurrentLane = walker.distanceTraveledInCurrentLane;
        progress = walker.GetCurrentProgress();
    }

    public void StartWalker()
    {
        Active = true;
        goingForward = true;
    }

    private void Update()
    {
        
        if (Active)
        {
            if (goingForward)
            {

                if (Slowing)
                {
                    curVelocity = Mathf.Clamp(curVelocity -= Accleration, MinimumVelocity, MaximumVelocity);
                }
                else
                {
                    curVelocity = Mathf.Clamp(curVelocity += Accleration, MinimumVelocity, MaximumVelocity);
                }

                distanceTraveledInCurrentLane += (curVelocity * Time.deltaTime);
                totalDistanceTraveled += (curVelocity * Time.deltaTime);

               
             
                progress = distanceTraveledInCurrentLane / distance;
                //Debug.Log(progress + " Updated propgress");



                SplineVelocity = currentSpline.GetVelocity(progress);
                SplineVelocityMagnitude = SplineVelocity.magnitude;

                if (progress > 1f)
                {

                    if (mode == SplineWalkerMode.Once)
                    {
                        progress = 1f;
                        Active = false;
                        goingForward = false;
                    }
                    else if (mode == SplineWalkerMode.Loop)
                    {
                        progress -= 1f;
                    }
                    else
                    {
                        progress = 2f - progress;
                        goingForward = false;
                    }
                }


               

            }
            else
            {
                /*
                progress -= Time.deltaTime / duration;
                if (progress < 0f)
                {
                    progress = -progress;
                    goingForward = true;
                }*/
            }

            Vector3 position = currentSpline.GetPoint(progress);
             transform.localPosition = position;
      
            if (lookForward)
            {
                transform.LookAt(position + currentSpline.GetDirection(progress));
            }
        }


        if (Input.GetKeyDown(KeyCode.P))
        {

            progress = (float)Point / (float)currentSpline.ControlPointCount;



            Vector3 position = currentSpline.GetPoint(progress);
            transform.localPosition = position;
            Debug.Log("Move to: " + progress );
           
        }


        if (Input.GetKeyDown(KeyCode.O))
        {

            progress = (float)Point / (float)currentSpline.ControlPointCount;



            Vector3 position = currentSpline.GetPoint(progress);
            transform.localPosition = position;
        

        }



    }

    public float GetCurrentProgress()
    {
        return distanceTraveledInCurrentLane / distance;
    }


   

}