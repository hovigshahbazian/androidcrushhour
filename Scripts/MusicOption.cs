﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicOption : MonoBehaviour {


    public Slider slider;
    private MM mm;
	// Use this for initialization
	void Start () {

        slider.onValueChanged.AddListener(MM.instance.SetVolumePref);
        slider.value = MM.instance.GetVolumePrefs();

    }
	
	// Update is called once per frame
	void Update () {
		
	}



}
