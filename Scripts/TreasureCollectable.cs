using System.Collections;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class TreasureCollectable : MonoBehaviour
{
    public int diamondValue = 100;

    void OnEnable()
    {
       // GetComponent<Renderer>().enabled = true;
       // GetComponent<Collider>().enabled = true;
    }
   
    void OnTriggerEnter(Collider col)
    {

        if (col.CompareTag("Player"))
        {
          //  GetComponent<Renderer>().enabled = false;
           // GetComponent<Collider>().enabled = false;
            EventManager.TriggerEvent("TreasureGrab");
            StartCoroutine(Pickup(col));
        }
    }

    IEnumerator Pickup(Collider player)
    {

        player.GetComponentInParent<TreasureCollector>().Collect(diamondValue);
        yield return new WaitForSeconds(1);
       // gameObject.SetActive(false);
    }



}

