﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionTimerUI : MonoBehaviour {

    public Text text;
    public float MissionTime = 360;
    [SerializeField]
    private float MissionTimer;
    private int min;
    private int secs;
    [SerializeField]
    private bool TimerActive;
   // public string eventStart;
    public string eventTimeEnd;
	// Use this for initialization
	void Start () {
        RestartTimer();
        EventManager.StartListening("StartLevel", StartTimer);
        EventManager.StartListening("StageComplete", PauseTimer);
        EventManager.StartListening("PlayerCrash", PauseTimer);
        UpdateUI();
	}
	
	// Update is called once per frame
	void Update () {



        if (TimerActive)
        {
            MissionTimer -= Time.deltaTime;
            UpdateUI();
            if(MissionTimer <= 0)
            {
                TimerActive = false;
                MissionTimer = MissionTime;
                if(eventTimeEnd != "")
                {
                    EventManager.TriggerEvent(eventTimeEnd);
                }

            }
        }
	}

    public void StartTimer()
    {
        TimerActive = true;
    }

    public void PauseTimer()
    {
        TimerActive = false;
    }

    public void RestartTimer()
    {
        TimerActive = false;
        MissionTimer = MissionTime;
    }

    public void UpdateUI()
    {
       

        min = (int)(MissionTimer / 60);
        
        secs = (int)(MissionTimer % 60);
        //Debug.Log(min + ":" + secs);
        text.text = min.ToString("D2") + ":" + secs.ToString("D2");
    }
}
