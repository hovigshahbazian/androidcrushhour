﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedometerUI : MonoBehaviour {


    public Image image;
    private float angle;
    public Speed speed;
    //range 0 to -240

	// Use this for initialization
	void Start () {
      
	}
	
	// Update is called once per frame
	void Update () {
        angle = speed.walker.curVelocity / 60;
       // Debug.Log(angle);
        image.GetComponent<RectTransform>().localRotation =  Quaternion.Euler(new Vector3(0, 0, -240 * angle));
	}
}
