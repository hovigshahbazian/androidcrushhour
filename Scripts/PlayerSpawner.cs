﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {

	//public PlayerGameData playerData;
	public Vector3 spawnPoint;
	public GameObject[] playerCars;
	public FollowMultiTransform followScripts;
	private GameObject curplayer;
    public SplineWalker mainWalker;
    public GameObject StatsUI;
	// Use this for initialization

    void Awake()
    {
        mainWalker = GameObject.FindGameObjectWithTag("PlayerLaneWalker").GetComponent<SplineWalker>();
    }

    void Start() {

		switch (GameDataManager.instance.playerData.CarType) {
		    case CarType.BasicCar:
			    curplayer = GameObject.Instantiate (playerCars [0],spawnPoint, playerCars[0].transform.rotation);
			    break;
		    case CarType.Ambulance:
			    curplayer = GameObject.Instantiate (playerCars [1],spawnPoint,playerCars[1].transform.rotation);
			    break;
		    case CarType.CopCar:
			    curplayer = GameObject.Instantiate (playerCars [2],spawnPoint,playerCars[2].transform.rotation);
			    break;
		    case CarType.Taxi:
			    curplayer = GameObject.Instantiate (playerCars [3],spawnPoint,playerCars[3].transform.rotation);
			    break;
		    case CarType.PickupTruck:
			    curplayer = GameObject.Instantiate (playerCars [4],spawnPoint,playerCars[4].transform.rotation);
			    break;
            case CarType.DerbyCar:
                curplayer = GameObject.Instantiate(playerCars[5], spawnPoint, playerCars[5].transform.rotation);
                break;
            case CarType.F1Car:
                curplayer = GameObject.Instantiate(playerCars[6], spawnPoint, playerCars[6].transform.rotation);
                break;
            case CarType.MonsterTruck:
                curplayer = GameObject.Instantiate(playerCars[7], spawnPoint, playerCars[7].transform.rotation);
                break;
            case CarType.MuscleCar:
                curplayer = GameObject.Instantiate(playerCars[8], spawnPoint, playerCars[8].transform.rotation);
                break;
            case CarType.OffRoadTruck:
                curplayer = GameObject.Instantiate(playerCars[9], spawnPoint, playerCars[9].transform.rotation);
                break;
            case CarType.RalleyCar:
                curplayer = GameObject.Instantiate(playerCars[10], spawnPoint, playerCars[10].transform.rotation);
                break;
            case CarType.StockCar:
                curplayer = GameObject.Instantiate(playerCars[11], spawnPoint, playerCars[11].transform.rotation);
                break;
            case CarType.StreetCar:
                curplayer = GameObject.Instantiate(playerCars[12], spawnPoint, playerCars[12].transform.rotation);
                break;
            case CarType.SportsCar:
                curplayer = GameObject.Instantiate(playerCars[13], spawnPoint, playerCars[13].transform.rotation);
                break;
            case CarType.SportsMuscleCar:
                curplayer = GameObject.Instantiate(playerCars[14], spawnPoint, playerCars[14].transform.rotation);
                break;

            default:
            curplayer = GameObject.Instantiate(playerCars[0], spawnPoint, playerCars[0].transform.rotation);
             break;
        }

	    if(followScripts != null)
		followScripts.AssignTarget(curplayer.transform);
		

       // curplayer.GetComponent<PlayerCarController>().walker = mainWalker;

        if (StatsUI != null) {
            StatsUI.SetActive(true);
        }
       
	}
	
   
}
