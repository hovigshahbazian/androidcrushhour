﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacerController : MonoBehaviour {

    private SplineWalker playerWalker;
    public bool Boosting = false;
    public float VisionRange = 25f;
    public Vector3 forward;
    public Vector3 position;
    private PlayerCarController controller;
	// Use this for initialization
	void Start () {
        controller = GetComponent<PlayerCarController>();
        playerWalker = controller.walker;
	}
	

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Racer"))
        {
            VehicleRacer racer = col.gameObject.GetComponent<VehicleRacer>();

            if(racer != null)
            {
            //    racer.StopChangingLane();
            }
        }
    }


    void FixedUpdate()
    {
       
        RaycastHit raycastHitInfo;
      //  Debug.Log("Raycasting");
        if (Physics.Raycast(position + (Vector3.up*0.5f), (forward*VisionRange), out raycastHitInfo, VisionRange)){
           // Debug.Log(gameObject.name + " Touching" + raycastHitInfo.collider.gameObject.name);
            if (raycastHitInfo.collider.transform.parent != null){
               // Debug.Log("Finding " + raycastHitInfo.collider.gameObject.name);
               
                if (raycastHitInfo.collider.CompareTag("Racer"))
                {
                    BackDraftSpeedUp();
                   // Debug.Log("Speeding");
                }
              }
        }
        else
        {
          //  Debug.Log("Not hitting");
        }
    }

    public void LateUpdate()
    {
        forward = controller.GetMyTransform().forward;
        position = controller.GetMyTransform().position;
       // Debug.Log(position);
       // Debug.DrawRay(position + (Vector3.up * 0.5f), (forward * VisionRange), Color.red, 0.001f);
    }


    public void BackDraftSpeedUp()
    {
        if(!Boosting)
        StartCoroutine(SpeedUp());
    }

    IEnumerator SpeedUp()
    {
        Boosting = true;
        playerWalker.Accleration += 0.05f ;
        playerWalker.MaximumVelocity += 5 ;
        yield return new WaitForSeconds(5f);
       playerWalker.MaximumVelocity -= 5;
        playerWalker.Accleration -= 0.05f;
        Boosting = false;
    }


}
