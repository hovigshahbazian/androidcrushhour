﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;
public class PlayerCarController : MonoBehaviour
{
    public enum PowerUps { None = -1, Ambulance = 1, Slow = 2, Speed = 3, Fog = 4, RoadRage = 5, Diamond = 6, Ghost = 7 }
    public enum BumpMode { Contact, Horizontal,ForwardOnly, LeftOnly, RightOnly, BackOnly}
    public BumpMode currentBumpMode = BumpMode.Contact;
    public SplineWalker walker;
    private RoadRageController rrc;
    public Rigidbody PlayerRigidBody;
    public enum PlayerState { Grounded, Jumping, Falling}
 

    public PowerUps CurrentPowerUp;
    public PlayerState playerstate;
    public bool Active;

    private float MinX = -5f;
    private float MaxX = 5f;
    public float relativeX = 0;
    private float relativeXChange = 0;
    private float bumpVelocity = 0;
    public float relativeY = 0;
    public float dropOff = 0;
    public bool PlayerCrashed;

 

    public float SpeedRating = 1;
   // public float AccelCap = 0.5f;
    public Vector3 velocity = Vector3.zero;
    public float GroundLevel = 0;
 //   public bool isFalling = false;
 //   public bool isJumping = false;
  //  public bool isTouchingFloor = false;

    private Transform myTransform;
    private Transform walkerTransform;
    public bool CollisionEventsEnabled;

    public float CarBumpSlowValue;
    public bool SlowOnCarBump;
    public float SlowRecoverTime;
    private float bumpTimer = 0.1f;
    public AudioSource CarSound;
    public AudioClip CarBumpSD;

    private bool Bumped;
    private bool contactTimeFlag = false;
    
    public float ContactTime = 0.5f;
    [SerializeField]
    private float contactTimer;
    private FollowMultiTransform followTrans;
    void Awake()
    {
        contactTimer = ContactTime;
        followTrans = Camera.main.GetComponent<FollowMultiTransform>();
        followTrans.AssignTarget(transform);
        walker = GameObject.FindGameObjectWithTag("PlayerLaneWalker").GetComponent<SplineWalker>();
        PlayerRigidBody = GetComponent<Rigidbody>();
    }

    void Start()
    {
        myTransform = transform;
        walkerTransform = walker.transform;
        myTransform.position = walkerTransform.position;
        CarSound.volume = SM.audioVolume;
        rrc = GetComponent<RoadRageController>();
        CurrentPowerUp = PowerUps.None;
        GroundLevel = walker.transform.position.y;
        EventManager.StartListening("StartLevel", Activate);
        EventManager.StartListening("PlayerCrash", Stopped);
        EventManager.StartListening("AmbulanceEnd", ResetPowerState);

        EventManager.TriggerEvent("PlayerSpawn");
    }

    void Update()
    {
        if (contactTimeFlag) {
            contactTimer -= Time.deltaTime;
            if (contactTimer <= 0)
            {
                contactTimer = ContactTime;
                contactTimeFlag = false;
            }
        }


        if(playerstate == PlayerState.Grounded)
        {          
            dropOff = 1f;
            velocity = Vector3.zero;
            GroundLevel = walkerTransform.position.y;
            myTransform.position = new Vector3(walkerTransform.position.x + relativeX, GroundLevel, walkerTransform.position.z);
        }


        if(playerstate == PlayerState.Jumping)
        {
            dropOff -= (0.05f);
            velocity.y += (-Time.deltaTime * 15);

            if (velocity.y < 0)
            {
                Debug.Log("Switching to falling State");
                playerstate = PlayerState.Falling;
            }

        }

        if(playerstate == PlayerState.Falling)
        {
            dropOff -= (0.05f);
            velocity.y += (-Time.deltaTime*15);
            GroundLevel = walkerTransform.position.y;

            if (myTransform.position.y <= GroundLevel)
            {
                myTransform.position = new Vector3(walkerTransform.position.x + relativeX, GroundLevel, walkerTransform.position.z);
                dropOff = 0f;
                velocity = Vector3.zero;
                playerstate = PlayerState.Grounded;
                Debug.Log("Switching to Ground State");
            }

        }

        myTransform.position += (velocity * Time.deltaTime);


    }

    public void Activate()
    {
        Active = true;
    }

  
    public Transform GetMyTransform()
    {
        return myTransform;
    }

    void FixedUpdate()
    {
        if (Active && !PlayerCrashed)
        {
          //  GroundLevel = walkerTransform.position.y;

            Vector3 down = -myTransform.up;
            Ray ray = new Ray(myTransform.position, down);

            Debug.DrawRay(ray.origin, ray.direction, Color.blue);
            
            RaycastHit hit;

            bool TouchingGround = Physics.Raycast(ray, out hit,10f);
            
            if (TouchingGround)
            {
              //  Debug.DrawLine(myTransform.position, hit.point, Color.green);

              //  Debug.Log(hit.distance);
            }
           // Debug.DrawRay(transform.position, transform.position + Vector3.up * 0.5f, Color.blue);
            //  Debug.DrawLine(transform.position , transform.position + Vector3.up * 0.5f, Color.blue);
           // Debug.Log(gameObject.name + "istouching ground:" + TouchingGround);
       

            if (!TouchingGround && playerstate == PlayerState.Grounded)
            {           
                playerstate = PlayerState.Falling;
                Debug.Log("Switching to Falling state");
            }          
        }
    }

    void LateUpdate()
    {

        if (Active && !PlayerCrashed)
        {
            if (!SteeringWheel.isHold)
            {
                SteeringWheel.input = 0;
            }

            
            

            if (GM.accelerometer)
            {
                float temp = Mathf.Clamp(Input.acceleration.x *3, -1, 1);

                relativeXChange = ((temp + bumpVelocity) * SpeedRating * Time.deltaTime);                  
            }
            else
            {
                relativeXChange = ((SteeringWheel.input + bumpVelocity)* SpeedRating * Time.deltaTime);              
            }

            relativeX = Mathf.Clamp(relativeX += relativeXChange, MinX, MaxX);

            if (walker.Active) {
                myTransform.position = new Vector3(walkerTransform.position.x, myTransform.position.y, walkerTransform.position.z) +  walkerTransform.right*relativeX;

                
            
                    myTransform.LookAt(myTransform.position + walker.currentSpline.GetDirection(walker.progress));

               // PlayerRigidBody.MovePosition(myTransform.position);
               // PlayerRigidBody.Sleep();


            }


        }


    }
    void OnCollisionEnter(Collision col)
    {

        if (CollisionEventsEnabled)
        {
            VehicleObstacle obs = col.gameObject.GetComponentInParent<VehicleObstacle>();
            DestructableObstacle dObs = col.gameObject.GetComponentInParent<DestructableObstacle>();
            if (!rrc.Active)
            {
                Vector3 contact = transform.InverseTransformPoint(col.contacts[0].point);


                // 

                if (obs != null)
                {
                    CarBump();
                    
                    
                    SendMessage("Damage");

                    CarBumpContact(-contact, 0.000f, 1f);
                    PlayCarBumpSound();
                }
               // Debug.Log("Bump");

                if (obs != null && !contactTimeFlag)
                {
                    contactTimeFlag = true;
                    switch (currentBumpMode)
                    {

                        case BumpMode.Contact:
                            obs.CarBumpContact(contact, 0.006f,10f);
                            break;
                        case BumpMode.ForwardOnly:
                            obs.CarBumpForward(0.012f);
                            break;
                        case BumpMode.RightOnly:
                            obs.CarBumpRightward(10f);
                            break;
                        case BumpMode.LeftOnly:
                            obs.CarBumpLeftward(10f);
                            break;
                        case BumpMode.BackOnly:
                            obs.CarBumpBackwards(0.012f);
                            break;
                    }
                }


                if (dObs != null)
                {
                    EventManager.TriggerEvent("RoadObstacleCrashed");
                    dObs.Crash(transform.forward.normalized);
                }
            }
            else //If RoadRage Power is Active run this block
            {
                if (obs != null)
                {
                    EventManager.TriggerEvent("RoadRageHit");
                }
                if (dObs != null)
                {
                    EventManager.TriggerEvent("RoadObstacleCrashed");
                }
                Vector3 contact = transform.InverseTransformPoint(col.contacts[0].point);
                rrc.RoadRageHitAnim.transform.localPosition = new Vector3(contact.x, 1.7f, contact.z);
                
                //obs.Crash(new Vector3(contact.x, 1.7f, contact.z));
                if (obs != null)
                {
                    if (obs.CrashAble)
                    {
                        obs.Crash(transform.forward.normalized);
                    }
                    else
                    {
                        obs.CarBumpContact(contact, 0.006f, 10f);
                    }
                }

                if(dObs != null)
                {
                    dObs.Crash(transform.forward.normalized);
                }
            }
        }
    }

    public void Jump(float JumpPower)
    {
        Debug.Log("traying to jump in state:" + playerstate);
            if (playerstate == PlayerState.Grounded || playerstate == PlayerState.Falling)
            {
                Debug.Log(" swithcing to Jumping state");
                velocity = (Vector3.up * JumpPower);
                playerstate = PlayerState.Jumping;
            }       
    }


    public void Stopped()
    {
        PlayerCrashed = true;
        walker.Active = false;
       CollisionEventsEnabled = false;
    }



    public void ResetPowerState()
    {
        Debug.Log("Power Reset");
        EventManager.TriggerEvent("StopCurrentPowerUp");
    }

    public void CarBump()
    {
        if (SlowOnCarBump)
        {
            StartCoroutine(ApplySlowDown());
        }

       // Debug.Log("CarBump");
    }

   

    IEnumerator ApplySlowDown()
    {
        SlowDown();
        CurrentPowerUp = PowerUps.Slow;
        yield return new WaitForSeconds(SlowRecoverTime);
        CurrentPowerUp = PowerUps.None;
        Revert();
    }

    public void SlowDown()
    {
        EventManager.TriggerEvent("SlowGrab");
        walker.curVelocity -= CarBumpSlowValue;
        walker.MaximumVelocity -= CarBumpSlowValue;
    }


    public void SlowDown(float value)
    {
        walker.curVelocity -= value;
        walker.MaximumVelocity -= value;
    }

    public void SpeedUP(float value)
    {
        walker.curVelocity += value;
        walker.MaximumVelocity += value;
    }

    public void Revert()
    {
        EventManager.TriggerEvent("SlowEnd");
        walker.curVelocity += CarBumpSlowValue;
        walker.MaximumVelocity += CarBumpSlowValue;
    }


    public void CarBumpForward(float dist)
    {
        StartCoroutine(CarBumpFor(dist));
    }

    public void CarBumpBackwards(float dist)
    {
        StartCoroutine(CarBumpBack(dist));
    }

    public void CarBumpLeftward(float dist)
    {
        StartCoroutine(CarBumpLeft(dist));
    }
    public void CarBumpRightward(float dist)
    {
        StartCoroutine(CarBumpRight(dist));
    }

    IEnumerator CarBumpFor(float forwardBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;
         
    
            walker.curVelocity += forwardBumpChange;
            walker.MaximumVelocity += forwardBumpChange;
            yield return new WaitForSeconds(bumpTimer);
            walker.curVelocity -= forwardBumpChange;
            walker.MaximumVelocity -= forwardBumpChange;
            yield return new WaitForSeconds(bumpTimer);  
            Bumped = false;
        }
    }

    IEnumerator CarBumpBack(float forwardBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;
         

            walker.curVelocity -= forwardBumpChange;
            walker.MaximumVelocity -= forwardBumpChange;
            yield return new WaitForSeconds(bumpTimer);
            walker.curVelocity += forwardBumpChange;
            walker.MaximumVelocity += forwardBumpChange;
            yield return new WaitForSeconds(bumpTimer);

          
            Bumped = false;
        }
    }

    IEnumerator CarBumpRight(float sideBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;

            bumpVelocity += sideBumpChange;
            yield return new WaitForSeconds(bumpTimer);
            bumpVelocity += -sideBumpChange;

            Bumped = false;
        }
    }


    IEnumerator CarBumpLeft(float sideBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;

           bumpVelocity += -sideBumpChange;

            yield return new WaitForSeconds(bumpTimer);
           bumpVelocity += sideBumpChange;
            Bumped = false;
        }
    }

    public void CarBumpContact(Vector3 contact, float forwardDist, float HorizontalDist)
    {
        if (contact.z > 1)
            CarBumpForward(forwardDist);

        if (contact.z < -1)
            CarBumpBackwards(forwardDist);

        if (contact.x > 1)
            CarBumpRightward(HorizontalDist);

        if (contact.x < -1)
            CarBumpLeftward(HorizontalDist);            
    }

    public void PlayCarBumpSound()
    {
        if(CarSound != null)
            CarSound.PlayOneShot(CarBumpSD);
    }
}
