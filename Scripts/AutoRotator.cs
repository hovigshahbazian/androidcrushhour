﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AutoRotator : MonoBehaviour {
    public bool StartRandomAtRotation;
    public float RotationSpeed;
    public Vector3 RotationAxis;

    
    void Awake()
    {
        transform.Rotate(RotationAxis, Random.Range(0f, 1f) * 180f);
    }

	// Use this for initialization
	void Start () {
        if (StartRandomAtRotation)
        {
            transform.Rotate(RotationAxis, Random.Range(0f, 1f) * 180f);
            //float rotation = Random.Range(0f, 1f);
            RotationAxis = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        }
    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(RotationAxis, RotationSpeed * Time.deltaTime);
	}
}
