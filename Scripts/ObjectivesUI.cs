﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ObjectiveType { None,Chase,Collect,CarBump,RoadRage, NoHit, TopSpeed,Distance,ReachGoal, Race,TimeLimit,BossCrush }

[System.Serializable]
public struct Objective
{
    [SerializeField]
    public string ObjectiveName;
    [SerializeField]
   public ObjectiveType ObjectiveType;
    [SerializeField]
    public float ObjectiveValue;
}

[System.Serializable]
public struct StageMission
{
    [SerializeField]
    public Stage stage;

    [SerializeField]
    public Objective[] Objectives;
}

public class ObjectivesUI : MonoBehaviour {

    [HideInInspector]
    public StageMission[] stageMissions;


    private Stage CurrentStage;
    public Text[] objectiveTexts;

	// Use this for initialization
	void Start () {
        SetUp();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void SetUp()
    {

        CurrentStage = GameObject.Find("GM").GetComponent<GM>().LevelID;

        int i = 0;
        foreach (Text text in objectiveTexts)
        {          

            if( i == 0)
            {
                text.text = "1)";
            }
            else if( i ==1)
            {
                text.text = "2)";
            }
            else if( i == 2)
            {
                text.text = "3)";
            }
                switch (stageMissions[(int)CurrentStage].Objectives[i].ObjectiveType)
                {
                    case ObjectiveType.BossCrush:
                        text.text += "Defeat The boss!";
                    break;
                    case ObjectiveType.Chase:
                        text.text += "Make it to the End before the " + stageMissions[(int)CurrentStage].Objectives[i].ObjectiveName + " catches you!";
                        break;
                    case ObjectiveType.NoHit:
                        text.text += "Dont Get Hit by " + stageMissions[(int)CurrentStage].Objectives[i].ObjectiveName;
                        break;
                    case ObjectiveType.Collect:
                        text.text += "Collect " + stageMissions[(int)CurrentStage].Objectives[i].ObjectiveValue + " " + stageMissions[(int)CurrentStage].Objectives[i].ObjectiveName;
                        break;
                    case ObjectiveType.CarBump:
                        text.text += "Bump into " + stageMissions[(int)CurrentStage].Objectives[i].ObjectiveValue + " " + stageMissions[(int)CurrentStage].Objectives[i].ObjectiveName;
                        break;
                    case ObjectiveType.Distance:
                        text.text += "Travel for " + stageMissions[(int)CurrentStage].Objectives[i].ObjectiveValue + " Miles";
                        break;
                    case ObjectiveType.RoadRage:
                        text.text += "Use Road Rage to Crush " + stageMissions[(int)CurrentStage].Objectives[i].ObjectiveValue + " " + stageMissions[(int)CurrentStage].Objectives[i].ObjectiveName + " off the Road";
                        break;
                    case ObjectiveType.TopSpeed:
                        text.text += "Go to the Top Speed of  " + stageMissions[(int)CurrentStage].Objectives[i].ObjectiveValue + " MPH";
                        break;
                    case ObjectiveType.ReachGoal:
                        text.text += "Make it to the end of the Stage";
                        break;
                    case ObjectiveType.Race:
                        text.text += "Make it First to the Finish Line.";
                        break;
                    case ObjectiveType.TimeLimit:
                        text.text += "Reach the End before Time Runs out!";
                        break;
                    case ObjectiveType.None:
                        text.text = "";
                        break;
                    default:
                        break;
                }

            i++;
           

            // stageMissions[CurrentStage].Objectives[i].ObjectiveName
        }






    }
}
