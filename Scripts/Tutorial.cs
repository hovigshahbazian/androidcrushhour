﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour {

    public Stage stage;  
    public GameObject tutorialUI;

	// Use this for initialization
	void Start () {
        Debug.Log(GetTutorialState());
        if(tutorialUI != null)
             tutorialUI.SetActive(!GetTutorialState());
        
    }
	


    public bool GetTutorialState()
    {
        return GameDataManager.instance.loadedLevelDataMap[stage].TutorialViewed;
    }



    public void SetTutorialState(bool b)
    {
        Debug.Log("Saving:" + b);
        GameDataManager.instance.loadedLevelDataMap[stage].TutorialViewed = b;
        GameDataManager.instance.SaveLevel(stage);
    }

        
        


}
