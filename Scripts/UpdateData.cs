﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateData : MonoBehaviour {

    private void Awake()
    {
        GameDataManager.instance.Load();

        foreach(Stage s in GameDataManager.instance.loadedLevelDataMap.Keys)
        {
           // GameDataManager.instance.LoadLevel(s);
        }

        
        SteeringWheelSenseSlider.LoadData();
        CarControlOptions.LoadData();
    }
  

    public void OnApplicationQuit()
    {
        GameDataManager.instance.Save();
    }
}
