﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class QuestCollectableCounterUI : MonoBehaviour {

    public SidequestCollect  questEvent;
  
    private Text counterText;
    private int eventC;
    private int eventG;
    // Use this for initialization
    void Start()
    {
        counterText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

        eventC = questEvent.CollectCounter;
        eventG = questEvent.CollectGoal;

        counterText.text = eventC + "/" + eventG + " " + questEvent.CollectableName;


    }
}
