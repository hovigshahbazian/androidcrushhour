﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {
    public bool paused;
    public GameObject PauseMenuUI;
    [SerializeField]
    private GM GameMananger;
    public Button RestartButton;
    public Button MainMenuButton;
    public Image PauseImage;
    public Image ResumeImage;
    // Use this for initialization
    void Start () {

        EventManager.StartListening("Pause", Pause);
        EventManager.StartListening("Unpause", Unpause);
        GameMananger = GameObject.Find("GM").GetComponent<GM>();
        if (RestartButton != null)
        {
            RestartButton.onClick.AddListener(GameMananger.Restart);
            RestartButton.onClick.AddListener(PlayButtonSound);

        }


        if (MainMenuButton != null)
        {
            MainMenuButton.onClick.AddListener(GameMananger.ReturnToMainMenu);
            RestartButton.onClick.AddListener(PlayButtonSound);
        }

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void PlayButtonSound()
    {
        SM.instance.PlaySound(SM.Sound.Menu_Select);
    }

    public void Pause()
    {
        Time.timeScale = 0;
        PauseMenuUI.SetActive(true);
        PlayButtonSound();
    }

    public void Unpause()
    {
        Time.timeScale = 1;
        PauseMenuUI.SetActive(false);
        PlayButtonSound();
    }



    public void TogglePause()
    {
        if (!paused)
        {
            paused = true;
            PauseImage.enabled = false;
            ResumeImage.enabled = true;
            Pause();
            
        }else
        {
            paused = false;
            PauseImage.enabled = true;
            ResumeImage.enabled = false;
            Unpause();
        }
    }
}
