﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class StartMenu : MonoBehaviour {


	public float startSecs;
	public string MainMenuScene;
	public string Options;
	public Animation fadeAnim;
	public Animation CarAnim;
	//public Animator stBtnAnim;
	//public Animator psBtnAnim;
	//public Animator opBtnAnim;


	//public Animation clip;

	//public GameObject stButton;
	//public GameObject psButton;
	//public GameObject opButton;

	//public Animator GameMapAnim;

	AsyncOperation async;
	AudioSource gmStSound;

	float transitionTimer;
	bool startTimer = false;

	// Use this for initialization
	void Start () {
		transitionTimer = startSecs;
		gmStSound = GetComponent<AudioSource> ();

	}
	
	// Update is called once per frame
	void Update () {

		if (startTimer) {
			transitionTimer -= Time.deltaTime;


			if (transitionTimer <= 0) {
				//StartGame ();
				async.allowSceneActivation = true;
			}
		}
			
	}
	 
	public void LoadGame(){
		startTimer = true;
	//	stBtnAnim.SetBool ("Clicked", true);
		//gmStSound.Play ();

		/*if (psButton != null) {
			psButton.SetActive (false);
		}

		if (opButton != null) {
			opButton.SetActive (false);
		}


		GameMapAnim.SetBool ("StartedGame", true);*/
	}


	public void LoadStore(){
		//startTimer = true;
		//psBtnAnim.SetBool ("Clicked", true);
		//gmStSound.Play ();
	}

	public void LoadOptions(){
		//startTimer = true;
		//opBtnAnim.SetBool ("Clicked", true);
		//gmStSound.Play ();
	}




	public void StartGame(){
		//SceneManager.LoadScene (MainMenuScene);
	
	}


	public IEnumerator LoadCampaignMapAsync(){
		fadeAnim.Play ();
		CarAnim.Play ();
		gmStSound.Play ();
	//	clip.Play ();
		startTimer = true;
		//Debug.Log ("Loading Level");
		async = SceneManager.LoadSceneAsync(MainMenuScene);
		async.allowSceneActivation = false;
		yield return async;
	//	Debug.Log ("Loading Complete");
	}

	//public void PauseGame(){
	//	Application.
	//}

	public void QuitGame(){
		Application.Quit();
	}
}
