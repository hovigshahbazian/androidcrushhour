﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleHealth : MonoBehaviour {
    public enum PlayerState { Alive, Dead }
    public bool Crashed = false;
    public int HitPoints;
    public int MaxHealth;
    public float DamageDelay = 1;
    public Animator CrashAnim;
    [SerializeField]
    private bool hitable = true;
    public bool isInvulnerable;
    
    // Use this for initialization
    void Awake () {
        HitPoints = MaxHealth;
	}
    void Start()
    {
        

        
    }

    // Update is called once per frame
    public void Damage()
    {
        if (hitable && !isInvulnerable)
        {
            StartCoroutine(ApplyDamage());

        }
    }

    IEnumerator ApplyDamage()
    {
        hitable = false;
        HitPoints = Mathf.Clamp(HitPoints - 1, 0, MaxHealth);

        if (HitPoints == 0)
        {
            Crash();
        }
        else
        {
            CrashAnim.SetTrigger("Damage");
        }
        yield return new WaitForSeconds(DamageDelay);

        hitable = true;


    }

    public void MakeInvulnerable()
    {
        isInvulnerable = true;
    }

    public void MakeVulnerable()
    {
        isInvulnerable = false;
    }

    public void RecoverDamage()
    {
        HitPoints = Mathf.Clamp(HitPoints + 1, 0, MaxHealth);
    }

    public void Crash()
    {
        if (!Crashed)
        {
            
            Crashed = true;
            GetComponent<Rigidbody>().isKinematic = false;
            EventManager.TriggerEvent("StageComplete");
           // Debug.Log("Boss Rash");
            VehicleObstacle vehicle = GetComponent<VehicleObstacle>();
          if(vehicle != null){
                vehicle.walker.Active = false;
          }

            CrashAnim.SetTrigger("Crash");
        }

    }

}
