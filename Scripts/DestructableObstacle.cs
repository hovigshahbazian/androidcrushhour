﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableObstacle : MonoBehaviour {

    public string CrashEvent;
    public bool GravityOnHit;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    public void Crash(Vector3 crashVector)
    {
        GetComponent<Rigidbody>().AddForce(crashVector * 1200);
        if (CrashEvent != "")
        {
            EventManager.TriggerEvent(CrashEvent);
        }
        if (GravityOnHit)
        {
            GetComponent<Rigidbody>().useGravity = true;
        }
    }

}
