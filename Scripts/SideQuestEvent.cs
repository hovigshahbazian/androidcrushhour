﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideQuestEvent : MonoBehaviour {

    public string QuestName;
    public string EventName;
    public string SideQuestCompleteEvent;
    public int EventCounter;
    public int EventGoal;
    public bool completed;


	// Use this for initialization
	void Start () {
		
        if(EventName != "")
        {
            EventManager.StartListening(EventName, IncrementCounter);
        }

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void IncrementCounter()
    {
        if (!completed)
        {
            EventCounter++;
            if(EventCounter >= EventGoal)
            {
                completed = true;
                if (SideQuestCompleteEvent != "")
                {
                    EventManager.TriggerEvent(SideQuestCompleteEvent);
                }
            }
        }
    }
}
