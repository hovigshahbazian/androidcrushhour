﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreasureScoreUI : MonoBehaviour {

	private Text text;
    public TreasureCollector treasureCollector;
	// Use this for initialization
	void Awake () {

		text = GetComponent<Text> ();
        EventManager.StartListening("PlayerSpawn", SetUpUI);

    }
	
	// Update is called once per frame
	void Update () {
        text.text = treasureCollector.TreasureScore.ToString();
	}


    void SetUpUI()
    {
        treasureCollector = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<TreasureCollector>();
    }
}
