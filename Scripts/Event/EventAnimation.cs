﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventAnimation : MonoBehaviour {

    private Animation anim;
    public string eventName;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animation>();

       
	}

    public void StartListening()
    {
        EventManager.StartListening(eventName, PlayAnim);
    }

    public void StopListening()
    {
        EventManager.StopListening(eventName, PlayAnim);
    }

    public void PlayAnim()
    {
        if (anim.isPlaying)
        {
            anim.Stop();
        }
        anim.Play();
    }
}
