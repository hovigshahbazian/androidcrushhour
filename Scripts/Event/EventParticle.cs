﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventParticle : MonoBehaviour {

	public string EventTrigger;
	private ParticleSystem particle;
	// Use this for initialization
	void Start () {

		particle = GetComponent<ParticleSystem> ();

		EventManager.StartListening (EventTrigger, PlayParticle);
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void PlayParticle(){
		//Debug.Log ("PlayPartivle");
		particle.Play ();
	}
}
