﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCollider : MonoBehaviour
{


    public string EventName;
    public bool UseTag;
    public string eventTag;
    // Use this for initialization
    void Start()
    {

    }


    public void OnTriggerEnter(Collider other)
    {


        if (UseTag)
        {
            if (other.CompareTag(eventTag))
            {
               // Debug.Log("Triggering object " + other.tag);

                EventManager.TriggerEvent(EventName);
            }
        }
        else
        {
            Debug.Log("actived Event:" + EventName);
            EventManager.TriggerEvent(EventName);
        }
    }
}
