﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventActiveBehavior : MonoBehaviour {

    public string EventName;
    public bool ActiveState;
    public MonoBehaviour behavior;

	// Use this for initialization
	void Start () {
        if (EventName != null) {
            EventManager.StartListening(EventName, SetActive);
           }       
	}
	


    public void SetActive()
    {
        behavior.enabled = ActiveState;
    }
}
