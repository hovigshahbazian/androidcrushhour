﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventAnimator : MonoBehaviour {

	public string EventTrigger;
	public string animTrigger;
    
	private Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();

		EventManager.StartListening (EventTrigger,SetTrigger);


	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetTrigger(){
        
		anim.SetTrigger (animTrigger);
	}


}
