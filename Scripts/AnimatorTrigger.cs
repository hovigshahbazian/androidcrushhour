﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorTrigger : MonoBehaviour {
    public Animator animator;
    public string triggerName = "";

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void OnTriggerEnter(Collider other)
    {
        if(triggerName != "")
        {
            animator.SetTrigger(triggerName);
        }
    }
}
