﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySwitch : MonoBehaviour {
    public Rigidbody rigid;

    
	public void TurnOnGravity()
    {
        rigid.useGravity = true;
    }

    public void TurnOffGravity()
    {
        rigid.useGravity = false;
    }


}
