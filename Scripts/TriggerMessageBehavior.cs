﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMessageBehavior : MonoBehaviour {

    public MonoBehaviour behavior;
    public string Message;

 

     void OnTriggerEnter(Collider other)
    {
        Debug.Log("Message trigger");
        behavior.SendMessage(Message);
    }
}
