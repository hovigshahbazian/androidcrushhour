﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartingStageUI : MonoBehaviour {

    public GameObject StartingUI;  
    public Button StartButton;
    private GM GameMananger;

	// Use this for initialization
	void Start () {
        
        GameMananger = GameObject.Find("GM").GetComponent<GM>();

        if (StartButton != null)
        {
            StartButton.onClick.AddListener(GameMananger.StartStage);
            StartButton.onClick.AddListener(CloseStartUI);
            StartButton.onClick.AddListener(PlayButtonSound);
        }

    }
	
	// Update is called once per frame

    public void PlayButtonSound()
    {
        SM.instance.PlaySound(SM.Sound.Menu_Select);
    }

    public void CloseStartUI()
    {
        StartingUI.gameObject.SetActive(false);
    }


   
}
