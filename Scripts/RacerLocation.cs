﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RacerLocation : MonoBehaviour {

    public SplineWalker playerLaneWalker;
    public VehicleObstacle[] Racers;

    private int Rank;
    public int currentRank;
    public Text RacerRankTextUI;

    public bool RaceFinished = false;
	// Use this for initialization
	void Start() {

        EventManager.StartListening("StageComplete", RaceCompleted);

    }
	
    public void RaceCompleted()
    {
        RaceFinished = true;
    }


	// Update is called once per frame
	void Update () {

        if (!RaceFinished)
        {
            Rank = 1;


            foreach (VehicleObstacle v in Racers)
            {
                if (v.walker.progress + v.curDistfromWalker > playerLaneWalker.progress)
                {
                    Rank++;
                }
            }


            if (currentRank != Rank)
            {
                currentRank = Rank;



                if (RacerRankTextUI != null)
                {

                    RacerRankTextUI.text = currentRank.ToString();

                    if (currentRank == 1)
                    {
                        RacerRankTextUI.text += "st";
                    }
                    else if (currentRank == 2)
                    {
                        RacerRankTextUI.text += "nd";
                    }
                    else if (currentRank == 3)
                    {
                        RacerRankTextUI.text += "rd";
                    }
                    else
                    {
                        RacerRankTextUI.text += "th";
                    }



                }
            }
        }

       
	}

}
