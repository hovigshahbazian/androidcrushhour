﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMultiTransform : MonoBehaviour {

    public Transform xTransform;
    //public Transform yTransform;
    [HideInInspector]
    public Transform zTransform;
    public bool SmoothFollow;
    public bool SmoothRotate;
    public Vector3 offset;
    public float followSpeed;
    public float rotateSpeed;

    private Vector3 myPos;
    private Vector3 myEulerRot;

    private Transform myT;
    private Transform xT;
    private Transform yT;
    private Transform zT;

    public bool hasTarget = false;

  
    public float EyeXRot =  25.0f;
	// Use this for initialization


    void Awake()
    {

        SetUp(GameObject.FindGameObjectWithTag("PlayerLaneWalker").transform);

    }
    void Start () {

    }
	
	// Update is called once per frame
	void LateUpdate () {

        if (hasTarget)
        {


            myPos = new Vector3(xT.position.x, yT.position.y, zT.position.z) + (xT.right * offset.x) + (yT.up * offset.y) + (zT.forward * offset.z);

            myEulerRot = new Vector3(xT.eulerAngles.x + EyeXRot, xT.eulerAngles.y, myT.eulerAngles.z);

            if (SmoothFollow)
            {
                myT.position = Vector3.Lerp(myT.position, myPos, Time.deltaTime * followSpeed);
            }
            else
            {
                myT.position = myPos;
            }


            if (SmoothRotate)
            {

                myT.rotation  = Quaternion.Lerp(myT.rotation, Quaternion.Euler(myEulerRot), Time.deltaTime * rotateSpeed);

            }
            else
            {
                
                myT.eulerAngles = myEulerRot;
            }
            
            
            
        }

    }

    public void SetUp(Transform tran)
    {
        xTransform = tran;
        zTransform = tran;
    }

    public void AssignTarget(Transform yTransform)
    {
        xT = xTransform;
        yT = yTransform;
        zT = zTransform;

        myT = transform;
        hasTarget = true;
    }
}
