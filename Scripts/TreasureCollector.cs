﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureCollector : MonoBehaviour {

    public int TreasuresCollected = 0;
    public float TreasureScore = 0;
    public float TreasureMultiplier = 1;
    public TreasueCollectUI treasureUI;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Collect(float value)
    {
        TreasureScore += value;
        if (treasureUI != null) { treasureUI.SetText(value); }
        TreasuresCollected++;
      
    }



}
