﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderCarLocation : MonoBehaviour {

    public Slider slider;
    public int XRange;
    private RectTransform rect;
	// Use this for initialization
	void Start () {
        rect = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
       rect.anchoredPosition = new Vector2(XRange * slider.value, rect.anchoredPosition.y);
	}
}
