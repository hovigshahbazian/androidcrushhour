using UnityEngine;
using System.Collections;


public class SM : MonoBehaviour {

    public enum Sound { Car_Skid, Car_Crash, Accelerating, Menu_Select, Siren }

	static public bool soundchange;

	public static float audioVolume;
	public AudioClip[] soundAudio = new AudioClip[30];

	AudioSource sound;

    public static bool audioon;

    public static SM instance;

    void Awake () {

            if (instance == null)
            {
                DontDestroyOnLoad(gameObject);
                instance = this;
                sound = GetComponent<AudioSource>();
        }
            else if (instance != this)
            {
                Destroy(gameObject);
            }

       
    }

    void Start()
    {
        audioVolume = GetSoundPrefs();
        audioon = true;
        SM.instance.sound.volume = SM.audioVolume;
    }


    public float GetSoundPrefs()
    {
        return PlayerPrefs.GetFloat("soundvolumechangevalue");
    }


    public void SetSoundPrefs(float f)
    {
  
        PlayerPrefs.SetFloat("soundvolumechangevalue", f);
        audioVolume = GetSoundPrefs();
    }


    public void StopSounds()
    {
        instance.sound.Stop();
    }

    public void AudioOff()
    {

        if (audioon)
        {
            audioon = false;
          
            SM.audioVolume = 0;
        }
        else
        {
            audioon = true;
            
            SM.audioVolume = 1f;
        }
        SM.soundchange = true;
    }


 

    public void PlaySound(Sound sound)
    {
        SM.instance.sound.Stop();
        SM.instance.sound.clip = soundAudio[(int)sound];
        SM.instance.sound.volume = SM.audioVolume;
        SM.instance.sound.Play();
    }

    public void PlaySoundOnce(Sound sound)
    {
        SM.instance.sound.Stop();
        SM.instance.sound.clip = soundAudio[(int)sound];
        SM.instance.sound.volume = SM.audioVolume;
        SM.instance.sound.PlayOneShot(soundAudio[(int)sound]);
    }



    public void PlayDelayedSound(int sound, float time)
    {
        StartCoroutine(Play(sound, time,false));
    }


    public void PlayDelayedLoopingSound(int sound, float time)
    {
        StartCoroutine(Play(sound, time, true));
    }


    IEnumerator Play(int soundIndex, float time, bool loop)
    {
        yield return new WaitForSeconds(time);
        SM.instance.sound.Stop();
        SM.instance.sound.loop = loop;
        SM.instance.sound.clip = soundAudio[soundIndex];
        SM.instance.sound.volume = SM.audioVolume;
        SM.instance.sound.Play();
    }


    IEnumerator PlayLooping(int soundIndex, float time)
    {
        SM.instance.sound.clip = soundAudio[soundIndex];
        SM.instance.sound.volume = SM.audioVolume;
        SM.instance.sound.Play();
        yield return new WaitForSeconds(time);
        SM.instance.sound.Stop();
       
        
    }

    void Update () {
	

	if (soundchange) {
		soundchange=false;
            SM.instance.sound.volume=SM.audioVolume;
	}

} 


}


