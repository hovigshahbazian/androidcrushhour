using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PupDisplay : MonoBehaviour
{
    //static public bool Active;


   
    public PlayerCarController.PowerUps type;
	public Image image;
    public string eventNameDisplay;
    public string eventNameClose;

    private void Start()
    {
        if (eventNameDisplay != null)
            EventManager.StartListening(eventNameDisplay, DisplayPowerUp);
        if(eventNameClose != null)
            EventManager.StartListening(eventNameClose, ClosePowerUp);

        image.enabled = false;
    }


    public void DisplayPowerUp()
    {
        image.enabled = true;
    }

    public void ClosePowerUp()
    {
        image.enabled = false;
    }

}

