﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelSelectWheel : MonoBehaviour {

    public ScrollRect scrollRect;
    private RectTransform rectTransform;
	// Use this for initialization
	void Start () {
        rectTransform = GetComponent<RectTransform>();
        
	}
	
	// Update is called once per frame
	void Update () {
        rectTransform.localRotation = Quaternion.AngleAxis(scrollRect.horizontalScrollbar.value* 720, Vector3.forward);
    }
}
