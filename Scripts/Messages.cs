using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Messages : MonoBehaviour
{



    public string message;
    private Text text;


    void Start()
    {


        EventManager.StartListening("RoadRageGrab", RoadRage);
        EventManager.StartListening("AmbulanceGrab", Ambulance);
        EventManager.StartListening("SpeedUpGrab", SpeedUP);
        EventManager.StartListening("SlowGrab", Slow);
        EventManager.StartListening("GhostGrab", Ghost);

        EventManager.StartListening("RoadRageEnd", NoMessage) ;
        EventManager.StartListening("AmbulanceEnd", NoMessage);
        EventManager.StartListening("SpeedUpEnd", NoMessage);
        EventManager.StartListening("SlowEnd", NoMessage);
        EventManager.StartListening("GhostEnd", NoMessage);

        message = "";
        text = GetComponent<Text>();

    }



    void Update()
    {
        text.text = message;
      
    
    }

    public void SetMessage(string str)
    {
        message = str;
    }


    public void NoMessage()
    {
        message = "";
    }


    public void Ambulance()
    {
        message = "Ambulance!!";
    }

    public void RoadRage()
    {
        message = "Road Rage!!";
    }

    public void SpeedUP()
    {
        message = "Speed Up!!";
    }

    public void Slow()
    {
        message = "Slow!!";
    }

    public void Ghost()
    {
        message = "Ghost!";
    }
}
