﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialOption : MonoBehaviour {

    private Toggle toggle;
    private GM gm;

	// Use this for initialization
	void Start () {
        gm = GameObject.Find("GM").GetComponent<GM>();
        toggle = GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(ShowTutorial);
        toggle.isOn = !GameDataManager.instance.loadedLevelDataMap[gm.LevelID].TutorialViewed;

    }
	
	// Update is called once per frame


    public void ShowTutorial(bool isShown)
    {
        GameDataManager.instance.loadedLevelDataMap[gm.LevelID].TutorialViewed = !isShown;
        GameDataManager.instance.SaveLevel(gm.LevelID);

    }
}
