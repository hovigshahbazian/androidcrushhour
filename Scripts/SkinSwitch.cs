﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinSwitch : MonoBehaviour {

    public Material[] Skin;

    public Mesh[] skinMesh;

    private MeshRenderer mesh;
    private MeshFilter meshFilter;

    public bool UsesMaterial = true;
   
    // Use this for initialization
    void Start () {
        mesh = GetComponent<MeshRenderer>();
        meshFilter = GetComponent<MeshFilter>();

        if (UsesMaterial) { 
            SetSkin();
        }else
        {
            SetSkinMesh();
        }

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetSkin()
    {

        if ((int)GameDataManager.instance.playerData.CarSkin < Skin.Length)
        {
            mesh.material = Skin[(int)GameDataManager.instance.playerData.CarSkin];
        }
       

    }

    public void SetSkinMesh()
    {
        if ((int)GameDataManager.instance.playerData.CarSkin < skinMesh.Length)
        {
            meshFilter.mesh = skinMesh[(int)GameDataManager.instance.playerData.CarSkin];
        }
    }


}
