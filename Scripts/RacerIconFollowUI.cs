﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RacerIconFollowUI : MonoBehaviour {


    public float ReleativeX = 0;
    public float Distance = 400f;
    public VehicleObstacle vehicle;
    public PlayerCarController player;
    public RectTransform rectTrans;
    public Text text;
    private float distBtwPlyr;
	// Use this for initialization
	void Start () {
        EventManager.StartListening("PlayerSpawn", GetPlayerInfo);
     
	}
	
	// Update is called once per frame
	void Update () {


        ReleativeX = vehicle.RelativeX;
        rectTrans.localPosition =  new Vector3(ReleativeX * Distance, rectTrans.localPosition.y, rectTrans.localPosition.z );
        distBtwPlyr = (player.walker.currentSpline.GetPoint(player.walker.progress).z - vehicle.walker.currentSpline.GetPoint(vehicle.walker.progress + vehicle.curDistfromWalker).z);
        if (player != null)
            text.text = distBtwPlyr.ToString("00.00");


        if (distBtwPlyr < 0)
        {
             rectTrans.gameObject.SetActive(false);
            text.enabled = false;
        }
        else
        {
            rectTrans.gameObject.SetActive(true);
            text.enabled = true;
        }

	}

    public void GetPlayerInfo()
    {
         player = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerCarController>(); ;
    }
}
