using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Speed : MonoBehaviour {
    [HideInInspector]
    public SplineWalker walker;
    private Text speedText;
    private float speed;
    public bool Active;
    // Use this for initialization
    void Start () {

        walker = GameObject.FindGameObjectWithTag("PlayerLaneWalker").GetComponent<SplineWalker>();

        speedText = GetComponent<Text>();
        EventManager.StartListening("StartLevel", StartSpeed);
        EventManager.StartListening("PlayerCrash", EndSpeed);
        EventManager.StartListening("StageComplete", EndSpeed);
    }


	void Update () {
        if (Active)
        {
            speed = walker.curVelocity;

            speedText.text = "Speed: " + speed.ToString("0.0");
        }
	}


    public void StartSpeed()
    {
        Active = true;
    }


    public void EndSpeed()
    {
        Active = false;
    }
}
