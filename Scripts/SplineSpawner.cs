﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineSpawner : MonoBehaviour {
    public SplineWalker splineWalker;
    public GameObject[] Splines;

    public Vector3 SpawnTranslateVector;
    public Vector3 CurrentSpawnLocation;
    public int currentIndex = 0;


    void Awake()
    {
        splineWalker = GameObject.Find("PlayerLaneWalker").GetComponent<SplineWalker>();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		


        if(splineWalker.GetCurrentProgress() >= 1)
        {
            

            currentIndex =  (currentIndex+1) % Splines.Length;
   
            CurrentSpawnLocation += SpawnTranslateVector;
            Splines[currentIndex].transform.position = new Vector3(Splines[currentIndex].transform.position.x, Splines[currentIndex].transform.position.y, CurrentSpawnLocation.z);

            SwitchSpline(Splines[currentIndex].GetComponent<BezierSpline>());
           
        }



	}

 

    public void SwitchSpline(BezierSpline otherSpline)
    {
        splineWalker.SetSpline(otherSpline,0);
        splineWalker.StartWalker();
        Debug.Log("Switching");
    }


}
