﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
public class NetworkEventManager : NetworkBehaviour {

    private Dictionary<string, UnityEvent> eventDictionary;

    private static NetworkEventManager networkEventManager;

    public static NetworkEventManager instance
    {
        get
        {
            if (!networkEventManager)
            {
                networkEventManager = FindObjectOfType(typeof(NetworkEventManager)) as NetworkEventManager;

                if (!networkEventManager)
                {
                    Debug.LogError("There needs to be one active Network EventManger script on a GameObject in your scene.");
                }
                else
                {
                    networkEventManager.Init();
                }
            }

            return networkEventManager;
        }
    }



    void Init()
    {
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, UnityEvent>();
        }
    }

    public static void StartListening(string eventName, UnityAction listener)
    {
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(string eventName, UnityAction listener)
    {
        if (networkEventManager == null) return;
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName)
    {
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke();
        }
    }
}
