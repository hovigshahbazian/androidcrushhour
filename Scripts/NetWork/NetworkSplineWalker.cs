﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class NetworkSplineWalker : NetworkBehaviour {

    [SerializeField]
    private string ownerConnStr;
    private NetworkPlayer NetOwner;
    private NetworkIdentity networkIdentity;
    public BezierSpline currentSpline;
    public GameObject StartingLane;
    public float distanceTraveledInCurrentLane;
    public float totalDistanceTraveled;
    public bool lookForward;
    public bool FindStartLaneOnWake = true;
    public SplineWalkerMode mode;

    public float distance;
    [SyncVar]
    public float progress;

 

    private bool goingForward = true;

    public float Speed;
    public float curVelocity;
    public float MaximumVelocity;
    public float MinimumVelocity;

    public float Accleration;
    public bool Slowing;

    public int Point;
    public int inputProgress;
    [SyncVar]
    public bool Active;
    public bool Switching = false;

    public Vector3 SplineVelocity;
    public float SplineVelocityMagnitude;


    public float lerpRate = 5;
    public float predcitionValue = 0.001f;
    private float lastPos;
    private float threshold = 0.5f;

    public override void OnStartClient()
    {
   
    }

    public override void OnStartAuthority()
    {
        
        NetworkEventManager.StartListening("StartLevel", StartWalker);
       
        Debug.Log( gameObject.name + " - On Authority");
        NetworkEventManager.TriggerEvent("PlayerWalkerAuthorized");
        SetUp();
        Place();
    }

    public override void OnStartLocalPlayer()
    {

    }

    void Start()
    {

        Debug.Log( gameObject.name + " - Start");

        if (!hasAuthority)
        {
            SetUp();
            Place();
            return;
        } 
     

    }


    public void SetUp()
    {
        if (FindStartLaneOnWake)
            StartingLane = GameObject.FindGameObjectWithTag("StartLane");

        if (StartingLane != null)
        {
            currentSpline = StartingLane.GetComponent<BezierSpline>();
            distance = currentSpline.FindMagnitudeOfSpline();
        }
    }


    public void Place()
    {

        Vector3 position = currentSpline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            transform.LookAt(position + currentSpline.GetDirection(progress));
        }

    }

    public void SetCurrentLane(BezierSpline spline)
    {
        currentSpline = spline;
        distance = currentSpline.FindMagnitudeOfSpline();
    }

    public void SetSpline(BezierSpline otherspline, float progressPoint)
    {
        currentSpline = otherspline;
        distance = otherspline.FindMagnitudeOfSpline();
        distanceTraveledInCurrentLane = progressPoint * distance;

    }


    public void CopyWalkerProgress(SplineWalker walker)
    {
        distanceTraveledInCurrentLane = walker.distanceTraveledInCurrentLane;
        progress = walker.GetCurrentProgress();
    }

    public void StartWalker()
    {
        Active = true;
        goingForward = true;
        Debug.Log("Walker Started");
    }
   

    private void Update()
    {
        if (!hasAuthority)
        {
            return;
        }
        
        if (Active)
        {
            if (goingForward)
            {

                if (Slowing)
                {
                    curVelocity = Mathf.Clamp(curVelocity -= Accleration, MinimumVelocity, MaximumVelocity);
                }
                else
                {
                    curVelocity = Mathf.Clamp(curVelocity += Accleration, MinimumVelocity, MaximumVelocity);
                }

                distanceTraveledInCurrentLane += (curVelocity * Time.deltaTime);
                totalDistanceTraveled += (curVelocity * Time.deltaTime);



                progress = distanceTraveledInCurrentLane / distance;
                //Debug.Log(progress + " Updated propgress");



                SplineVelocity = currentSpline.GetVelocity(progress);
                SplineVelocityMagnitude = SplineVelocity.magnitude;

                if (progress > 1f)
                {

                    if (mode == SplineWalkerMode.Once)
                    {
                        progress = 1f;
                        Active = false;
                        goingForward = false;
                    }
                    else if (mode == SplineWalkerMode.Loop)
                    {
                        progress -= 1f;
                    }
                    else
                    {
                        progress = 2f - progress;
                        goingForward = false;
                    }
                }




            }
            else
            {
                /*
                progress -= Time.deltaTime / duration;
                if (progress < 0f)
                {
                    progress = -progress;
                    goingForward = true;
                }*/
            }

            Vector3 position = currentSpline.GetPoint(progress);
            transform.localPosition = position;

            if (lookForward)
            {
                transform.LookAt(position + currentSpline.GetDirection(progress));
            }
        }

        /*
        if (Input.GetKeyDown(KeyCode.P))
        {

            progress = (float)Point / (float)currentSpline.ControlPointCount;



            Vector3 position = currentSpline.GetPoint(progress);
            transform.localPosition = position;
            Debug.Log("Move to: " + progress);

        }


        if (Input.GetKeyDown(KeyCode.O))
        {

            progress = (float)Point / (float)currentSpline.ControlPointCount;



            Vector3 position = currentSpline.GetPoint(progress);
            transform.localPosition = position;


        }

    */

    }

    public void FixedUpdate()
    {
        LerpPosition();
        TransmitPosition();
    }

    void LerpPosition()
    {
        
       if (!hasAuthority)
       {

               Vector3 position = currentSpline.GetPoint(progress+predcitionValue);

               position = Vector3.Lerp(transform.position, position, Time.deltaTime * lerpRate);

               transform.localPosition = position;

               if (lookForward)
               {
                   transform.LookAt(position + currentSpline.GetDirection(progress+predcitionValue));
               }

       }
    }

    [Command]
    void Cmd_ProvidePositionToServer(float pos)
    {
        progress = pos;
    }

    [ClientCallback]
    void TransmitPosition()
    {
        if (hasAuthority && (progress -  lastPos) > threshold)
        {
            Cmd_ProvidePositionToServer(progress);
            lastPos = progress;
        }
    }


    public float GetCurrentProgress()
    {
        return distanceTraveledInCurrentLane / distance;
    }


    public void OnGUI()
    {
        if (!hasAuthority) {
            GUI.TextArea(new Rect(new Vector2(0, 200), new Vector2(100, 100)), " Non-authority walker progress: " + progress);
        }


    }








}
