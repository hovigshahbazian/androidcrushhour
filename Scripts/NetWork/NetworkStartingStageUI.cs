﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetworkStartingStageUI : NetworkBehaviour {
    public GameObject StartingUI;
    public Button StartButton;
    private NetworkGM GameMananger;
    public float timer = 10;
    // Use this for initialization
    void Start()
    {

        GameMananger = GameObject.Find("GM").GetComponent<NetworkGM>();

        if (StartButton != null)
        {
            
            StartButton.onClick.AddListener(GameMananger.StartStage);
            StartButton.onClick.AddListener(CloseStartUI);
        }

    }

    // Update is called once per frame
    void Update()
    {
      
    }





    public void CloseStartUI()
    {
        StartingUI.gameObject.SetActive(false);
    }

 
}
