﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkGM : NetworkBehaviour {
    public enum GameState { Intro, Playing, Failed, Complete }
    [SyncVar]
    public GameState CurGameState;
 
 
    //private bool QuestOne = false;
    //private bool QuestTwo = false;
    //private bool QuestThree = false;


    public string QuestOneCompleteEvent;
    public string QuestTwoCompleteEvent;
    public string QuestThreeCompleteEvent;

    public int CrushCansCollected = 0;
    public int LevelID = 0;


    public static bool accelerometer = false;
    public static bool vibrate = false;

    public int target = 60;
    public SM soundManager;

    public override void OnStartClient()
    {

        Debug.Log(gameObject.name + " - On Start Client");
        

    }




    void Start()
    {
        


        NetworkEventManager.StartListening("PlayerCrash", GameOver);
        NetworkEventManager.StartListening("StageComplete", StageComplete);
        NetworkEventManager.StartListening("CollectCan", CollectCrushCan);
       

        if (QuestOneCompleteEvent != "")
            NetworkEventManager.StartListening(QuestOneCompleteEvent, CompleteQuestOne);

        if (QuestTwoCompleteEvent != "")
            NetworkEventManager.StartListening(QuestTwoCompleteEvent, CompleteQuestTwo);

        if (QuestThreeCompleteEvent != "")
            NetworkEventManager.StartListening(QuestThreeCompleteEvent, CompleteQuestThree);

        CurGameState = GameState.Intro;

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        

    }

    void StageComplete()
    {
        CurGameState = GameState.Complete;

        GameDataManager.instance.playerData.CrushCans += CrushCansCollected;

    }

    void GameOver()
    {
        CurGameState = GameState.Failed;
        soundManager.PlaySound(SM.Sound.Car_Crash);
        GameDataManager.instance.playerData.CrushCans += CrushCansCollected;
    }



    public void Restart()
    {
        NetworkEventManager.TriggerEvent("RestartScene");
        NetworkEventManager.TriggerEvent("Unpause");
    }

    public void ReturnToMainMenu()
    {
        NetworkEventManager.TriggerEvent("GoToMainMenu");
        NetworkEventManager.TriggerEvent("Unpause");
    }

    void Update()
    {

        if (target != Application.targetFrameRate)
        {
            Application.targetFrameRate = target;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            NetworkEventManager.TriggerEvent("PlayerCrash");
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }


    }

    public void CompleteQuestOne()
    {
        //QuestOne = true;
    }
    public void CompleteQuestTwo()
    {
        //QuestTwo = true;
    }
    public void CompleteQuestThree()
    {
        //QuestThree = true;
    }

    public void CollectCrushCan()
    {
        CrushCansCollected++;
    }


    public void StartStage()
    {
      

       // Debug.Log("StartStage - Server");


       // NetworkEventManager.TriggerEvent("StartLevel");
      //  CurGameState = GameState.Playing;



        //if (!isServer)
        //    return;

        RpcStartStage();
    
    }


    [ClientRpc]
    public void RpcStartStage()
    {
        Debug.Log("StartStage - client");
        NetworkEventManager.TriggerEvent("StartLevel");
        CurGameState = NetworkGM.GameState.Playing;
    }
}
