﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class NetworkPlayerCarController : NetworkBehaviour {
    public enum PowerUps { None = -1, Ambulance = 1, Slow = 2, Speed = 3, Fog = 4, RoadRage = 5, Diamond = 6, Ghost = 7 }
    public enum BumpMode { Contact, Horizontal, ForwardOnly, LeftOnly, RightOnly, BackOnly }
    public BumpMode currentBumpMode = BumpMode.Contact;
    public NetworkSplineWalker walker;
    private RoadRageController rrc;
    public PowerUps CurrentPowerUp;
    [SyncVar]
    public bool Active;
    private float MinX = -5f;
    private float MaxX = 5f;
    public float relativeX = 0;
    private float relativeXChange = 0;
    private float bumpVelocity = 0;
    public float relativeY = 0;
    public float dropOff = 0;
    public bool PlayerCrashed;
    public float SpeedRating = 1;
    public Vector3 velocity = Vector3.zero;
    public float GroundLevel = 0;
    public bool isFalling = false;
    public bool isJumping = false;
    public bool isTouchingFloor = false;
    private Transform myTransform;

    [SerializeField]
    private Transform walkerTransform;
    [SyncVar]
    public Vector3 WalkerPosition;
    [SyncVar]
    public float ClientSplineProgress;
    [SyncVar]
    public Vector3 ClientSplinePosition;

    public bool CollisionEventsEnabled;
    public float CarBumpSlowValue;
    public bool SlowOnCarBump;
    public float BumpRecoverTime;
    private bool Bumped;
    private NetworkFollowMultiTransform followTrans;
    private Vector3 netStartPos;
    private NetworkTransform netTransform;
    public GameObject playerLane;
    private GameObject curplayerlane;

    public float lerpRate = 5;




    public override void OnStartClient()
    {
        Debug.Log(gameObject.name + " - On Start client");
    }

    public override void OnStartAuthority()
    {
        Debug.Log(gameObject.name + " - On Start Authority");
        NetworkEventManager.StartListening("PlayerWalkerAuthorized", FindPlayerWalker);
        CmdSpawnLaneWalker();
    }


    public override void OnStartLocalPlayer()
    {
        Debug.Log(gameObject.name + " - On Start Local player");
        
    }


    [Command]
    public void CmdSpawnLaneWalker()
    {
        curplayerlane = Instantiate(playerLane);
        Debug.Log("Server command spawn walker");
        NetworkServer.SpawnWithClientAuthority(curplayerlane, connectionToClient);    
    }

    public void FindPlayerWalker()
    {
        walker = null;
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("PlayerLaneWalker"))
        {
            Debug.Log("Looking at " + g.name);
            if (g.GetComponent<NetworkSplineWalker>().hasAuthority)
            {
                walker = g.GetComponent<NetworkSplineWalker>();
                Debug.Log("Found walker " + g.name);
            }
        }
      
     
        Debug.Log("Player Finding Lane walker");
        if(walker != null)
        {
            Debug.Log("Walker Found");
            SetUpWalker();
            NetworkEventManager.TriggerEvent("WalkerFound");
        }
        
       
    }

    public void SetUpWalker()
    {
        Debug.Log("Setting up Walker for player");
        walkerTransform = walker.transform;
        GroundLevel = walker.transform.position.y;

        Debug.Log("Setting Walker for Camera");
        followTrans = Camera.main.GetComponent<NetworkFollowMultiTransform>();
        followTrans.SetUp(walker.transform);
        followTrans.AssignTarget(transform);
    }

    void Start()
    {
        myTransform = transform;
        //  FindPlayerWalker();

        // Debug.Log("Network Active " + NetworkServer.active); ;
        if (!isLocalPlayer)
        {
            
            return;
        }
        



        Debug.Log( gameObject.name + " - Start");
      

      
      
        netStartPos = transform.position;
    
        relativeX = netStartPos.x;
     

        rrc = GetComponent<RoadRageController>();
        CurrentPowerUp = PowerUps.None;

        NetworkEventManager.StartListening("StartLevel", Activate);
        NetworkEventManager.StartListening("PlayerCrash", Stopped);
        NetworkEventManager.StartListening("AmbulanceEnd", ResetPowerState);

        NetworkEventManager.TriggerEvent("PlayerSpawn");
    }

    public void Activate()
    {  
        Debug.Log("Activated");
        Active = true;
    }

 


    void Update()
    {
       
        if (!isLocalPlayer)
        {
           
            return;
        }

       

        if (Active)
        {
            if (Input.GetButtonDown("Jump"))
            {
                Jump(25f);
            }


            if (!isTouchingFloor)
            {

                dropOff -= Time.deltaTime;
                velocity.y += dropOff;

                if (velocity.y < 0)
                {
                    isFalling = true;
                    isJumping = false;
                }

            }

            if (myTransform.position.y < GroundLevel && !isJumping)
            {
                myTransform.position = new Vector3(walkerTransform.position.x + relativeX, GroundLevel, walkerTransform.position.z);
                isTouchingFloor = true;
                dropOff = 0f;
                velocity = Vector3.zero;
                isFalling = false;
            }

            myTransform.position += velocity * Time.deltaTime;
        }

    }

   


    void FixedUpdate()
    {

        if (!isLocalPlayer)
        {
          //  Debug.Log("Set non Local player position");
         //   myTransform.position = Vector3.Lerp(myTransform.position,WalkerPosition, Time.deltaTime * lerpRate);
            return;
        }


        if (Active && !PlayerCrashed)
        {
            GroundLevel = walkerTransform.position.y;

            Vector3 down = myTransform.TransformPoint(Vector3.down * 10);

            Debug.DrawLine(myTransform.position, down, Color.blue);

            if (!Physics.Raycast(myTransform.position, down, 10) && !isJumping)
            {
                isFalling = true;
                isTouchingFloor = false;
            }
            else
            {
                // Debug.Log("Floor Exists");
            }
        }
    }

    void LateUpdate()
    {
        if (!isLocalPlayer)
        {
         
            return;
        }

        if (Active && !PlayerCrashed)
        {
            if (!SteeringWheel.isHold)
            {
                SteeringWheel.input = 0;
            }



            if (GM.accelerometer)
            {
                relativeXChange = ((Input.acceleration.x + bumpVelocity) * SpeedRating * Time.deltaTime);
            }
            else
            {
                relativeXChange = ((SteeringWheel.input + bumpVelocity) * SpeedRating * Time.deltaTime);
            }

            relativeX = Mathf.Clamp(relativeX += relativeXChange, MinX, MaxX);

            if (walker.Active)
            {
                WalkerPosition = new Vector3(walkerTransform.position.x, myTransform.position.y, walkerTransform.position.z) + walkerTransform.right * relativeX;

                myTransform.position = WalkerPosition;


                myTransform.LookAt(myTransform.position + walker.currentSpline.GetDirection(walker.progress));

                ClientSplineProgress = walker.progress + 0.003f;
                ClientSplinePosition = walker.currentSpline.GetPoint(ClientSplineProgress) + walkerTransform.right * relativeX;


            }


        }


    }
    void OnCollisionStay(Collision col)
    {
        if (!isLocalPlayer)
        {
            return;
        }

        if (!CollisionEventsEnabled)
        {
            VehicleObstacle obs = col.gameObject.GetComponentInParent<VehicleObstacle>();

            if (!rrc.Active)
            {
                Vector3 contact = transform.InverseTransformPoint(col.contacts[0].point);


                SendMessage("Damage");


                CarBump();
                CarBumpContact(-contact, 0.000f, 1f);
                // Debug.Log("Bump");

                if (obs != null)
                {
                    switch (currentBumpMode)
                    {

                        case BumpMode.Contact:
                            obs.CarBumpContact(contact, 0.006f, 10f);
                            break;
                        case BumpMode.ForwardOnly:
                            obs.CarBumpForward(0.006f);
                            break;
                        case BumpMode.RightOnly:
                            obs.CarBumpRightward(10f);
                            break;
                        case BumpMode.LeftOnly:
                            obs.CarBumpLeftward(10f);
                            break;
                        case BumpMode.BackOnly:
                            obs.CarBumpBackwards(0.006f);
                            break;
                    }
                }
            }
            else
            {

                EventManager.TriggerEvent("RoadRageHit");

                Vector3 contact = transform.InverseTransformPoint(col.contacts[0].point);
                rrc.RoadRageHitAnim.transform.localPosition = new Vector3(contact.x, 1.7f, contact.z);

                obs.Crash();
            }
        }
    }

    public void Jump(float JumpPower)
    {
        if (isTouchingFloor)
        {
            velocity = (Vector3.up * JumpPower);
            isTouchingFloor = false;
            isJumping = true;
            Debug.Log("Jumped");
        }
    }


    public void Stopped()
    {
        PlayerCrashed = true;
        walker.Active = false;
        CollisionEventsEnabled = false;
    }



    public void ResetPowerState()
    {
        CurrentPowerUp = PowerUps.None;
    }

    public void CarBump()
    {
        if (SlowOnCarBump)
        {
            StartCoroutine(ApplySlowDown());
        }

        Debug.Log("CarBump");
    }



    IEnumerator ApplySlowDown()
    {
        SlowDown();
        CurrentPowerUp = PowerUps.Slow;
        yield return new WaitForSeconds(BumpRecoverTime);
        CurrentPowerUp = PowerUps.None;
        Revert();
    }

    public void SlowDown()
    {
        EventManager.TriggerEvent("SlowGrab");
        walker.curVelocity -= CarBumpSlowValue;
        walker.MaximumVelocity -= CarBumpSlowValue;
    }


    public void Revert()
    {
        EventManager.TriggerEvent("SlowEnd");
        walker.curVelocity += CarBumpSlowValue;
        walker.MaximumVelocity += CarBumpSlowValue;
    }


    public void CarBumpForward(float dist)
    {
        StartCoroutine(CarBumpFor(dist));
    }

    public void CarBumpBackwards(float dist)
    {
        StartCoroutine(CarBumpBack(dist));
    }

    public void CarBumpLeftward(float dist)
    {
        StartCoroutine(CarBumpLeft(dist));
    }
    public void CarBumpRightward(float dist)
    {
        StartCoroutine(CarBumpRight(dist));
    }

    IEnumerator CarBumpFor(float forwardBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;


            walker.curVelocity += forwardBumpChange;
            walker.MaximumVelocity += forwardBumpChange;
            yield return new WaitForSeconds(0.1f);
            walker.curVelocity -= forwardBumpChange;
            walker.MaximumVelocity -= forwardBumpChange;
            yield return new WaitForSeconds(0.1f);
            Bumped = false;
        }
    }

    IEnumerator CarBumpBack(float forwardBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;


            walker.curVelocity -= forwardBumpChange;
            walker.MaximumVelocity -= forwardBumpChange;
            yield return new WaitForSeconds(0.1f);
            walker.curVelocity += forwardBumpChange;
            walker.MaximumVelocity += forwardBumpChange;
            yield return new WaitForSeconds(0.1f);


            Bumped = false;
        }
    }

    IEnumerator CarBumpRight(float sideBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;

            bumpVelocity += sideBumpChange;
            yield return new WaitForSeconds(0.1f);
            bumpVelocity += -sideBumpChange;

            Bumped = false;
        }
    }


    IEnumerator CarBumpLeft(float sideBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;

            bumpVelocity += -sideBumpChange;

            yield return new WaitForSeconds(0.1f);
            bumpVelocity += sideBumpChange;
            Bumped = false;
        }
    }

    public void CarBumpContact(Vector3 contact, float forwardDist, float HorizontalDist)
    {
        if (contact.z > 1)
            CarBumpForward(forwardDist);

        if (contact.z < -1)
            CarBumpBackwards(forwardDist);

        if (contact.x > 1)
            CarBumpRightward(HorizontalDist);

        if (contact.x < -1)
            CarBumpLeftward(HorizontalDist);
    }
}
