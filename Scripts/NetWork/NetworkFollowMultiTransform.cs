﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkFollowMultiTransform : NetworkBehaviour {
    public Transform xTransform;
    //public Transform yTransform;
    [HideInInspector]
    public Transform zTransform;
    public bool SmoothFollow;
    public bool SmoothRotate;
    public Vector3 offset;
    public float followSpeed;
    public float rotateSpeed;

    private Vector3 myPos;
    private Vector3 myEulerRot;

    private Transform myT;
    [SerializeField]
    private Transform xT;
    private Transform yT;
    [SerializeField]
    private Transform zT;

    public bool hasTarget = false;
    public Vector3 EyeXRot;
    // Use this for initialization
    public override void OnStartClient()
    {

        Debug.Log(gameObject.name + " - On Start Client");
        //NetworkEventManager.StartListening("PlayerWalkerSpawn", SetUp);
      //  SetUp();
    }
   
  
    void Start()
    {
        Debug.Log(" Camera Start ");
    }

    // Update is called once per frame
    void LateUpdate()
    {

        if (hasTarget && xT != null && zT != null)
        {
          

            myPos = new Vector3(xT.position.x, yT.position.y, zT.position.z) + (xT.right * offset.x) + (yT.up * offset.y) + (zT.forward * offset.z);

            myEulerRot = new Vector3(myT.eulerAngles.x, xT.eulerAngles.y, myT.eulerAngles.z);

            if (SmoothFollow)
            {
                myT.position = Vector3.Lerp(myT.position, myPos, Time.deltaTime * followSpeed);
            }
            else
            {
                myT.position = myPos;
            }


            if (SmoothRotate)
            {

                myT.rotation = Quaternion.Lerp(myT.rotation, Quaternion.Euler(myEulerRot), Time.deltaTime * rotateSpeed);

            }
            else
            {

                myT.eulerAngles = myEulerRot;
            }



        }

    }

    public void SetUp(Transform tran)
    {    
        xTransform = tran;
        zTransform = tran;
        Debug.Log("Camera Finding Lane Walker");
    }

    public void AssignTarget(Transform yTransform)
    {
        xT = xTransform;
        yT = yTransform;
        zT = zTransform;

        myT = transform;
        hasTarget = true;
        Debug.Log("Assign Camera Target");
    }

    public void OnGUI()
    {
      //  if(hasTarget && xT != null && zT != null)
         //   GUI.TextField(new Rect(new Vector2(200, 200), new Vector2(400, 50)), transform.position.ToString() + " " + xT.gameObject.name + " " + xT.position);

     



         //   GUI.TextField(new Rect(new Vector2(0, 200), new Vector2(200, 50)), " Script is working");

    } 
}
