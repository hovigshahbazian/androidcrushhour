﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkSplineWalkerSpawner : NetworkBehaviour {

    public GameObject playerWalkerPrefab;



    public override void OnStartServer()
    {


    }

  
    public void SpawnLaneWalker()
    {

        var walker = Instantiate(playerWalkerPrefab);
        NetworkServer.Spawn(walker.gameObject);
      
    }




}
