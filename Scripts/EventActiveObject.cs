﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventActiveObject : MonoBehaviour
{

    public string EventName;
    public bool ActiveState;
    public GameObject gameObject;

    // Use this for initialization
    void Start()
    {
        if (EventName != null)
        {
            EventManager.StartListening(EventName, SetActive);
        }
    }



    public void SetActive()
    {
            gameObject.SetActive(ActiveState);
    }
}
