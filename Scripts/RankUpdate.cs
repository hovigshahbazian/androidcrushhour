﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankUpdate : MonoBehaviour {

	public enum RankType { Destruction, Survival, Treasure };

    //public LevelGameData levelData;
    public Stage LevelIndex;
	public RankType rankType;

	public Image[] stars = new Image[3];

	// Use this for initialization
	void Start () {

		switch (rankType) {
		case RankType.Destruction:
			switch (GameDataManager.instance.loadedLevelDataMap[LevelIndex].DestructionRank) {
			case Rank.None:
				break;
			case Rank.First:
				stars [0].color = Color.white;
				break;
			case Rank.Second:
				stars [0].color = Color.white;
				stars [1].color = Color.white;
				break;
			case Rank.Third:
				stars [0].color = Color.white;
				stars [1].color = Color.white;
				stars [2].color = Color.white;
				break;
			}
			break;
		case RankType.Survival:
			switch (GameDataManager.instance.loadedLevelDataMap[LevelIndex].SurvivalRank) {
			case Rank.None:
				break;
			case Rank.First:
				stars [0].color = Color.white;
				break;
			case Rank.Second:
				stars [0].color = Color.white;
				stars [1].color = Color.white;
				break;
			case Rank.Third:
				stars [0].color = Color.white;
				stars [1].color = Color.white;
				stars [2].color = Color.white;
				break;
			}
			break;
		case RankType.Treasure:
			switch (GameDataManager.instance.loadedLevelDataMap[LevelIndex].CrushcanRank) {
			case Rank.None:
				break;
			case Rank.First:
				stars [0].color = Color.white;
				break;
			case Rank.Second:
				stars [0].color = Color.white;
				stars [1].color = Color.white;
				break;
			case Rank.Third:
				stars [0].color = Color.white;
				stars [1].color = Color.white;
				stars [2].color = Color.white;
				break;
			}
			break;

		default:
			break;
		}



	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
