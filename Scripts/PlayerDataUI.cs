﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDataUI : MonoBehaviour {

	public enum Data{ Stars, CarType, Skin, CrushCan };

	//public PlayerGameData playerData;

	public Data type;

	private Text text;
	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();



	}
	
	// Update is called once per frame
	void Update () {

		switch (type) {
		case Data.CarType:
			text.text = GameDataManager.instance.playerData.CarType.ToString ();;
			break;
        case Data.Skin:
            text.text = GameDataManager.instance.playerData.CarSkin.ToString(); ;
            break;
        case Data.Stars:
			text.text = "x" + GameDataManager.instance.playerData.Stars.ToString ("D");
			break;
        case Data.CrushCan:
            text.text = "x" + GameDataManager.instance.playerData.CrushCans.ToString("D");
            break;
                

        }




	}

}
