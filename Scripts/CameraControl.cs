﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {


    private FollowMultiTransform carFollowTransform;

	// Use this for initialization
	void Start () {
        carFollowTransform = GetComponent<FollowMultiTransform>();

        EventManager.StartListening("StageComplete", StopFollowing);
        EventManager.StartListening("StageFailed", StopFollowing);
        EventManager.StartListening("StartFollowing", StartFollowing);
    }
	

    public void StopFollowing()
    {
        carFollowTransform.enabled = false;
    }

    public void StartFollowing()
    {
        carFollowTransform.enabled = true;
    }



	// Update is called once per frame
	void Update () {
		
	}
}
