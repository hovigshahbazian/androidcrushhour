﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundOption : MonoBehaviour {

    public Slider slider;
    // Use this for initialization
    void Start () {
        slider.value = SM.instance.GetSoundPrefs();
        slider.onValueChanged.AddListener(SM.instance.SetSoundPrefs);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
