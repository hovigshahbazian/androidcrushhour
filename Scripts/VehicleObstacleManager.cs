﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleObstacleManager : MonoBehaviour {

    public GameObject[] cars;
    public GameObject[] laneWalkers;
    public GameObject StartingLane;
    public BezierSpline CurrentSpline;
    public GameObject PlayerWalker;
    public float SpawnTimer = 4;
    public float SpawnTime = 4;
    public int MaxSpawnX = 5;
    public int MinSpawnX = -5;
    public float InitialSpeed = 0.003f;
    public float LaneSpeedDiff = 0.001f;
    public bool activeOnStart = true;
    public float MaximumSpeed = 0.006f;
    public float CarAcceleration = 0;

    public bool Active;

    void Awake()
    {
        if (StartingLane == null)
        {
            StartingLane = GameObject.FindGameObjectWithTag("StartLane");
           
        }
        CurrentSpline = StartingLane.GetComponent<BezierSpline>();
        PlayerWalker = GameObject.FindGameObjectWithTag("PlayerLaneWalker");
    }



	// Use this for initialization
	void Start () {

        if(activeOnStart)
            EventManager.StartListening("StartLevel", StartSpawner);

        EventManager.StartListening("StartVehicleSpawner", StartSpawner);
        EventManager.StartListening("AmbulanceEnd", StartSpawner);
        EventManager.StartListening("AmbulanceGrab", PauseSpawner);
        EventManager.StartListening("PlayerCrash", PauseSpawner);
        EventManager.StartListening("SwitchLane", SwitchLane);

        SetWalkers(CurrentSpline);
        
	}
	
    public void SetWalkers(BezierSpline newSpline)
    {
        foreach (GameObject LaneWalker in laneWalkers)
        {
           // if (LaneWalker.GetComponent<SplineWalker>().StartingLane == null)
          //  {
                LaneWalker.GetComponent<SplineWalker>().SetCurrentLane(newSpline);
           // }
        }
    }


	// Update is called once per frame
	void Update () {

        if (Active)
        {
            //select a random car from the pool of cars
            if (SpawnTimer > 0)
            {
                SpawnTimer -= Time.deltaTime;

                if (SpawnTimer < 0)
                {
                    SpawnTimer = SpawnTime;
                    SpawnCar(Random.Range(0, cars.Length - 1));
                }
            }

            InitialSpeed = Mathf.Clamp(InitialSpeed += CarAcceleration * Time.deltaTime, InitialSpeed, MaximumSpeed);
        }


    }


    public void SpawnCar(int i)
    {
     //  Debug.Log("Spawn Car: " + i);
        if (!cars[i].activeInHierarchy)
        {
            cars[i].SetActive(true);
          
            VehicleObstacle vehicle = cars[i].GetComponentInChildren<VehicleObstacle>();

           //  vehicle.walker.SetUp();
            vehicle.walker.SetCurrentLane(CurrentSpline);
            vehicle.walker.CopyWalkerProgress(PlayerWalker.GetComponent<SplineWalker>());
            vehicle.walker.Place();
            
            vehicle.walker.StartWalker();
            vehicle.walker.gameObject.SetActive(true);

            vehicle.RelativeX = (float)Random.Range(MinSpawnX, MaxSpawnX);
            vehicle.Speed = InitialSpeed + ((vehicle.RelativeX) * LaneSpeedDiff);
           // Debug.Log("Spawn");
        }
    }



    public void StartSpawner()
    {
        SpawnTimer = SpawnTime;
        Active = true;
        Debug.Log("StartSpawning");
    }

    public void PauseSpawner()
    {
        SpawnTimer = 0;
        Active = false;
        Debug.Log("PauseSpawning");
    
}


    public void SpawnWalker(float location)
    {

    }

    public void SwitchLane()
    {
        Debug.Log("Switching Lane");
        CurrentSpline = PlayerWalker.GetComponent<SplineWalker>().currentSpline;
        SetWalkers(CurrentSpline);
    }
}
