﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineEvent : MonoBehaviour {


    public SplineWalker walker;
    public string EventName;
    private bool triggered;
    public float progressTrigger = 1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(walker.progress >= progressTrigger && !triggered)
        {
            triggered = true;
            if(EventName != "")
            EventManager.TriggerEvent(EventName);
        }
	}


}
