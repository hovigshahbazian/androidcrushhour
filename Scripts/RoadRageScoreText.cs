﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoadRageScoreText : MonoBehaviour {

	private Text text;
    public Text AnimatedText;
    public RoadRageController rrc;
    private bool SetUp = false;
  
	// Use this for initialization
	void Awake() {

        EventManager.StartListening("PlayerSpawn", SetUpUI);
        EventManager.StartListening("RoadRageHit", SetUpRageHitText);
        EventManager.StartListening("RoadObstacleCrashed", SetUpRageHitObstacleText);

        text = GetComponent<Text> ();
      
	}
	
	// Update is called once per frame
	void Update () {
        if(SetUp && rrc != null)
		text.text = rrc.RoadRageCounter.ToString ();
      
	}

    void SetUpUI()
    {
        SetUp = true;
        rrc = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<RoadRageController>();
    }

    void SetUpRageHitText()
    {
        AnimatedText.text = "+" + rrc.RoadRageCarValue;
    }

    void SetUpRageHitObstacleText()
    {
        AnimatedText.text = "+" + rrc.RoadRageObstacleValue;
    }
}
