﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEnding : MonoBehaviour {

    public GameObject camera;
    public bool ActiveState;
    public bool UseTag;
    public string Tag;

    public void Start()
    {
        camera = GameObject.FindWithTag("MainCamera");
    }

    public void OnTriggerEnter(Collider other)
    {
        if (UseTag)
        {
            if (other.CompareTag(Tag))
            {
                camera.GetComponent<FollowMultiTransform>().enabled = ActiveState;
            }
        }
        else
        {
            camera.GetComponent<FollowMultiTransform>().enabled = ActiveState;
        }

    }
}
