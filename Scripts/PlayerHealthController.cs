﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthController : MonoBehaviour {

    public enum PlayerState { Alive, Dead}
    public bool Crashed = false;
    public int HitPoints;
    public int MaxHealth = 5;
    public float DamageDelay = 1;
    public Animator CrashAnim;    
    [SerializeField]
    private bool hitable = true;
    public bool isInvulnerable;
    // Use this for initialization
    void Start () {

        EventManager.StartListening("RoadRageGrab", MakeInvulnerable);
        EventManager.StartListening("RoadRageEnd", MakeVulnerable); ;
        EventManager.StartListening("AmbulanceGrab", RecoverDamage);
    }
	
	// Update is called once per frame
	void Update () {
		


	}



    public void Damage()
    {
        if (hitable && !isInvulnerable)
        {     
            StartCoroutine(ApplyDamage());

        }
    }

    IEnumerator  ApplyDamage()
    {
        hitable = false;
        HitPoints = Mathf.Clamp(HitPoints - 1, 0, MaxHealth);

        if (HitPoints == 0)
        {
            Crash();
        }
        else
        {
            CrashAnim.SetTrigger("Damage");
        }
        yield return new WaitForSeconds(DamageDelay);
        
        hitable = true;
   
       
    }

    public void MakeInvulnerable()
    {
        isInvulnerable = true;
    }

    public void MakeVulnerable()
    {
        isInvulnerable = false;
    }

    public void RecoverDamage()
    {
        HitPoints = Mathf.Clamp(HitPoints + 1, 0, MaxHealth);
    }

    public void Crash()
    {
        if (!Crashed)
        {
            if (GM.vibrate)
            {
               // Handheld.Vibrate();
            }
            Crashed = true;
            GetComponent<Rigidbody>().isKinematic = false;
            EventManager.TriggerEvent("PlayerCrash");
            CrashAnim.SetTrigger("Crash");
        }
   
    }

}
