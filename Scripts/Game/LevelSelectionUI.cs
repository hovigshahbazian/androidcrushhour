﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelectionUI : MonoBehaviour {

	

    public Button LoadStageButton;
    public Button BackToLevelsButton;


    public GameObject SceneLoadingObject;

    public OptionsMenu optionMenu;
    public RectTransform levelButtonsLocation;
    public RectTransform stageButtonsLocation;

    public GameObject[] spawnedLevelButtons;
    public GameObject SpawnedStageButton;

    private Dictionary<Level,LevelButtonUI> levelSelectButtons =  new Dictionary<Level,LevelButtonUI>();

    private Dictionary<Stage, StageButton> StageSelectButtons = new Dictionary<Stage,StageButton>();



    public Stage currentlySelectedStage = 0;
    public Level currentlySelectedLevel = 0;
    public Dictionary<Stage, string> stageScenes = new Dictionary<Stage, string>();
    public StageStats stageStats;
    public StageInfo stageInfo;



    public Text CurrentLevelName;
    public Text CurrentStageName;


    // Use this for initialization
    void Awake () {
        LoadStageButton.interactable = false;
        BackToLevelsButton.onClick.AddListener(() => BacktoLevelSelection());
        LoadStageButton.onClick.AddListener(() => LoadStage());

        CreateSceneNameList();
        int i = 0;

        //construct Level SelectionUI
     foreach(GameObject g in spawnedLevelButtons)
        {
            LevelButtonUI curLevelbuttonUI = spawnedLevelButtons[i].GetComponent<LevelButtonUI>();
            Level level = curLevelbuttonUI.level;

            levelSelectButtons.Add(curLevelbuttonUI.level, Instantiate(spawnedLevelButtons[i], levelButtonsLocation).GetComponent<LevelButtonUI>());          
            levelSelectButtons[level].Levelbutton.onClick.AddListener(() => SelectLevel(level));
            levelSelectButtons[level].Levelbutton.onClick.AddListener(() => optionMenu.PlayButtonSelection());
            i++;
        }
	}



	// Update is called once per frame
	void Update () {

	
	}

    public void CreateSceneNameList()
    {
        stageScenes.Add(Stage.HighWay_1, "1.1");
        stageScenes.Add(Stage.HighWay_2, "1.2");
        stageScenes.Add(Stage.HighWay_3, "1.3");
        stageScenes.Add(Stage.HighWay_4, "1.4");
        stageScenes.Add(Stage.Coast_1, "2.1");
        stageScenes.Add(Stage.Coast_2, "2.2");
        stageScenes.Add(Stage.Coast_3, "2.3");
        stageScenes.Add(Stage.Coast_4, "2.4");
        stageScenes.Add(Stage.City_1, "3.1");
        stageScenes.Add(Stage.City_2, "3.2");
        stageScenes.Add(Stage.City_3, "3.3");
        stageScenes.Add(Stage.City_4, "3.4");
        stageScenes.Add(Stage.Raceway_1, "4.1");
        stageScenes.Add(Stage.Raceway_2, "4.2");
        stageScenes.Add(Stage.Raceway_3, "4.3");
        stageScenes.Add(Stage.Raceway_4, "4.4");
        stageScenes.Add(Stage.Canyon_1, "5.1");
        stageScenes.Add(Stage.Canyon_2, "5.2");
        stageScenes.Add(Stage.Canyon_3, "5.3");
        stageScenes.Add(Stage.Desert_1, "6.1");
        stageScenes.Add(Stage.Desert_2, "6.2");
        stageScenes.Add(Stage.Desert_3, "6.3");
        stageScenes.Add(Stage.Alpine_1, "7.1");
        stageScenes.Add(Stage.Alpine_2, "7.2");
        stageScenes.Add(Stage.Alpine_3, "7.3");
        stageScenes.Add(Stage.Route_1, "8.1");
        stageScenes.Add(Stage.Route_2, "8.2");
        stageScenes.Add(Stage.Route_3, "8.3");
        stageScenes.Add(Stage.Vegas_1, "9.1");
        stageScenes.Add(Stage.Vegas_2, "9.2");
        stageScenes.Add(Stage.Vegas_3, "9.3");
        stageScenes.Add(Stage.Atlantic_1, "10.1");
        stageScenes.Add(Stage.Atlantic_2, "10.2");
        stageScenes.Add(Stage.Atlantic_3, "10.3");
        stageScenes.Add(Stage.Winter_1, "11.1");
        stageScenes.Add(Stage.Winter_2, "11.2");
       stageScenes.Add(Stage.Winter_3, "11.3");
        stageScenes.Add(Stage.Star_1, "12.1");
        stageScenes.Add(Stage.Star_2, "12.2");
        stageScenes.Add(Stage.Star_3, "12.3");
        stageScenes.Add(Stage.Atlantis_1, "13.1");
        stageScenes.Add(Stage.Atlantis_2, "13.2");
        stageScenes.Add(Stage.Atlantis_3, "13.3");
        stageScenes.Add(Stage.Wipeout_1, "14.1");
        stageScenes.Add(Stage.Wipeout_2, "14.2");
        stageScenes.Add(Stage.Wipeout_3, "14.3");
    }




    public void SelectStage(Stage stage){
		currentlySelectedStage = stage;  
        CurrentStageName.text = currentlySelectedStage.ToString();
        LoadStageButton.interactable = true;
    }

    public void SelectLevel(Level level)
    {
        currentlySelectedLevel = level;
        CurrentLevelName.text = currentlySelectedLevel.ToString();
        levelButtonsLocation.gameObject.SetActive(false);
        stageButtonsLocation.gameObject.SetActive(true);
        int i = 0;
        foreach (Stage s in levelSelectButtons[currentlySelectedLevel].stages)
        {
            Debug.Log(s);

            StageSelectButtons.Add(s,Instantiate(SpawnedStageButton, stageButtonsLocation).GetComponent<StageButton>());
            StageSelectButtons[s].stageScreenShot.sprite = levelSelectButtons[level].StageScreen[i];
            StageSelectButtons[s].stageImage.sprite = levelSelectButtons[level].StageImage[i];
            StageSelectButtons[s].button.onClick.AddListener(() => SelectStage(s));
            StageSelectButtons[s].button.onClick.AddListener(() => optionMenu.PlayButtonSelection());
            StageSelectButtons[s].button.onClick.AddListener(() => stageStats.RefreshStarRanks(s));
            StageSelectButtons[s].button.onClick.AddListener(() => stageInfo.RefreshStageInfo(s));
           // StageSelectButtons[s].Text.text = s.ToString();
            i++;
        }

        BackToLevelsButton.gameObject.SetActive(true);

    }

    public void BacktoLevelSelection()
    {
        BackToLevelsButton.gameObject.SetActive(false);
        CurrentLevelName.text = " Level Select";
        CurrentStageName.text = " Stage Select";
        foreach (Stage s in levelSelectButtons[currentlySelectedLevel].stages)
        {
            Destroy(StageSelectButtons[s].gameObject);

        }

        StageSelectButtons.Clear();
        LoadStageButton.interactable = false;
        levelButtonsLocation.gameObject.SetActive(true);
        stageButtonsLocation.gameObject.SetActive(false);
    }


    public void LoadStage()
    {
        SceneLoadingObject.SendMessage("LoadStageScene", stageScenes[currentlySelectedStage]);
    }

    public void RefreshLevels()
    {
        int i = 0;

        //construct Level SelectionUI
        foreach (LevelButtonUI l in levelSelectButtons.Values)
        {
            
            l.CheckRequirements();
            i++;
        }
    }

 
}
