﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarSelector : MonoBehaviour {

	public Button leftButton;
	public Button rightButton;
	public Button Selector;
	public Text SelectorText;
	public Text CarName;
	public CarType currentSelectCar = 0;

	public Dictionary<CarType,int> unlockAmounts;

	//public float DistanceToTravel;
	//public float Speed;
	public ColorSelector colorSelector;
    public FollowPosition selectorPosition;
    public Transform[] selectorNodes;
    public int CurrentNodeI = 0;

	//float distanceGoal;
	//float currentdistanceTraveled;
	bool Traveling;
	public bool Paused;
	bool MovingLeft;
	bool MovingRight;

	//public Vector3andSpace moveUnitsPerSecond;
	//public Vector3andSpace rotateDegreesPerSecond;
	//public bool ignoreTimescale;
	//private float m_LastRealTime;


	public MeshRenderer[] cars;
	public CarUnlockUI[] unlockSigns;

    private void Awake()
    {
        selectorPosition = GetComponent<FollowPosition>();
        selectorPosition.followTransform = selectorNodes[CurrentNodeI];
        SetUp();
       // selectorPosition.enabled = false;
    }
	private void Start()
	{
       
		
		SetUpSelectors ();
        unlockAmounts = new Dictionary<CarType, int>();

        unlockAmounts[CarType.Ambulance] = 3;
        unlockAmounts[CarType.CopCar] = 3;
        unlockAmounts[CarType.DerbyCar] = 3;
        unlockAmounts[CarType.F1Car] = 3;
        unlockAmounts[CarType.MonsterTruck] = 3;
        unlockAmounts[CarType.MuscleCar] = 3;
        unlockAmounts[CarType.SportsMuscleCar] = 3;
        unlockAmounts[CarType.OffRoadTruck] = 3;
        unlockAmounts[CarType.PickupTruck] = 3;
        unlockAmounts[CarType.RalleyCar] = 3;
        unlockAmounts[CarType.SportsCar] = 3;
        unlockAmounts[CarType.StockCar] = 3;
        unlockAmounts[CarType.StreetCar] = 3;
        unlockAmounts[CarType.Taxi] = 3;
    }

    private void SetUpUnlockUI(CarType car)
    {
        unlockSigns[(int)car].Unlocked = GameDataManager.instance.playerData.UnlockedCarsFlags[(int)car];

        if (GameDataManager.instance.playerData.UnlockedCarsFlags[(int)car])
        {
            cars[(int)car].material.color = Color.white;
            unlockSigns[(int)car].Disable();
            Debug.Log(car + " UI is disbaled");
        }
        else
        {
            cars[(int)car].material.color = Color.black;
            unlockSigns [(int)car].Enable();
        }
    }

	private void SetUp(){
		unlockSigns [0].Disable();

        SetUpUnlockUI(CarType.Ambulance);
        SetUpUnlockUI(CarType.CopCar);
        SetUpUnlockUI(CarType.DerbyCar);
        SetUpUnlockUI(CarType.F1Car);
        SetUpUnlockUI(CarType.MonsterTruck);
        SetUpUnlockUI(CarType.MuscleCar);
        SetUpUnlockUI(CarType.OffRoadTruck);
        SetUpUnlockUI(CarType.PickupTruck);
        SetUpUnlockUI(CarType.RalleyCar);
        SetUpUnlockUI(CarType.SportsCar);
        SetUpUnlockUI(CarType.SportsMuscleCar);
        SetUpUnlockUI(CarType.StockCar);
        SetUpUnlockUI(CarType.StreetCar);
        SetUpUnlockUI(CarType.Taxi);

	}


    public void SetSelector(CarType car)
    {
        if (GameDataManager.instance.playerData.UnlockedCarsFlags[(int)car])
        {
            colorSelector.gameObject.SetActive(true);
            Selector.onClick.AddListener(() => SelectVehicle());
            SelectorText.text = "Select Car";
        }
        else
        {
            colorSelector.gameObject.SetActive(false);
            Selector.onClick.AddListener(() => UnlockVehicle());
            SelectorText.text = "Unlock Car";
        }
    }

	private void SetUpSelectors(){
		Selector.onClick.RemoveAllListeners ();
		//setup color selectors
		colorSelector.currentCar = currentSelectCar;
		colorSelector.SetUpColorSelectors ();

      
		//setup car selector
		switch (currentSelectCar) {
		case CarType.BasicCar:
			Selector.onClick.AddListener (() => SelectVehicle ());
			CarName.text = "Basic Car";
			SelectorText.text = "Select Car";
			colorSelector.gameObject.SetActive (true);
			break;
		case CarType.Ambulance:
			    CarName.text = "Ambulance";
                SetSelector(currentSelectCar);
                break;
		case CarType.CopCar:
			    CarName.text = "Cop Car";
                SetSelector(currentSelectCar);
                break;
		case CarType.Taxi:
			    CarName.text = "Taxi Car";
                SetSelector(currentSelectCar);
                break;
		case CarType.PickupTruck:
			    CarName.text = "Pick Up Truck";
                SetSelector(currentSelectCar);
                break;
		case CarType.DerbyCar:
			    CarName.text = "Derby Car";
                SetSelector(currentSelectCar);
                break;
		case CarType.F1Car:
			    CarName.text = "F1 Racer";
                SetSelector(currentSelectCar);
                break;
		case CarType.MonsterTruck:
			    CarName.text = "MonsterTruck";
                SetSelector(currentSelectCar);
                break;
		case CarType.MuscleCar:
			    CarName.text = "Muscle Car";
                SetSelector(currentSelectCar);
                break;
		case CarType.OffRoadTruck:
			    CarName.text = "OffRoad Truck";
                SetSelector(currentSelectCar);
                break;
		case CarType.RalleyCar:
			    CarName.text = "Ralley Car";
                SetSelector(currentSelectCar);
                break;
		case CarType.StockCar:
			    CarName.text = "Stock Car";
                SetSelector(currentSelectCar);
                break;
		case CarType.StreetCar:
			    CarName.text = "Street Car";
                SetSelector(currentSelectCar);
                break;
		case CarType.SportsCar:
			    CarName.text = "Sports Car";
                SetSelector(currentSelectCar);
                break;
		case CarType.SportsMuscleCar:
			    CarName.text = "Muscle Sports Car";
                SetSelector(currentSelectCar);
                break;
		}
	}



	// Update is called once per frame
	private void Update()
	{
        
		if (!Paused) {

            if(transform.position.x == selectorNodes[CurrentNodeI].position.x)
            {
                Paused = true;
                leftButton.interactable = true;
                rightButton.interactable = true;
            }      
		}
	}





	public void MoveToNextCar(){

		if (currentSelectCar < CarType.SportsMuscleCar){
			Paused = false;
            CurrentNodeI++;
            selectorPosition.followTransform = selectorNodes[CurrentNodeI];
          //  MovingRight = true;
			leftButton.interactable = false;
			rightButton.interactable = false;
			currentSelectCar += 1;
			SetUpSelectors ();
		}

	}

	public void MoveToPrevCar(){

		if (currentSelectCar > CarType.BasicCar) {
			Paused = false;
            CurrentNodeI--;
            selectorPosition.followTransform = selectorNodes[CurrentNodeI];
          //  MovingLeft = true;
			leftButton.interactable = false;
			rightButton.interactable = false;
			currentSelectCar -= 1;
			SetUpSelectors ();
		}


	}



	public void UnlockVehicle(){

        UnlockCar(currentSelectCar);
        SetUpUnlockUI(currentSelectCar);
        SetUpSelectors ();
	}

    public void UnlockCar(CarType SelectedCar, bool state = true)
    {
        if (!GameDataManager.instance.playerData.UnlockedCarsFlags[(int)SelectedCar] && GameDataManager.instance.playerData.CrushCans >= unlockAmounts[SelectedCar])
        {
            GameDataManager.instance.playerData.UnlockedCarsFlags[(int)SelectedCar] = state;
            GameDataManager.instance.playerData.CrushCans -= unlockAmounts[SelectedCar];
            GameDataManager.instance.Save();
            Debug.Log("CarUnlocked");
        }
    }



    public void SelectVehicle(){
		
		switch (currentSelectCar) {
		case CarType.BasicCar:
                GameDataManager.instance.playerData.CarType = CarType.BasicCar;      
			break;
		case CarType.Ambulance:
                GameDataManager.instance.playerData.CarType = CarType.Ambulance;
			break;
		case CarType.CopCar:
                GameDataManager.instance.playerData.CarType = CarType.CopCar;
			break;
		case CarType.Taxi:
                GameDataManager.instance.playerData.CarType = CarType.Taxi;
			break;
		case CarType.PickupTruck:
                GameDataManager.instance.playerData.CarType = CarType.PickupTruck;
			break;
		case CarType.DerbyCar:
                GameDataManager.instance.playerData.CarType = CarType.DerbyCar;
			break;
		case CarType.F1Car:
                GameDataManager.instance.playerData.CarType = CarType.F1Car;
			break;
		case CarType.MonsterTruck:
                GameDataManager.instance.playerData.CarType = CarType.MonsterTruck;
			break;
		case CarType.MuscleCar:
                GameDataManager.instance.playerData.CarType = CarType.MuscleCar;
			break;
		case CarType.OffRoadTruck:
                GameDataManager.instance.playerData.CarType = CarType.OffRoadTruck;
			break;
		case CarType.RalleyCar:
                GameDataManager.instance.playerData.CarType = CarType.RalleyCar;
			break;
		case CarType.StockCar:
                GameDataManager.instance.playerData.CarType = CarType.StockCar;
			break;
		case CarType.StreetCar:
                GameDataManager.instance.playerData.CarType = CarType.StreetCar;
			break;
		case CarType.SportsCar:
                GameDataManager.instance.playerData.CarType = CarType.SportsCar;
			break;
        case CarType.SportsMuscleCar:
                GameDataManager.instance.playerData.CarType = CarType.SportsMuscleCar;
                break;
        }

        GameDataManager.instance.playerData.CarSkin = colorSelector.Cars[(int)currentSelectCar].CurrentColor;
        GameDataManager.instance.Save();
    }






	[Serializable]
	public class Vector3andSpace
	{
		public Vector3 value;
		public Space space = Space.Self;
	}

}



