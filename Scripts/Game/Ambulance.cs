﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ambulance : MonoBehaviour {

    public SplineWalker walker;
    public bool Active;
    public float Dist;
    public float speed;
    public AudioSource sound;
   
    private float CurDist;
    // Use this for initialization
    void Start () {

        walker = GameObject.Find("PlayerLaneWalker").GetComponent<SplineWalker>();
        EventManager.StartListening("AmbulanceGrab", ActivateAmbulance);
        CurDist = Dist;
	}
	
	// Update is called once per frame
	void LateUpdate () {

        if (Active)
        {
            CurDist += speed*Time.deltaTime;

            Vector3 position = walker.currentSpline.GetPoint(walker.progress + CurDist);
            transform.localPosition = position;
            transform.LookAt(walker.currentSpline.GetPoint(walker.progress + CurDist) + walker.currentSpline.GetDirection(walker.progress + CurDist));
            transform.position = walker.currentSpline.GetPoint(walker.progress + CurDist);




            Vector3 screenPosition = Camera.main.WorldToScreenPoint(transform.position);


            if (screenPosition.z > 40)
            {
                DeactivateAmbulance();

            }

        }

     


    }


    public void ActivateAmbulance()
    {
        gameObject.SetActive(true);
        Active = true;
        CurDist = Dist;
        sound.volume = SM.audioVolume;
        sound.Play();
    }
       
    public void DeactivateAmbulance()
    {
        gameObject.SetActive(false);
        Active = false;
        EventManager.TriggerEvent("AmbulanceEnd");
    }




}
