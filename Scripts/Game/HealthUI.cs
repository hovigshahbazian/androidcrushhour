﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour {

    private Text text;
    [SerializeField]
    private PlayerHealthController playerHealth;
    private bool setUp;
    public Sprite[] Healthsprites;
    public Image healthImage;
  
    // Use this for initialization
    void Awake()
    {
        EventManager.StartListening("PlayerSpawn", SetUpUI);
        text = GetComponent<Text>();
        //Debug.Log("PlayerSpawned setting  health UI");
        healthImage.sprite = Healthsprites[Healthsprites.Length - 1];
    }

    // Update is called once per frame
    void Update()
    {
        if (setUp && playerHealth != null)
        {
            text.text = "Health: " + playerHealth.HitPoints.ToString();
            UpdateUI();
        }




    }


    void SetUpUI()
    {
        setUp = true;
        playerHealth = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerHealthController>();
    }

    public void UpdateUI()
    {
        if (playerHealth.HitPoints <= playerHealth.MaxHealth)
        {
           float index = ((float)playerHealth.HitPoints / (float)playerHealth.MaxHealth) * 5f;

            int healthIndex = Mathf.RoundToInt(index);

            Debug.Log(index);

            healthImage.sprite = Healthsprites[healthIndex];
        }

      
    }




}
