using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof (CarController))]
    public class CarUserControlMain : MonoBehaviour
    {

		public AudioSource RoadRageSound;
		public Animator RoadRageCollectAnim;
        private CarController m_Car; // the car controller we want to use
		public GameObject RoadRageFire;
		public Animator RoadRageHitAnim;
	
		public Animation RoadRageCarScoreAnim;
		public Animator DiamondCollectCarAnim;
		public Animation DiamondTextCarScoreAnim;
		public Animator CrashAnim;


        public float vstart=7f;
        public bool move=true;
        public bool free=false;
        public bool gravity;
        public float v;
        public float h;
        static public float positionstartx;
        static public float positionlastx;

		public float MinX;
		public float MaxX;

		//Quaternion rotation; 
		//Quaternion rotationold; 

	//	public GM.PowerUps lastpowerup;

	//	public float widthoffset;

      //  public float speed=6.0f;
      //  public float filter=5.0f;
     //   public float slowspeed;
      //  public float lastx;
      //  public float speedupspeed;
      //  public float lastspeed;
	//	public Vector3 accel;


		//public Vector3 tempreset;

		public float SpeedRating = 1f;
		public float DestructionRating = 1f;
		public float TreasureRating = 1f;

        private void Awake()
        {
            // get the car controller
         //   m_Car = GetComponent<CarController>();
			//gravity=true;
			//GM.CurrentDestructionRating = DestructionRating;
			//GM.CurrentTreasureRating = TreasureRating;

        }
        private void Start() {
		//	widthoffset=480f;
			//rotationold = transform.rotation; 
		//	rotation = transform.rotation; 



			
			//positionlastx=m_Car.transform.position.x;

			//move=true;

			//lastx=0;
			
			//GM.CurrentPowerUp= GM.PowerUps.None;
			
			//lastpowerup=GM.CurrentPowerUp;
		}

        void OnCollisionEnter (Collision col)
		{


			//Debug.Log (col.gameObject.name);

			if (col.gameObject.tag!="Terrain") {
				
				//if (GM.vibrate)
				//	Handheld.Vibrate();
				//SM.soundplay=1;
				/*
				if (GM.RoadRageInactive) {
					GM.PlayerCollided=true;
					CrashAnim.SetTrigger ("Crash");
					EventManager.TriggerEvent ("PlayerCrash");

					if (!GM.GameOverStateActive) {
						GM.GameStarted=false;
						GM.gameovertimer=10;
						GM.gameoverstart=true;
						GM.GameOverStateActive=true;
						Debug.Log ("Crashed");

					}
				}
				else {
					
				
				}*/
			}
		}

		private void OnTriggerEnter(Collider col) {

			//if (col.gameObject.name=="mario" && GM.CurrentPowerUp==GM.PowerUps.None) {
			//	PupDisplay.Active=false;
				//GM.CurrentPowerUp = (GM.PowerUps)Random.Range(1,5);

		
			//	while (GM.CurrentPowerUp==lastpowerup) {
				//	GM.CurrentPowerUp = (GM.PowerUps)Random.Range(1,5);
				//}

				//GM.CurrentPowerUp=GM.PowerUps.RoadRage;

				// if (GM.CurrentPowerUp==GM.PowerUps.Slow && m_Car.m_Topspeed < 55) {
				//	 GM.CurrentPowerUp=GM.PowerUps.Speed;
				// }


				// lastpowerup=GM.CurrentPowerUp;


		


				//if (GM.CurrentPowerUp==GM.PowerUps.Ambulance) { 
				//	GM.ambtimer=4;
				//}
				//if (GM.CurrentPowerUp==GM.PowerUps.Slow) { 
				//	GM.slowtimer=100;
				//	GM.slowflag=true;
					//m_Car.m_Topspeed-=10;
					//slowspeed=m_Car.m_Topspeed;

				//}

			//	if (GM.CurrentPowerUp==GM.PowerUps.Speed) { 
			//		GM.speeduptimer=80;
			//		GM.speedupflag=true;
				//	speedupspeed=m_Car.m_Topspeed+10;
			///	}
            /*
				if (GM.CurrentPowerUp==GM.PowerUps.Fog) { 
					GM.fogtimer=60;
					GM.fogflag=true;
					Fog.doit=true;
				}

				if (GM.CurrentPowerUp==GM.PowerUps.RoadRage) { 
					GM.rrtimer=150;
					
					RoadRageCollectAnim.SetTrigger ("GrabRoadRage");
				}
                */
			//}
       /*     
			if (col.gameObject.name=="diamond") {
				GM.diamondhit=true;
				DiamondCollectCarAnim.SetTrigger ("DiamondCollect");
				DiamondTextCarScoreAnim.Play ();
				GM.Diamonds += (int)TreasureRating;
				col.GetComponent<Collider> ().enabled = false;
			}*/


		}




        private void FixedUpdate()
        {
			
			if (false) {
                /*
				v=vstart;
				if (GM.PlayerCollided) {

					if (m_Car.m_Topspeed!=0) {
						m_Car.m_Topspeed-=1;
						if (m_Car.m_Topspeed <0 ) {
							m_Car.m_Topspeed=0;
						}
					}
					v=0;
				}
				else {
					//if (GM.slowflag) {
					//	m_Car.m_Topspeed=slowspeed;
					//}

					//if (GM.speedupflag) {
						//m_Car.m_Topspeed=speedupspeed;
					//}
/*
					if (!GM.slowflag && !GM.slowflag) {
						if (m_Car.m_Topspeed<100) {
							m_Car.m_Topspeed+=(Time.deltaTime/3);
						}
					}*/
				//}
			}
			else
				v=0;

	    	//GM.speedf=m_Car.CurrentSpeed;
	    	//GM.ambspeed=m_Car.CurrentSpeed;
	    	//GM.speed=(int)m_Car.CurrentSpeed;

			//GM.CurrentCarDistance=m_Car.transform.position;

    	//	GM.CurrentCarDistance.z-=25;
			
		/*	if (false) {
				if (!SteeringWheel.isHold) {
					SteeringWheel.input=0;
				}

				move=true;

				if (GM.accelerometer) {
					if (false) {
						if (m_Car.transform.position.x > 26) {
							if (Input.acceleration.x > 0) 
								move=false;
						}	   
						if (m_Car.transform.position.x < 16) {
							if (Input.acceleration.x < 0)
								move=false;
						}

					
					}
				}
				else {
					free=true;
					if (!free) {
						if (m_Car.transform.position.x > 26) {
							if (SteeringWheel.input > 0)
								move=false;
						}

						if (m_Car.transform.position.x < 16) {
							if (SteeringWheel.input < 0)
								move=false;
						}
					}

					if (move) {
						float movement = (SteeringWheel.input*SpeedRating) / 2;

						m_Car.transform.Translate (movement, 0, 0);


						if (m_Car.transform.position.x > MaxX) {
							m_Car.transform.Translate (-movement, 0, 0);
						}
							
						if (m_Car.transform.position.x < MinX) {
							m_Car.transform.Translate (-movement, 0, 0);
						}

					}
				}
		//	}

			//Plane.zval=GM.CurrentCarDistance.z-Plane.zvaladd;

			h=0;

			gravity=true;
            /*

			if (GM.RoadRageActive) {
				
				//gravity=false;

				//if (m_Car.CurrentSpeed < m_Car.m_Topspeed) {
				//	m_Car.transform.Translate(0, 0, .5f);
			   // }

				if (GM.RoadRageInactive) {
					//gravity=true;
					RoadRageSound.Stop();
					RoadRageFire.SetActive (false);
			    	if (m_Car.CurrentSpeed >= m_Car.m_Topspeed) {
						
						GM.RoadRageActive=false;
						//Debug.Log("Road Rage ended");
			    	}
				}
                */
				//if (GM.HitCarRoadRage) {
				//	
				
				//}
		//	}
	#if !MOBILE_INPUT
				float handbrake = CrossPlatformInputManager.GetAxis("Jump");
				m_Car.Move(h, v, v, handbrake, gravity);
	#else
			  //  float handbrake = false;
			     m_Car.Move(h, v, v,  0.0f , gravity);
	#endif

			}

        }
    }
