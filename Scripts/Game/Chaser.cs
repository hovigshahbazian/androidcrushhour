﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chaser : MonoBehaviour {

    public SplineWalker walker;
    public bool Active;
    public float Dist;
    public float speed;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (Active)
        {
            Dist += speed * Time.deltaTime;

            Vector3 position = walker.currentSpline.GetPoint(walker.progress + Dist);
            transform.localPosition = position;
            transform.LookAt(walker.currentSpline.GetPoint(walker.progress + Dist) + walker.currentSpline.GetDirection(walker.progress + Dist));
            transform.position = walker.currentSpline.GetPoint(walker.progress + Dist);

        }
    }


    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag != "Terrain")
        {
           Active = false;
        }

    }
}
