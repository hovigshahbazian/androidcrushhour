﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gap : MonoBehaviour {

    //public LaneSwitch laneSwitch;
    private BezierSpline spline;
    public GameObject DropToLane;
    public float ProgressPoint = 0;


    void Start()
    {
        spline = DropToLane.GetComponent<BezierSpline>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            PlayerCarController player = col.GetComponentInParent<PlayerCarController>();
            Debug.Log("Fall");
            if (player != null)
            {
                player.walker.SetSpline(spline,ProgressPoint);
                player.playerstate = PlayerCarController.PlayerState.Falling;
            }
        }

       
    }


}
