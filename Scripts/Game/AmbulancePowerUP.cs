﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbulancePowerUP : MonoBehaviour {


    public Renderer render;
    public Collider col;

    public void OnEnable()
    {
        render.enabled = true;
        col.enabled = true;
    }

    /*
	// Update is called once per frame
	void Update () {

        /*
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(transform.position);

        if (screenPosition.z < 0)
        {
            gameObject.SetActive(false);
        }
    }*/


    void OnTriggerEnter(Collider otherCol)
    {
        Debug.Log(otherCol.name);
        if (otherCol.CompareTag("Player") ){

            PlayerCarController playerController = otherCol.GetComponentInParent<PlayerCarController>();

            if (playerController != null)
            {
                if (playerController.CurrentPowerUp == PlayerCarController.PowerUps.None) 
                {
                    EventManager.TriggerEvent("AmbulanceGrab");
                    render.enabled = false;
                    col.enabled = false;
                    playerController.CurrentPowerUp = PlayerCarController.PowerUps.Ambulance;
                }
            }
        }
    }


   





}
