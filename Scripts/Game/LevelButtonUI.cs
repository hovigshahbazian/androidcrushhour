﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButtonUI : MonoBehaviour {

    public Image LevelImage;
    public Button Levelbutton;
    public Sprite[] StageScreen;
    public Sprite[] StageImage;
    public Level level;
    public Stage[] stages;
    public bool Unlocked = false;
    public int UnlockRequirement;
    public Image LockedPanel;
    public Text RequirmentsText;

    // Use this for initialization
    void Awake()
    {
       
    }


    // Use this for initialization
    void Start () {
        // Unlocked = GameDataManager.instance.LevelUnlockedMap[level];
        CheckRequirements();
	}
	


    public void CheckRequirements()
    {
        Debug.Log("Checking requirments");
        if (GameDataManager.instance.playerData.Stars >= UnlockRequirement)
        {
            Unlocked = true;
        }
        else
        {
            Unlocked = false;
        }


        if (Unlocked)
        {
            LockedPanel.enabled = false;
            if (RequirmentsText != null)
                RequirmentsText.enabled = false;
        }
        else
        {
            LockedPanel.enabled = true;
            if (RequirmentsText != null)
            {
                RequirmentsText.text = "You need " + UnlockRequirement + " Stars to Unlock this Level";
                RequirmentsText.enabled = true;
            }
        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}
