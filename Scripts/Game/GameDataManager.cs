﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public enum Level { Highway, Coast, City,Raceway, Canyon,Desert, Alpine,Route,Vegas,Atlantic,Winter, Star,Atlantis,Wipeout }


    [Serializable]
public enum Stage { HighWay_1, HighWay_2, HighWay_3,HighWay_4,
                    Coast_1, Coast_2, Coast_3, Coast_4,
                    City_1, City_2, City_3, City_4,
                    Raceway_1,Raceway_2, Raceway_3, Raceway_4,
                    Canyon_1, Canyon_2, Canyon_3, 
                    Desert_1,Desert_2,Desert_3,
                    Alpine_1, Alpine_2, Alpine_3,
                    Route_1,Route_2,Route_3,
                    Vegas_1,Vegas_2,Vegas_3,
                    Atlantic_1,Atlantic_2,Atlantic_3,
                    Winter_1,Winter_2,Winter_3,
                    Star_1,Star_2,Star_3,
                    Atlantis_1,Atlantis_2,Atlantis_3,
                    Wipeout_1,Wipeout_2,Wipeout_3
                    }


public enum CarType { BasicCar = 0, Ambulance = 1, CopCar = 2, Taxi = 3, PickupTruck = 4, DerbyCar = 5, F1Car = 6, MonsterTruck = 7,
                      MuscleCar = 8, OffRoadTruck = 9, RalleyCar = 10, StockCar = 11, StreetCar = 12, SportsCar = 13, SportsMuscleCar = 14 };

public enum Rank { None = 0, First = 1, Second = 2, Third = 3 }


public class GameDataManager : MonoBehaviour {

	public static GameDataManager instance;

    public Dictionary<Stage, LevelData> loadedLevelDataMap;
   // public Dictionary<Level, bool> LevelUnlockedMap;


    public PlayerData playerData;
    //  public LevelData[] loadedLevelData;

    [NonSerialized]
    public Stage[] LevelList = { Stage.HighWay_1,Stage.HighWay_2,Stage.HighWay_3,
                                 Stage.Coast_1,Stage.Coast_2,Stage.Coast_3,
                                 Stage.City_1, Stage.City_2, Stage.City_3,
                                 Stage.Raceway_1, Stage.Raceway_2, Stage.Raceway_3,
                                 Stage.Canyon_1, Stage.Canyon_2,Stage.Canyon_3,
                                 Stage.Desert_1, Stage.Desert_2,Stage.Desert_3,
                                 Stage.Alpine_1, Stage.Alpine_2, Stage.Alpine_3,
                                 Stage.Route_1, Stage.Route_2, Stage.Route_3,
                                 Stage.Vegas_1, Stage.Vegas_2, Stage.Vegas_3,
                                 Stage.Atlantic_1, Stage.Atlantic_2, Stage.Atlantic_3,
                                 Stage.Winter_1, Stage.Winter_2, Stage.Winter_3,
                                 Stage.Star_1, Stage.Star_2, Stage.Star_3,
                                 Stage.Atlantis_1, Stage.Atlantis_2, Stage.Atlantis_3,
                                 Stage.Wipeout_1, Stage.Wipeout_2,Stage.Wipeout_3};


	void Awake()
	{
		if (instance == null) {
			DontDestroyOnLoad (gameObject);
			instance = this;

            loadedLevelDataMap = new Dictionary<Stage, LevelData>();

            foreach (Stage s in LevelList)
            {
                loadedLevelDataMap.Add(s, new LevelData());
            }

            /*
            loadedLevelDataMap.Add(Stage.HighWay_1, new LevelData());
            loadedLevelDataMap.Add(Stage.HighWay_2, new LevelData());
            loadedLevelDataMap.Add(Stage.HighWay_3, new LevelData());
            loadedLevelDataMap.Add(Stage.Coast_1, new LevelData());
            loadedLevelDataMap.Add(Stage.Coast_2, new LevelData());
            loadedLevelDataMap.Add(Stage.Coast_3, new LevelData());
            loadedLevelDataMap.Add(Stage.City_1, new LevelData());
            loadedLevelDataMap.Add(Stage.City_2, new LevelData());
            loadedLevelDataMap.Add(Stage.City_3, new LevelData());
            loadedLevelDataMap.Add(Stage.Raceway_1, new LevelData());
            loadedLevelDataMap.Add(Stage.Raceway_2, new LevelData());
            loadedLevelDataMap.Add(Stage.Raceway_3, new LevelData());
            loadedLevelDataMap.Add(Stage.Canyon_1, new LevelData());
            loadedLevelDataMap.Add(Stage.Desert_1, new LevelData());
            loadedLevelDataMap.Add(Stage.Alpine_1, new LevelData());
            loadedLevelDataMap.Add(Stage.Atlantic_1, new LevelData());
            loadedLevelDataMap.Add(Stage.Atlantis_1, new LevelData());
            loadedLevelDataMap.Add(Stage.Route_1, new LevelData());
            loadedLevelDataMap.Add(Stage.Star_1, new LevelData());
            loadedLevelDataMap.Add(Stage.Winter_1, new LevelData());
            loadedLevelDataMap.Add(Stage.Wipeout_1, new LevelData());
            */

            /*
            LevelUnlockedMap = new Dictionary<Level, bool>();
            LevelUnlockedMap.Add(Level.Highway, true);
            LevelUnlockedMap.Add(Level.Coast,true);
            LevelUnlockedMap.Add(Level.City, true);
            LevelUnlockedMap.Add(Level.Raceway, true);
            LevelUnlockedMap.Add(Level.Canyon,true);
            LevelUnlockedMap.Add(Level.Desert,true);
            LevelUnlockedMap.Add(Level.Alpine,true);
            LevelUnlockedMap.Add(Level.Route,true);
            LevelUnlockedMap.Add(Level.Vegas,true);
            LevelUnlockedMap.Add(Level.Atlantic, true);
            LevelUnlockedMap.Add(Level.Winter,true);
            LevelUnlockedMap.Add(Level.Star, true);
            LevelUnlockedMap.Add(Level.Atlantis,true);
            LevelUnlockedMap.Add(Level.Wipeout, true);*/

        }
        else if (instance != this) {
			Destroy (gameObject);
		}

    
    }


	void Start(){
        
    }


	public void Save()
	{
        string SaveFilePath = Application.persistentDataPath;
        if (persistentDataPath != null)
        {
            SaveFilePath = persistentDataPath;
        }
        BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create(SaveFilePath + "/playerInfo.dat");
        //Debug.Log("Data Saved at: " + SaveFilePath + "/playerInfo.dat");
		bf.Serialize (file, GameDataManager.instance.playerData);
	    file.Close ();
	}


	public void SaveLevel(Stage level)
	{
        string SaveFilePath = Application.persistentDataPath;

        #if UNITY_ANDROID

        if (persistentDataPath != null)
        {
            SaveFilePath = persistentDataPath;
        }
        #endif

        #if UNITY_IOS
		SaveFilePath = Application.persistentDataPath;
        #endif

        BinaryFormatter bf = new BinaryFormatter ();

		FileStream file = File.Create(SaveFilePath + "/Level" + level.ToString() + "Info.dat");

        Debug.Log(SaveFilePath + "/Level" + level.ToString() + "Info.dat");
		bf.Serialize (file, GameDataManager.instance.loadedLevelDataMap[level]);
		file.Close ();

      
	}
		
	public void Load()
	{
        string SaveFilePath = Application.persistentDataPath;

        #if UNITY_ANDROID

        if (persistentDataPath != null)
        {
            SaveFilePath = persistentDataPath;
        }
        #endif

        #if UNITY_IOS
		    SaveFilePath = Application.persistentDataPath;
        #endif


        if (File.Exists(SaveFilePath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open,FileAccess.Read,FileShare.Read);

            try
            {
                GameDataManager.instance.playerData = (PlayerData)bf.Deserialize(file) as PlayerData;
            }
            catch(Exception e)
            {
                Debug.Log(e.Message);
            }

            file.Close();

        }else
        {
            GameDataManager.instance.playerData = new PlayerData();
        }
        Debug.Log("Player Loaded");

	}
			

	public void LoadLevel (Stage key)
	{

        string SaveFilePath = Application.persistentDataPath;

        #if UNITY_ANDROID

        if (persistentDataPath != null)
        {
            SaveFilePath = persistentDataPath;
        }
        #endif

        #if UNITY_IOS
		SaveFilePath = Application.persistentDataPath;
        #endif


        Debug.Log ("LeveLoaded");
        if (File.Exists(SaveFilePath + "/Level" + key.ToString() + "Info.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/Level" + key.ToString() + "Info.dat", FileMode.Open,FileAccess.Read,FileShare.Read);
            GameDataManager.instance.loadedLevelDataMap[key] = (LevelData)bf.Deserialize(file) as LevelData;
            file.Close();
        }
        else
        {
            GameDataManager.instance.loadedLevelDataMap[key] = new LevelData();
        }

        Debug.Log("Loaded level" + GameDataManager.instance.loadedLevelDataMap[key].TutorialViewed);

    }



    public void ErasePlayerData(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

		PlayerData pd = new PlayerData ();

        playerData = pd;

		bf.Serialize (file, pd);
		file.Close ();
	}

    public void EraseAllLevelData()
    {

        loadedLevelDataMap.Clear();
        loadedLevelDataMap.Add(Stage.HighWay_1, new LevelData());
        loadedLevelDataMap.Add(Stage.HighWay_2, new LevelData());
        loadedLevelDataMap.Add(Stage.HighWay_3, new LevelData());
        loadedLevelDataMap.Add(Stage.Coast_1, new LevelData());
        loadedLevelDataMap.Add(Stage.Coast_2, new LevelData());
        loadedLevelDataMap.Add(Stage.Coast_3, new LevelData());
        loadedLevelDataMap.Add(Stage.City_1, new LevelData());
        loadedLevelDataMap.Add(Stage.City_2, new LevelData());
        loadedLevelDataMap.Add(Stage.City_2, new LevelData());
        loadedLevelDataMap.Add(Stage.Raceway_1, new LevelData());
        loadedLevelDataMap.Add(Stage.Raceway_2, new LevelData());
        loadedLevelDataMap.Add(Stage.Raceway_3, new LevelData());
        loadedLevelDataMap.Add(Stage.Canyon_1, new LevelData());
        loadedLevelDataMap.Add(Stage.Canyon_2, new LevelData());
        loadedLevelDataMap.Add(Stage.Canyon_3, new LevelData());
        loadedLevelDataMap.Add(Stage.Desert_1, new LevelData());
        loadedLevelDataMap.Add(Stage.Alpine_1, new LevelData());
        loadedLevelDataMap.Add(Stage.Atlantic_1, new LevelData());
        loadedLevelDataMap.Add(Stage.Atlantis_1, new LevelData());
        loadedLevelDataMap.Add(Stage.Route_1, new LevelData());
        loadedLevelDataMap.Add(Stage.Star_1, new LevelData());
        loadedLevelDataMap.Add(Stage.Winter_1, new LevelData());
        loadedLevelDataMap.Add(Stage.Wipeout_1, new LevelData());

    }

    
	public void EraseLevelData(float key){
	     BinaryFormatter bf = new BinaryFormatter ();
		 FileStream file = File.Create(Application.persistentDataPath + "/Level" + key.ToString() + "Info.dat");
        LevelData ld = new LevelData();
        bf.Serialize(file, ld);
        file.Close();
	}

	void OnApplicationQuit() {
		//Save ();
		//SaveLevel(Stage.HighWay_1);
		//SaveLevel(Stage.Coast_1);
		//SaveLevel(Stage.City_1);
		//SaveLevel(Stage.Raceway_1);

	}




    /// ======================================================================================================
    ///Functions Dealing with Finding the Persisitent Data Path for a Unity Android Game
    /// ======================================================================================================


    private static string[] _persistentDataPaths;

    public static bool IsDirectoryWritable(string path)
    {
        try
        {
            if (!Directory.Exists(path)) return false;
            string file = Path.Combine(path, Path.GetRandomFileName());
            using (FileStream fs = File.Create(file, 1)) { }
            File.Delete(file);
            return true;
        }
        catch
        {
            return false;
        }
    }

    private static string GetPersistentDataPath(params string[] components)
    {
        try
        {
            string path = Path.DirectorySeparatorChar + string.Join("" + Path.DirectorySeparatorChar, components);
            if (!Directory.GetParent(path).Exists) return null;
            if (!Directory.Exists(path))
            {
                Debug.Log("creating directory: " + path);
                Directory.CreateDirectory(path);
            }
            if (!IsDirectoryWritable(path))
            {
                Debug.LogWarning("persistent data path not writable: " + path);
                return null;
            }
            return path;
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            return null;
        }
    }

    public static string persistentDataPathInternal
    {
#if UNITY_ANDROID
        get
        {
            if (Application.isEditor || !Application.isPlaying) return Application.persistentDataPath;
            string path = null;
            if (string.IsNullOrEmpty(path)) path = GetPersistentDataPath("storage", "emulated", "0", "Android", "data", Application.identifier, "files");
            if (string.IsNullOrEmpty(path)) path = GetPersistentDataPath("data", "data", Application.identifier, "files");
            return path;
        }
#else
		get { return Application.persistentDataPath; }
#endif
    }

    public static string persistentDataPathExternal
    {
#if UNITY_ANDROID
        get
        {
            if (Application.isEditor || !Application.isPlaying) return null;
            string path = null;
            if (string.IsNullOrEmpty(path)) path = GetPersistentDataPath("storage", "sdcard0", "Android", "data", Application.identifier, "files");
            if (string.IsNullOrEmpty(path)) path = GetPersistentDataPath("storage", "sdcard1", "Android", "data", Application.identifier, "files");
            if (string.IsNullOrEmpty(path)) path = GetPersistentDataPath("mnt", "sdcard", "Android", "data", Application.identifier, "files");
            return path;
        }
#else
		get { return null; }
#endif
    }

    public static string[] persistentDataPaths
    {
        get
        {
            if (_persistentDataPaths == null)
            {
                List<string> paths = new List<string>();
                if (!string.IsNullOrEmpty(persistentDataPathInternal)) paths.Add(persistentDataPathInternal);
                if (!string.IsNullOrEmpty(persistentDataPathExternal)) paths.Add(persistentDataPathExternal);
                if (!string.IsNullOrEmpty(Application.persistentDataPath) && !paths.Contains(Application.persistentDataPath)) paths.Add(Application.persistentDataPath);
                _persistentDataPaths = paths.ToArray();
            }
            return _persistentDataPaths;
        }
    }

    // returns best persistent data path
    public static string persistentDataPath
    {
        get { return persistentDataPaths.Length > 0 ? persistentDataPaths[0] : null; }
    }

    public static string GetPersistentFile(string relativePath)
    {
        if (string.IsNullOrEmpty(relativePath)) return null;
        foreach (string path in persistentDataPaths)
        {
            if (FileExists(path, relativePath)) return Path.Combine(path, relativePath);
        }
        return null;
    }

    public static bool SaveData(string relativePath, byte[] data)
    {
        string path = GetPersistentFile(relativePath);
        if (string.IsNullOrEmpty(path))
        {
            return SaveData(relativePath, data, 0);
        }
        else
        {
            try
            {
                File.WriteAllBytes(path, data);
                return true;
            }
            catch (Exception ex)
            {
                Debug.LogWarning("couldn't save data to: " + path);
                Debug.LogException(ex);
                // try to delete file again
                if (File.Exists(path)) File.Delete(path);
                return SaveData(relativePath, data, 0);
            }
        }
    }

    private static bool SaveData(string relativePath, byte[] data, int pathIndex)
    {
        if (pathIndex < persistentDataPaths.Length)
        {
            string path = Path.Combine(persistentDataPaths[pathIndex], relativePath);
            try
            {
                string dir = Path.GetDirectoryName(path);
                if (!Directory.Exists(dir))
                {
                    Debug.Log("creating directory: " + dir);
                    Directory.CreateDirectory(dir);
                }
                File.WriteAllBytes(path, data);
                return true;
            }
            catch (Exception ex)
            {
                Debug.LogWarning("couldn't save data to: " + path);
                Debug.LogException(ex);
                if (File.Exists(path)) File.Delete(path);       // try to delete file again
                return SaveData(relativePath, data, pathIndex + 1); // try next persistent path
            }
        }
        else
        {
            Debug.LogWarning("couldn't save data to any persistent data path");
            return false;
        }
    }

    public static bool FileExists(string path, string relativePath)
    {
        return Directory.Exists(path) && File.Exists(Path.Combine(path, relativePath));
    }









}
[Serializable]
public class PlayerData
{
    [SerializeField]
    private CarType carType;
    [SerializeField]
    private int stars;
    [SerializeField]
    private int crushCans;
    [SerializeField]
    private CarColor carSkin = CarColor.First;

    public bool[] UnlockedCarsFlags = new bool[15];

   // public bool AmbUnlocked;
 //   public bool CopUnlocked;
  //  public bool TaxiUnlocked;
  //  public bool PickupUnlocked;
  //  public bool DerbyCarUnlocked;
  //  public bool F1CarUnlocked;
  //  public bool MonsterTruckUnlocked;
  //  public bool MuscleCarUnlocked;
 //   public bool OffRoadTruckUnlocked;
 //   public bool RalleyCarUnlocked;
 //   public bool StockCarUnlocked;
  //  public bool StreetCarUnlocked;
  //  public bool SportsCarUnlocked;
 //   public bool MusleSportsCarUnlocked;


    public CarType CarType{
        get{return carType;}
        set { carType = value; }
   }
  
    public int Stars
    {
        get { return stars; }
        set { stars = value;  }
    }

    public int CrushCans
    {
        get { return crushCans; }
        set { crushCans = value; }
    }


    public CarColor CarSkin
    {
        get { return carSkin; }
        set { carSkin = value; }
    }
}

[Serializable]
public class LevelData
{
    [SerializeField]
    public bool TutorialViewed;
    [SerializeField]
    private int score;
    [SerializeField]
    private bool unlocked;
    [SerializeField]
    private bool completed;

    public bool sideQuestCompleteOne;
    public bool sideQuestCompleteTwo;
    public bool sideQuestCompleteThree;


    [SerializeField]
    private int crushCansCollected;

    [SerializeField]
    private Rank crushCanRank;


    [SerializeField]
    private int roadRageScore;
    [SerializeField]
    private Rank destructionRank;

    [SerializeField]
    private Rank treasureRank;

    [SerializeField]
    private float distanceRecord;

    [SerializeField]
    private int highestHealthLeft;

    [SerializeField]
    private float TopSpeedRecord;

    [SerializeField]
    private Rank survivalRank;
    [SerializeField]
    private int starsGathered;

    public int Score
    {
        get { return score; }
        set { score = value; }
    }


    public bool Unlocked
    {
        get { return unlocked; }
        set { unlocked = value; }
    }


    public bool Completed
    {
        get { return completed; }
        set { completed = value; }
    }

    public int CrushCansCollected
    {
        get { return crushCansCollected; }
        set { crushCansCollected = value; }
    }

    public Rank CrushcanRank
    {
        get { return crushCanRank; }
        set { crushCanRank = value; }
    }

    public int RoadRageScore
    {
        get { return roadRageScore; }
        set { roadRageScore = value; }
    }
    public Rank TreasureRank
    {
        get { return treasureRank; }
        set { treasureRank = value; }
    }

    public Rank DestructionRank
    {
        get { return destructionRank; }
        set { destructionRank = value; }
    }

    public Rank SurvivalRank
    {
        get { return survivalRank; }
        set { survivalRank = value; }
    }

    public float DistanceRecord
    {
        get { return distanceRecord; }
        set { distanceRecord = value; }
    }

    public int StarsGathered
    {
        get { return starsGathered; }
        set { starsGathered = value; }
    }

}


