using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof(CarController))]
    public class CarUserControl2 : MonoBehaviour
    {
        private CarController m_Car; // the car controller we want to use
        public GameObject mesh;
        public Vector3 temp;
        public Vector3 InLanePosition;
        public Vector3 OutLanePosition;
        public float v;
        public float h;
        public float addit;
        public float shiftval;
        public int type;
        public int type2;
        public int CurrentLane;
        public int origlane;
        // public int speed;
        public bool switched;
        public bool collided;
        // public bool collidedflag;
        public bool gravity;
        public bool active;
        public bool shown;
        public bool switchedit2;
        public int id;

        Quaternion rotation;

        public float vstart = .75f;
        public float jumpval;

        Vector3 scale;
        Vector3 scaleorig;


        private void Awake()
        {
            m_Car = GetComponent<CarController>();
            gravity = false;
        }
        void OnCollisionEnter(Collision col)
        {
            if (col.gameObject.name != "Terrain")
            {
                collided = true;
                //collidedflag=true;
            }
        }



        private void Start()
        {
            origlane = CurrentLane;
            InLanePosition = m_Car.transform.position;
            OutLanePosition = InLanePosition;
            temp = InLanePosition;

            //CurrentLane=origlane;

            //DoLane();

            switched = false;

            shiftval = .02f;
            collided = false;
            active = false;
            mesh.SetActive(false);
            //collidedflag=false;

            shown = false;
            jumpval = 0;



            ResetCar();

        }

        private void SetSpeed()
        {
            if (type != -1)
            {
                if (CurrentLane == 1 || CurrentLane == 2)
                {
                    m_Car.m_Topspeed = 40;
                }

                if (CurrentLane == 3)
                {
                    m_Car.m_Topspeed = 40;
                }

                if (CurrentLane == 4)
                {
                    m_Car.m_Topspeed = 40;
                }

                if (CurrentLane == 5)
                {
                    m_Car.m_Topspeed = 30;
                }

                if (CurrentLane == 6 || CurrentLane == 7)
                {
                    m_Car.m_Topspeed = 20;
                }
            }

        }

        //places cars outside of the terrain and resets rotation
        private void ResetCar()
        {
            if (type != -1)
            {
                //	InLanePosition.x=70+(id*2);
                //	InLanePosition.z=GM.CurrentCarDistance.z+(id*10);
                //	InLanePosition.y=OutLanePosition.y;
                //	m_Car.transform.position=InLanePosition;

                GetComponent<Rigidbody>().rotation = Quaternion.identity;
            }
        }


        private void FixedUpdate()
        {
            /*
            if (GM.GameStarted)
            {
                v = vstart;
            }
            else
                v = 0;

            InLanePosition = m_Car.transform.position;
            temp = InLanePosition;


            Vector3 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
            /*
		   if (!active && type!=-1) {
			   if (GM.SelectedCar==id ){
				   if (!GM.AmbulanceActive || CurrentLane ==1 || CurrentLane ==7) {
					   GM.LastLaneSelected=-1;
					   if (GM.LastLaneSelected!=CurrentLane) {
						   CurrentLane=origlane;
						   GM.LastLaneSelected=CurrentLane;
						   active=true;
						   mesh.SetActive (true);
						   shown=true;
						   switched=false;
						   InLanePosition.x=OutLanePosition.x;
						   InLanePosition.z=100+GM.CurrentCarDistance.z;
						   SetSpeed();
						   InLanePosition.y=OutLanePosition.y;
							transform.rotation = Quaternion.identity;
					
					   }
				   }
			   }
		   }
           *//*
            //if we are active and not the ambulance
            if (active && type != -1 && !shown)
            {

                temp = m_Car.transform.position;

                //setSpeed of car
                if (!GM.AmbulanceActive && !collided)
                {
                    SetSpeed();
                }


                //if we leave the screen, we leave the lane and deactive
                if (screenPosition.z < jumpval && type != -1)
                {
                    active = false;
                    mesh.SetActive(false);
                    ResetCar();
                }
                /*

                 //shift to another lane while the ambulance is active
                if (GM.AmbulanceActive) {
                    if (origlane==2) {
                        if (temp.x >=InfiniteTerrain.lanepos1) {
                            InLanePosition.x-=shiftval;
                            CurrentLane=1;
                        }
                        else
                            switched=true;
                    }
                    if (origlane==32) {
                        if (temp.x >=InfiniteTerrain.lanepos2) {
                            InLanePosition.x-=shiftval;
                            CurrentLane=2;
                        }
                        else {
                            if (temp.x >=InfiniteTerrain.lanepos1) {
                                InLanePosition.x-=shiftval;
                                CurrentLane=1;
                            }
                            else
                                switched=true;
                        }
                    }
                    if (origlane==3) {
                        if (temp.x >=InfiniteTerrain.lanepos2) {
                            InLanePosition.x-=shiftval;
                            CurrentLane=2;
                        }
                        else {
                            if (temp.x >=InfiniteTerrain.lanepos1) {
                                InLanePosition.x-=shiftval;
                                CurrentLane=1;
                            }
                            else
                                switched=true;
                        }
                    }
                    if (origlane==4) {
                        if (type==4) {
                            if (temp.x <=InfiniteTerrain.lanepos5) {
                                InLanePosition.x+=shiftval;
                                CurrentLane=5;
                            }
                            else {
                                if (temp.x <=InfiniteTerrain.lanepos6) {
                                    InLanePosition.x+=shiftval;
                                    CurrentLane=6;
                                }
                                else {
                                    if (temp.x <=InfiniteTerrain.lanepos7) {
                                        InLanePosition.x+=shiftval;
                                        CurrentLane=7;
                                    }
                                    else
                                        switched=true;

                                }
                            }

                        }
                        else {
                            if (temp.x >=InfiniteTerrain.lanepos3) {
                                InLanePosition.x-=shiftval;
                                CurrentLane=3;
                            }
                            else {
                                if (temp.x >=InfiniteTerrain.lanepos2) {
                                    InLanePosition.x-=shiftval;
                                    CurrentLane=2;
                                }
                                else {
                                    if (temp.x >=InfiniteTerrain.lanepos1) {
                                        InLanePosition.x-=shiftval;
                                        CurrentLane=1;
                                    }
                                    else
                                        switched=true;

                                }
                            }
                        }
                    }
                    if (origlane==5) {
                        if (temp.x <=InfiniteTerrain.lanepos6) {
                            InLanePosition.x+=shiftval;
                            CurrentLane=6;
                        }
                        else {
                            if (temp.x <=InfiniteTerrain.lanepos7) {
                                InLanePosition.x+=shiftval;
                                CurrentLane=7;
                            }
                            else
                                switched=true;

                        }
                    }
                    if (origlane==6) {
                        if (temp.x <=InfiniteTerrain.lanepos7) {
                            InLanePosition.x+=shiftval;
                            CurrentLane=7;
                        }
                        else
                            switched=true;
                    }
                }
            }

            */
            /*
                type2 = 0;

                if (type == -1)
                {
                    type2 = 1;
                    m_Car.m_Topspeed = GM.ambspeed;
                    active = false;
                    mesh.SetActive(false);
                    v = 1f;
                    if (GM.AmbulanceActive)
                    {
                        mesh.SetActive(true);
                        m_Car.m_Topspeed = GM.speedf + 20;
                        if (screenPosition.z > 40)
                        {
                            GM.AmbulanceActive = false;
                            //OutLanePosition.z=GM.CurrentCarDistance.z-(GM.speed/2);
                            m_Car.transform.position = OutLanePosition;
                            PupDisplay.Active = true;
                            Pup.toplace = true;
                            GM.CurrentPowerUp = GM.PowerUps.None;
                        }
                    }
                    else
                    {
                        InLanePosition = m_Car.transform.position;
                        InLanePosition.x = OutLanePosition.x;
                        m_Car.transform.position = InLanePosition;
                    }
                }
                if (collided && type != -1)
                {

                    collided = false;
                }



                //InLanePosition.y=OutLanePosition.y;


                h = 0;

                gravity = false;


                //if we are ambulance 
                if (type == -1)
                {
                    gravity = true;
                }
                else
                {
                    shown = false;
                    if (GM.GameStarted)
                    {
                        if ((CurrentLane < 3) && !GM.AmbulanceActive)
                        {
                            InLanePosition.z += .2f;
                        }
                        if ((CurrentLane == 3 || CurrentLane == 4) && !GM.AmbulanceActive)
                        {
                            InLanePosition.z += .1f;
                        }

                        if ((CurrentLane == 5 || CurrentLane == 6 || CurrentLane == 7) && !GM.AmbulanceActive)
                        {
                            InLanePosition.z += .05f;
                        }

                        m_Car.transform.position = InLanePosition;
                        if (screenPosition.z < 10)
                        {
                            gravity = true;
                        }
                    }
                }
                

#if !MOBILE_INPUT
            float handbrake = CrossPlatformInputManager.GetAxis("Jump");
			m_Car.Move(h, v, v, handbrake, gravity);
#else
                //m_Car.Move(h, v, v, 0f, gravity);
#endif
            }*/
        }
    }
}
