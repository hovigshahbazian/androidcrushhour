﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostPowerUp : MonoBehaviour {
 
    public float GhostTime = 5;

    public Renderer render;
    public Collider col;

    void OnEnable()
    {
        render.enabled = true;
        col.enabled = true;
    }


    public void OnTriggerEnter(Collider other)
    {
      //  Debug.Log(other.tag);
        if (other.CompareTag("Player"))
        {
           // Debug.Log("Ghosting");
            StartCoroutine(PickUp(other));

        }
    }


    IEnumerator PickUp(Collider player)
    {
        PlayerCarController playerController = player.GetComponentInParent<PlayerCarController>();
        if (playerController != null)
        {
           render.enabled = false;
           col.enabled = false;
            EventManager.TriggerEvent("GhostGrab");
           
            yield return new WaitForSeconds(GhostTime);
            EventManager.TriggerEvent("GhostEnd");
           
        }
    }

}
