﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEndMenuAnimate : MonoBehaviour {
	public Animator MenuAnimator;
	public Animator DestructRankAnim;
	public Animator SurvivalRankAnim;
	public Animator TreasureRankAnim;
	public string EventTrigger;
    //public LevelGameData levelData;
    public GM gm;
    public Stage levelIndex;
	//public float[] SurvialRankScores;
	//public int[] DestructionRankScores;
	//public int[] TreasureRankScores;
    public Distance distanceUI;
    [SerializeField]
    RoadRageController rrc;
    [SerializeField]
    TreasureCollector tsc;
	private int starsGathered = 0;
    public SideQuestRewardUI QuestOneReward;
    public SideQuestRewardUI QuestTwoReward;
    public SideQuestRewardUI QuestThreeReward;


    //bonus for sidequest complete - Extra cush cans and bigger score



	// Use this for initialization
	void Start () {
        gm = GameObject.Find("GM").GetComponent<GM>();
        levelIndex = gm.LevelID;
        rrc = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<RoadRageController>();
        tsc = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<TreasureCollector>();
        
        distanceUI = FindObjectOfType<Distance>();
        //EventManager.StartListening (EventTrigger, PlayMenuAnim);
        PlayMenuAnim();

	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void PlayMenuAnim(){
		MenuAnimator.SetTrigger(EventTrigger);
		UpdateStars ();
      //  Debug.Log("Updating Stars");
	}

	public void PlaySurvivalRankAnim(){
      //  Debug.Log("Playng SurvivalAnim");
		if (distanceUI.distance > gm.SurvialRankScores [2]) {
			SurvivalRankAnim.SetTrigger ("Star3");
		}else if(distanceUI.distance > gm.SurvialRankScores [1] && distanceUI.distance < gm.SurvialRankScores [2]) {
			SurvivalRankAnim.SetTrigger ("Star2");
		}else if(distanceUI.distance > gm.SurvialRankScores [0] && distanceUI.distance <= gm.SurvialRankScores [1]) {
			SurvivalRankAnim.SetTrigger ("Star1");
		}else if (distanceUI.distance < gm.SurvialRankScores [0]) {
		}
	}

	public void PlayDestructRankAnim(){
      //  Debug.Log("Playng Destruction Anim");
        if (rrc.RoadRageCounter > gm.DestructionRankScores [2]) {
			DestructRankAnim.SetTrigger ("Star3");
		}else if(rrc.RoadRageCounter > gm.DestructionRankScores [1] && rrc.RoadRageCounter < gm.DestructionRankScores [2]) {
			SurvivalRankAnim.SetTrigger ("Star2");
		}else if(rrc.RoadRageCounter > gm.DestructionRankScores [0] && rrc.RoadRageCounter <= gm.DestructionRankScores [1]) {
			DestructRankAnim.SetTrigger ("Star1");
		}else if (rrc.RoadRageCounter < gm.DestructionRankScores [0]) {

		}
	}




	public void PlayTreasureRankAnim(){
    //    Debug.Log("Playng Treasure Anim");
        if (tsc.TreasureScore > gm.TreasureRankScores [2]) {
			TreasureRankAnim.SetTrigger ("Star3");
		}else if(tsc.TreasureScore > gm.TreasureRankScores [1] && tsc.TreasureScore < gm.TreasureRankScores [2]) {
			TreasureRankAnim.SetTrigger ("Star2");
		}else if(tsc.TreasureScore > gm.TreasureRankScores [0] && tsc.TreasureScore <= gm.TreasureRankScores [1]) {
			TreasureRankAnim.SetTrigger ("Star1");
		}else if (tsc.TreasureScore < gm.TreasureRankScores[0]) {
		}
	}

    public void PlayQuestRewardAnim(string trigger)
    {
        MenuAnimator.SetTrigger(trigger);
    }

    
    
	public void UpdateStars(){
        
        if (GameDataManager.instance != null)
        {
            if (distanceUI.distance > gm.SurvialRankScores[2])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].SurvivalRank = Rank.Third;
                starsGathered += 3;
            }
            else if (distanceUI.distance > gm.SurvialRankScores[1] && distanceUI.distance < gm.SurvialRankScores[2])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].SurvivalRank = Rank.Second;
                starsGathered += 2;
            }
            else if (distanceUI.distance > gm.SurvialRankScores[0] && distanceUI.distance <= gm.SurvialRankScores[1])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].SurvivalRank = Rank.First;
                starsGathered += 1;
            }
            else if (distanceUI.distance < gm.SurvialRankScores[0])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].SurvivalRank = Rank.None;
            }


            if (tsc.TreasureScore > gm.TreasureRankScores[2])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].TreasureRank = Rank.Third;
                starsGathered += 3;
            }
            else if (tsc.TreasureScore > gm.TreasureRankScores[1] && tsc.TreasureScore < gm.TreasureRankScores[2])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].TreasureRank = Rank.Second;
                starsGathered += 2;
            }
            else if (tsc.TreasureScore > gm.TreasureRankScores[0] && tsc.TreasureScore <= gm.TreasureRankScores[1])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].TreasureRank = Rank.First;
                starsGathered += 1;
            }
            else if (tsc.TreasureScore < gm.TreasureRankScores[0])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].TreasureRank = Rank.None;
            }

            if (rrc.RoadRageCounter > gm.DestructionRankScores[2])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].DestructionRank = Rank.Third;
                starsGathered += 3;
            }
            else if (rrc.RoadRageCounter > gm.DestructionRankScores[1] && rrc.RoadRageCounter < gm.DestructionRankScores[2])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].DestructionRank = Rank.Second;
                starsGathered += 2;
            }
            else if (rrc.RoadRageCounter > gm.DestructionRankScores[0] && rrc.RoadRageCounter <= gm.DestructionRankScores[1])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].DestructionRank = Rank.First;
                starsGathered += 1;
            }
            else if (rrc.RoadRageCounter < gm.DestructionRankScores[0])
            {
                GameDataManager.instance.loadedLevelDataMap[levelIndex].DestructionRank = Rank.None;
            }
           // Debug.Log(" You gathered " + starsGathered + " stars");

            if (starsGathered >= GameDataManager.instance.loadedLevelDataMap[levelIndex].StarsGathered)
            {
                GameDataManager.instance.playerData.Stars += (starsGathered - GameDataManager.instance.loadedLevelDataMap[levelIndex].StarsGathered);
                GameDataManager.instance.loadedLevelDataMap[levelIndex].StarsGathered = starsGathered;
                GameDataManager.instance.Save();
                GameDataManager.instance.SaveLevel(levelIndex);
               // Debug.Log("Adding stars");
            }

            

        }

	}


}
