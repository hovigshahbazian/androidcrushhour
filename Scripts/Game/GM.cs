using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class GM : MonoBehaviour {

    public enum GameState { Intro, Playing, Failed, Complete }
    
    public GameState CurGameState;

   public float[] SurvialRankScores = new float[3]{ 1000f, 2000f, 3000f };
   public int[] DestructionRankScores = new int[3] { 10, 25, 50 };
    public int[] TreasureRankScores = new int[3] { 1000, 2000, 3000 };

    public bool QuestOneComplete = false;
    public bool QuestTwoComplete = false;
    public bool QuestThreeComplete = false;

   
    public string QuestOneCompleteEvent;
    public string QuestTwoCompleteEvent;
    public string QuestThreeCompleteEvent;

    public int CrushCansCollected = 0;
    public Stage LevelID = 0;


    public static bool accelerometer = true;
    public static bool vibrate = false;
 
    public int target=60;

  
   // public SM soundManager;

    private void Awake()
    {
       


    }
    void Start () {
        GameDataManager.instance.LoadLevel(LevelID);
        EventManager.StartListening("PlayerCrash", GameOver);

        EventManager.StartListening("PlayerTimeOut", GameOver);
        EventManager.StartListening("StageComplete", StageComplete);
        EventManager.StartListening("CollectCan", CollectCrushCan);
        


        if (QuestOneCompleteEvent != "")
            EventManager.StartListening(QuestOneCompleteEvent, CompleteQuestOne);

        if (QuestTwoCompleteEvent != "")
            EventManager.StartListening(QuestTwoCompleteEvent, CompleteQuestTwo);

        if (QuestThreeCompleteEvent != "")
            EventManager.StartListening(QuestThreeCompleteEvent, CompleteQuestThree);

        CurGameState = GameState.Intro;

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
       
	 
	}
    public void StartStage()
    {
        EventManager.TriggerEvent("StartLevel");

        CurGameState = GameState.Playing;
       // soundManager.PlaySound(0);
      //  soundManager.PlayDelayedLoopingSound(2, 3f);

    }

    void StageComplete()
    {
        CurGameState = GameState.Complete;
  
        GameDataManager.instance.playerData.CrushCans += CrushCansCollected;
        GameDataManager.instance.loadedLevelDataMap[LevelID].Completed = true;
        GameDataManager.instance.Save();
        GameDataManager.instance.SaveLevel(LevelID);
    }

    void GameOver()
    {
        CurGameState = GameState.Failed;
        SM.instance.PlaySound(SM.Sound.Car_Crash);    
        GameDataManager.instance.playerData.CrushCans += CrushCansCollected;
        GameDataManager.instance.Save();
    }



    public void Restart () {
        EventManager.TriggerEvent("RestartScene");
        EventManager.TriggerEvent("Unpause");
        
	}

    public void ReturnToMainMenu()
    {
        EventManager.TriggerEvent("GoToMainMenu");
        EventManager.TriggerEvent("Unpause");
    }



	public void AccOn (bool accelState) {
	        accelerometer=accelState;
            SteeringWheel.SteeringWheelDisplay(!accelState);
	}

	public void VibOn () {
		if (vibrate)
			vibrate=false;
		else {
			vibrate=true;
			//Handheld.Vibrate();
		}
	}



    void Update () {

		if (target != Application.targetFrameRate) {
			Application.targetFrameRate = target;
		}

        if (Input.GetKeyDown(KeyCode.C))
        {
            EventManager.TriggerEvent("PlayerCrash");
        }

		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit();
		}

       
    }

   public void CompleteQuestOne()
    {
        QuestOneComplete = true;
        //GameDataManager.instance.loadedLevelDataMap[LevelID].sideQuestCompleteOne = true;
    }
    public void CompleteQuestTwo()
    {
        QuestTwoComplete = true;
       // GameDataManager.instance.loadedLevelDataMap[LevelID].sideQuestCompleteTwo = true;
    }
    public void CompleteQuestThree()
    {
        QuestThreeComplete = true;
       // GameDataManager.instance.loadedLevelDataMap[LevelID].sideQuestCompleteThree = true;
    }

    public void CollectCrushCan()
    {
        CrushCansCollected++;
    }

}


