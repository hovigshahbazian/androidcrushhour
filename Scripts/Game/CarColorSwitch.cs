﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CarColor { First = 0, Second = 1, Third = 2, Fourth = 3, Fifth = 4, Sixth = 5, Seventh = 6, Eight = 7, Ninth = 8, Tenth = 9 }

public class CarColorSwitch : MonoBehaviour {

	public MeshRenderer[] carColors;

	

	public CarColor CurrentColor;

	// Use this for initialization
	void Start () {
		SwitchColor (CurrentColor);
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void SwitchColor(CarColor color){

		foreach (MeshRenderer m in carColors){
			m.enabled = false;


		}

		if ((int)(color) > carColors.Length) {
			color = (CarColor)carColors.Length;
		} 

		CurrentColor = color;
		carColors [(int)(color)].enabled = true;

	}



	public void MoveLeft(){

		if(CurrentColor > CarColor.First)
			CurrentColor--; 

		SwitchColor (CurrentColor);



	}

	public void MoveRight(){
		
		if(CurrentColor < CarColor.Tenth && (int)CurrentColor < carColors.Length-1 )
			CurrentColor++; 

		SwitchColor (CurrentColor);


	}


}
