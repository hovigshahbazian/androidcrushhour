﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelDataUI : MonoBehaviour {

	private Text text;
	//public LevelGameData[] levelData;
	public string hiscoretxt;
    // Use this for initialization
    public float levelIndex;

	void Start () {
		text = GetComponent<Text>();
		text.text = hiscoretxt;
		text.text += GameDataManager.instance.loadedLevelDataMap[Stage.HighWay_1].Score;
		text.text += "\n" + " Cars Crushed: " + GameDataManager.instance.loadedLevelDataMap[Stage.HighWay_1].RoadRageScore;
		text.text += "\n" + " Miles Traveled: " + GameDataManager.instance.loadedLevelDataMap[Stage.HighWay_1].DistanceRecord.ToString("F");
		text.text += "\n" + " Diamonds Collected: " + GameDataManager.instance.loadedLevelDataMap[Stage.HighWay_1].CrushCansCollected;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetHighscore(Stage level){
		text.text = hiscoretxt;
		text.text += GameDataManager.instance.loadedLevelDataMap[level].Score;
		text.text += "\n" + " Cars Crushed: " + GameDataManager.instance.loadedLevelDataMap[level].RoadRageScore;
		text.text += "\n" + " Miles Traveled: " + GameDataManager.instance.loadedLevelDataMap[level].DistanceRecord.ToString("F");
		text.text += "\n" + " Diamonds Collected: " + GameDataManager.instance.loadedLevelDataMap[level].CrushCansCollected;



	}
}
