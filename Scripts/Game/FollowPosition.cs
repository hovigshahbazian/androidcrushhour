using UnityEngine;
using System.Collections;

public class FollowPosition : MonoBehaviour {

	public Transform followTransform;
	public Vector3 offset;
	// Use this for initialization


	public bool ignoreX;
	public bool ignoreY;
	public bool ignoreZ;

	Vector3 followPos;
	Vector3 myPos;

	Transform myTransform;
    public bool SmoothFollow;
    public float followSpeed;
	void Start () {
		myTransform = transform;


	}

	// Update is called once per frame
	void Update () {
		

		if (ignoreX && ignoreY && ignoreZ) {
			myPos = new Vector3 (myTransform.position.x + offset.x, myTransform.position.y + offset.y, myTransform.position.z + offset.z);
		} else if (ignoreX && ignoreY) {
            myPos = new Vector3 (myTransform.position.x + offset.x, myTransform.position.y + offset.y, followTransform.position.z + offset.z);
		} else if (ignoreX && ignoreZ) {
            myPos = new Vector3 (myTransform.position.x + offset.x, followTransform.position.y + offset.y, myTransform.position.z + offset.z);
		} else if (ignoreY && ignoreZ) {
            myPos = new Vector3 (followTransform.position.x + offset.x, myTransform.position.y + offset.y, myTransform.position.z + offset.z);
		} else if (ignoreX) {
            myPos = new Vector3 (myTransform.position.x + offset.x, followTransform.position.y + offset.y, followTransform.position.z + offset.z);
		} else if (ignoreY) {
            myPos = new Vector3 (followTransform.position.x + offset.x, myTransform.position.y + offset.y, followTransform.position.z + offset.z);
		} else if (ignoreZ) {
            myPos = new Vector3 (followTransform.position.x + offset.x, followTransform.position.y + offset.y, myTransform.position.z + offset.z);
		} else {
            myPos  = new Vector3 (followTransform.position.x + offset.x, followTransform.position.y + offset.y, followTransform.position.z + offset.z);
		}

        if (SmoothFollow)
        {
            //  myTransform.position = Vector3.Lerp(myTransform.position, myPos,Time.deltaTime*followSpeed);

            myTransform.position = Vector3.Lerp(myTransform.position, myPos, Time.deltaTime * followSpeed);

        }
        else
        {
            myTransform.position = myPos;
        }


    }
}