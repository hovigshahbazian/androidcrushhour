﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaneSwitch : MonoBehaviour
{
    public GameObject Lane;
    private BezierSpline spline;
    [Range(0,1)]
    public float ProgressPoint = 0;

    public bool Activated = false;
    public bool Repeatable = false;
    public bool SwitchAllCars = false;
    public bool AllowObstacleVehices = false;
  void Start()
    {
        spline = Lane.GetComponent<BezierSpline>();
    }

    public void OnTriggerEnter(Collider other)
    {
        
       
        if (other.CompareTag("Player") ){

           
            PlayerCarController player = other.GetComponentInParent<PlayerCarController>();
            if (player != null)
            {
                if (!Activated || Repeatable)
                {
                    other.GetComponentInParent<PlayerCarController>().walker.SetSpline(spline, ProgressPoint) ;
                    if (SwitchAllCars)
                    {
                        EventManager.TriggerEvent("SwitchLane");
                    }
                    Activated = true;
                }
            }
        }
        
        if (other.CompareTag("Obstacle") && AllowObstacleVehices)
        {
            VehicleObstacle player = other.GetComponentInParent<VehicleObstacle>();
            if (player != null)
            {
                if (!Activated || Repeatable)
                {
                    other.GetComponentInParent<VehicleObstacle>().walker.SetSpline(spline, ProgressPoint);
                    Activated = true;
                }
            }
        }


    }
}
