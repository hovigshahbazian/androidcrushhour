using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GM_menu : MonoBehaviour {


	public string MainSceneName;
	public string SettingsSceneName;

	public Animation clip;
	public float startSecs;
	float transitionTimer;
	bool startTimer = false;
	AsyncOperation async;

	void Awake () {
	}
 
	void Start () {

		transitionTimer = startSecs;

	}

	void Update(){

		if (startTimer) {
			transitionTimer -= Time.deltaTime;


			if (transitionTimer <= 0) {

				async.allowSceneActivation = true;
			}
		}
	}


	public void LoadSettingsScene () {
		SceneManager.LoadScene (SettingsSceneName);

	}
		


	public IEnumerator LoadCarSelectAsync(){
		if(clip)
			clip.Play ();

		startTimer = true;
		async = SceneManager.LoadSceneAsync ("CarSelect");
		async.allowSceneActivation = false;
		yield return async;
	
	}

	public IEnumerator LoadMainMenuAsync(){
		if(clip)
			clip.Play ();

		startTimer = true;
		async = SceneManager.LoadSceneAsync ("Main Menu");
		async.allowSceneActivation = false;
		yield return async;

	}


}




