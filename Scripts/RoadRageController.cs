﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadRageController : MonoBehaviour {


    public bool Active;
    public AudioSource RoadRageSound;
    public Animator RoadRageCollectAnim;
    public GameObject RoadRageFire;

    public Animator RoadRageHitAnim;
    public Animation RoadRageCarScoreAnim;

    public float RoadRageTimer;
    public float RoadRageCounter;
    public static float CurrentDestructionRating;

    private float RoadRageHitTimer = 0;
    public float RoadRageHitTime = 0.5f;
    public bool RoadRageHitAble = true;
    public float RoadRageMultipler = 1;
    public int RoadRageCarValue = 5;
    public int RoadRageObstacleValue = 1;
    public AudioClip RoadRageActivateSD;
    public AudioClip RoadRageHitSD;
   

    // Use this for initialization
    void Start () {  

        EventManager.StartListening("RoadRageHit", RoadRageHit);
        EventManager.StartListening("RoadObstacleCrashed", RoadRageObstacleHit);
        EventManager.StartListening("StopCurrentPowerUp", StopRoadRage);
        EventManager.StartListening("RoadRageGrab", StartRoadRage);
        EventManager.StartListening("RoadRageEnd", StopRoadRage);



        RoadRageCounter = 0;
        CurrentDestructionRating = 1;
        RoadRageSound.volume = SM.audioVolume;

    }
	
	// Update is called once per frame
	void Update () {

        

        if(RoadRageHitTimer > 0)
        {
            RoadRageHitTimer -= Time.deltaTime;

            if(RoadRageHitTimer < 0)
            {
                RoadRageHitTimer = 0;
                RoadRageHitAble = true;
            }
        }

    }



    public void RoadRageHit()
    {
        if (RoadRageHitAble)
        {
            RoadRageHitAnim.SetTrigger("RoadRageHit");
            PlayRoadRageHitSound();
            RoadRageCounter += 5;//(RoadRageCarValue * RoadRageMultipler);
            RoadRageHitTimer = RoadRageHitTime;
            RoadRageHitAble = false;
        }
    }

    public void RoadRageObstacleHit()
    {
        if (RoadRageHitAble)
        {
            RoadRageHitAnim.SetTrigger("RoadRageHit");
            PlayRoadRageHitSound();
            RoadRageCounter += 1;// (RoadRageObstacleValue * RoadRageMultipler);
            RoadRageHitTimer = RoadRageHitTime;
            RoadRageHitAble = false;
        }
    }

    public void PlayRoadRageActivateSound()
    {
        
        RoadRageSound.PlayOneShot(RoadRageActivateSD);
    }

    public void StopRoadRageActiveSound()
    {
        RoadRageSound.Stop();
    }

     public void PlayRoadRageHitSound()
    {
        
        RoadRageSound.PlayOneShot(RoadRageHitSD);
    }

    public void StartRoadRage()
    {
        if (!Active)
        {
            //Debug.Log("Starting RoadRage");
            GetComponentInParent<PlayerCarController>().ResetPowerState();
            GetComponentInParent<PlayerCarController>().CurrentPowerUp = PlayerCarController.PowerUps.RoadRage;
            RoadRageFire.SetActive(true);
            Active = true;
            PlayRoadRageActivateSound();
        }
       
         
    }

    public void StopRoadRage()
    {
        if (Active)
        {
           // Debug.Log("Ending RoadRage");
            GetComponentInParent<PlayerCarController>().CurrentPowerUp = PlayerCarController.PowerUps.None;
            RoadRageFire.SetActive(false);
            StopRoadRageActiveSound();
            Active = false;
        }
    }

}
