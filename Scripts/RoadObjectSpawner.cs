﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadObjectSpawner : MonoBehaviour {

    public SplineWalker walker;
    private ObjectPool pool;
    public float SpawnTime = 4;
    public float SpawnTimer = 4;
    public float SpawnDistance = 0.001f;

    public int MaxX = 5;
    public int MinX = -5;

    // Use this for initialization
    void Start () {
        pool = GetComponent<ObjectPool>();
        EventManager.StartListening("StartLevel", StartSpawning);


	}
	
	// Update is called once per frame
	void Update () {
		
        if(SpawnTimer > 0)
        {
            SpawnTimer -= Time.deltaTime;

            if(SpawnTimer < 0)
            {
                SpawnTimer = SpawnTime;
                SpawnRoadObject();
              //  Debug.Log("SpawnObject");
            }



        }

	}


    public void SpawnRoadObject()
    {
        GameObject obj = pool.GetPooledObject();
        obj.SetActive(true);

        obj.transform.position = walker.currentSpline.GetPoint(walker.progress + SpawnDistance) + walker.transform.right * Random.Range(MinX,MaxX) + Vector3.up ;
      
    }

    public void StartSpawning()
    {
        SpawnTimer = SpawnTime;
    }

}
