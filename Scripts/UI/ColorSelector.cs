﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorSelector : MonoBehaviour {

	public CarColorSwitch[] Cars;
	public Button LeftSelectBtn;
	public Button RightSelectBtn;
	public Text text;


	public CarType currentCar;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		text.text = " Color " + (((int)Cars [(int)currentCar].CurrentColor) + 1 ).ToString () ;
	}
		
	public void SetUpColorSelectors(){
		LeftSelectBtn.onClick.RemoveAllListeners ();
		RightSelectBtn.onClick.RemoveAllListeners ();

	
		LeftSelectBtn.onClick.AddListener (() => Cars [(int)currentCar].MoveLeft());
		RightSelectBtn.onClick.AddListener (() => Cars [(int)currentCar].MoveRight());

	}





}
