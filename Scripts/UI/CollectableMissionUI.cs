﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectableMissionUI : MonoBehaviour {


    public int crushCansCollected = 0;
    public string CollectEvent;
    public Text text;
    public string CollectableName = "Crush Can";
	// Use this for initialization
	void Start () {
        if(CollectEvent!=null)
        EventManager.StartListening(CollectEvent, GainCrushCan);

        text.text = CollectableName + ":" + crushCansCollected.ToString("D2");
    }
	


    public void GainCrushCan()
    {
        crushCansCollected++;
        text.text = CollectableName + ":" +  crushCansCollected.ToString("D2");
    }
}
