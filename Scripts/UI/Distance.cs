using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Distance : MonoBehaviour {

    [HideInInspector]
    public SplineWalker walker;
    public float distance;
    private Text distanceText;
    public bool Active;
	// Use this for initialization
	void Start () {
        distanceText = GetComponent<Text>();

        walker = GameObject.FindGameObjectWithTag("PlayerLaneWalker").GetComponent<SplineWalker>();

        EventManager.StartListening("StartLevel",StartDistance);
        EventManager.StartListening("PlayerCrash", EndDistance);
        EventManager.StartListening("StageComplete", EndDistance);
    }

	void Update () {


        if (Active)
        {
            distance = walker.totalDistanceTraveled;

            distanceText.text =  distance.ToString("0");
        }
	}


    public void StartDistance()
    {
        Active = true;
    }


    public void EndDistance()
    {
        Active = false;
    }
}
