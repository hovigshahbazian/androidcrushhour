﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CenterScaling : MonoBehaviour {


	float initialScale;
	ScrollRect scroll;
	int numOfItems;

    float[] itemCenters;
	float scale;
	// Use this for initialization
	void Start () {
		scroll = GetComponent<ScrollRect> ();
		numOfItems = scroll.content.childCount;
		itemCenters = new float[numOfItems];

		float divider = 1.0f / (numOfItems - 1);

		itemCenters [0] = 0.0f;


		for (int i = 1; i < numOfItems; i++) {

			itemCenters [i] += itemCenters [i - 1] + divider;
		}

	
	
	

	}

	// Update is called once per frame
	void Update () {
		for (int i = 0; i <numOfItems; i++) {
			scale =  2 - (Mathf.Abs( scroll.normalizedPosition.x - itemCenters[i])*2);
			scroll.content.GetChild (i).localScale = new Vector3 (scale, scale,scale);
		}

	}
}
