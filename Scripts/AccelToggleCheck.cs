﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccelToggleCheck : MonoBehaviour {

    Toggle toggle;
    private GM gm;
	// Use this for initialization
	void Start () {


        gm = GameObject.Find("GM").GetComponent<GM>();
        toggle = GetComponent<Toggle>();
        toggle.isOn = GM.accelerometer;

        toggle.onValueChanged.AddListener(gm.AccOn);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
