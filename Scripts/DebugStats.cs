﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugStats : MonoBehaviour {

    //Declare these in your class
    int m_frameCounter = 0;
    float m_timeCounter = 0.0f;
    float m_lastFramerate = 0.0f;
    public float m_refreshTime = 0.5f;


    GUIStyle style = new GUIStyle();

	// Use this for initialization
	void Start () {
        style.fontSize = 50;
        style.normal.textColor = Color.red ;
	}
	
	// Update is called once per frame
	void Update () {
        if (m_timeCounter < m_refreshTime)
        {
            m_timeCounter += Time.deltaTime;
            m_frameCounter++;
        }
        else
        {
            //This code will break if you set your m_refreshTime to 0, which makes no sense.
            m_lastFramerate = (float)m_frameCounter / m_timeCounter;
            m_frameCounter = 0;
            m_timeCounter = 0.0f;
        }
    }

    void OnGUI()
    {
        GUI.Label(new Rect(30, 30, 300, 300), (m_lastFramerate).ToString("F0"), style);
      //  GUI.Label(new Rect(130, 30, 300, 300), "Accel: " + Input.acceleration.x.ToString(), style);
      //  GUI.Label(new Rect(330, 30, 300, 300), "Steer: " + SteeringWheel.input, style);
    }


}
