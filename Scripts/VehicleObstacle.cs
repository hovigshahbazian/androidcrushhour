﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AIState { Regular, Ambulance, Racer };

public class VehicleObstacle : MonoBehaviour {


    public SplineWalker walker;
    public float RelativeX;
    public float curDistfromWalker;
    public float startingDistFromWalker = 0.05f;
    public float Speed;
    public bool Following = true;
    public AIState curAI = AIState.Regular;
    public float pullOverSpeed;
    public float maxMovementX = 5;
    public float minMovementX = -5;
    public float Accelleration = 0f;
    public volatile bool Bumped = false;
    private float bumpTimer = 0.1f;
    public float ContactTimer = 0.5f;
    public bool SlowdownOnBump = false;
    public bool BumpOffCrashToogle = false;

    public bool DespawnOutOfView = true;
    public bool CrashAble = false;
    private VehicleHealth vehicleHelth;
    public bool isBoss = false;
	// Use this for initialization
	void Start () {
        vehicleHelth = GetComponent<VehicleHealth>();
        EventManager.StartListening("AmbulanceGrab", EnterAmbulanceAI);
        EventManager.StartListening("AmbulanceEnd", EnterRegularAI);
        EventManager.StartListening("BossCrash", Stop);
        EventManager.StartListening("StageComplete", DisableCollision);

	}
	void OnEnable()
    {
        Following = true;
        curDistfromWalker = startingDistFromWalker;
        //GetComponent<Rigidbody>().isKinematic = true;
    }

	// Update is called once per frame
	void LateUpdate () {

        Speed += Accelleration;

        if (Following)
        {
            curDistfromWalker += Speed * Time.deltaTime; ;

            Vector3 position = walker.currentSpline.GetPoint(walker.progress + curDistfromWalker);
            transform.localPosition = position;
            transform.LookAt(walker.currentSpline.GetPoint(walker.progress + curDistfromWalker) + walker.currentSpline.GetDirection(walker.progress + curDistfromWalker));


           
            RelativeX = Mathf.Clamp(RelativeX += (pullOverSpeed * Time.deltaTime), minMovementX, maxMovementX);

            transform.position = walker.currentSpline.GetPoint(walker.progress + curDistfromWalker) + (walker.transform.right * RelativeX);

        }

        if(walker.progress >= 1)
        {
            Stop();
            Despawn();
        }


        Vector3 screenPosition = Camera.main.WorldToScreenPoint(transform.position);

        
        if (screenPosition.z < 0 && DespawnOutOfView)
        {
            Despawn();

        }

        

    }
    

    void OnCollisionEnter(Collision col)
    {
        Debug.Log(col.gameObject.name + " has collided");

        if (vehicleHelth != null)
        {
            Debug.Log(col.gameObject.tag);
            if (col.gameObject.CompareTag("PlayerInfo"))
            {
                vehicleHelth.Damage();
                if(gameObject.tag == "Boss")
                {
                    EventManager.TriggerEvent("BossDamaged");
                }
            }

          
        }
        if (col.gameObject.CompareTag("Racer"))
        {
            Vector3 contact = transform.InverseTransformPoint(col.contacts[0].point);
            CarBumpContact(contact, 0.006f, 10f);
        }


    }


    void Despawn()
    {
        gameObject.SetActive(false);
        walker.gameObject.SetActive(false);
        Accelleration = 0f;
        pullOverSpeed = 0f;
   // Debug.Log("Despawn");
    }

    public void EnterAmbulanceAI()
    {
        curAI = AIState.Ambulance;
        pullOverSpeed = 1f;
    }

    public void EnterRegularAI()
    {
        curAI = AIState.Regular;
        pullOverSpeed = 0.00f;
    }

 

    public void CarBumpContact(Vector3 contact,float forwardDist, float HorizontalDist)
    {
        if (contact.z > 1)
        {
            CarBumpForward(forwardDist);
        }

        if (contact.z < -1)
        {
            CarBumpBackwards(forwardDist);
        }
        //Debug.Log(contact.x);
        if (contact.x > 1)
        {
            CarBumpRightward(HorizontalDist);
            if(RelativeX == maxMovementX && BumpOffCrashToogle)
            {
                Crash();
            }
        }

        if (contact.x < -1)
        {
            CarBumpLeftward(HorizontalDist);
            if (RelativeX == minMovementX && BumpOffCrashToogle)
            {
                Crash();
            }
        }
    }




    public void CarBumpForward(float dist) {
        StartCoroutine(CarBumpFor(dist));
    }

    public void CarBumpBackwards()
    {
        StartCoroutine(CarBumpBack(0.012f));
    }

    public void CarBumpBackwards(float dist)
    {
        StartCoroutine(CarBumpBack(dist));
    }

    public void CarBumpLeftward(float dist)
    {
        StartCoroutine(CarBumpLeft(dist));
    }
    public void CarBumpRightward(float dist)
    {
        StartCoroutine(CarBumpRight(dist));
    }

    IEnumerator CarBumpFor(float forwardBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;
            float oldSpeed = Speed;
            Debug.Log(" Forward Bumped");
            Accelleration = forwardBumpChange;
            yield return new WaitForSeconds(bumpTimer);
          //  Accelleration = -forwardBumpChange;
            //yield return new WaitForSeconds(bumpTimer);

            Accelleration = 0f;
            Speed = oldSpeed;
            Bumped = false;
        }
    }

    IEnumerator CarBumpBack(float forwardBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;
            float oldSpeed = Speed;
            Debug.Log(" backward Bumped");
            Accelleration = -forwardBumpChange;
            yield return new WaitForSeconds(bumpTimer);
           // Accelleration = forwardBumpChange;
          //  yield return new WaitForSeconds(bumpTimer);

            Accelleration = 0f;
            Speed = oldSpeed;
            Bumped = false;
        }
    }

    IEnumerator CarBumpRight(float sideBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;
          //  Debug.Log("right: " + sideBumpChange);
            pullOverSpeed += sideBumpChange;
          //  Debug.Log("right:" + pullOverSpeed);
            yield return new WaitForSeconds(bumpTimer);
            pullOverSpeed += -sideBumpChange;

            Bumped = false;
        }
    }


    IEnumerator CarBumpLeft(float sideBumpChange)
    {
        if (!Bumped)
        {
            Bumped = true;

            pullOverSpeed += -sideBumpChange;
          //  Debug.Log("left:" + pullOverSpeed);
            yield return new WaitForSeconds(bumpTimer);
            pullOverSpeed += sideBumpChange;
            Bumped = false;
        }
    }


    public void Stop()
    {
        Speed = 0;
        Accelleration = 0;
    }

    public void DisableCollision()
    {
        Collider col = GetComponentInChildren<Collider>();
       if( col != null)
        {
            col.enabled = false;
        }
    }

    public void SlowDown()
    {

        StartCoroutine(ApplySlowDown());
    }

    public void SlowDown(float speed, float time)
    {
        StartCoroutine(ApplySlowDown(speed,time));
    }

    public void SpeedUp(float speed, float time)
    {
        StartCoroutine(ApplySpeedUp(speed, time));
    }



    IEnumerator ApplySlowDown()
    {
        SlowDown(1f);
    
        yield return new WaitForSeconds(5);

        SpeedUp(1f);
    }

    IEnumerator ApplySlowDown(float speed, float time)
    {
        SlowDown(speed);

        yield return new WaitForSeconds(time);

        SpeedUp(speed);
    }


    IEnumerator ApplySpeedUp(float speed, float time)
    {
        
        SpeedUp(speed);
        yield return new WaitForSeconds(time);
        SlowDown(speed);

    }


    private void SlowDown(float velocity)
    {

        walker.curVelocity -= velocity;
        walker.MaximumVelocity -= velocity;
    }

    private void SpeedUp(float velocity)
    {

        walker.curVelocity += velocity;
        walker.MaximumVelocity += velocity;
    }


    

    public void Crash()
    {
        if (CrashAble)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.forward * 500000);
            Following = false;
        }
       
    }
    public void Crash(Vector3 crashVector)
    {
        if (CrashAble )
        {
            GetComponent<Rigidbody>().AddForce(crashVector * 500000);
            Following = false;
        }
    }
}
