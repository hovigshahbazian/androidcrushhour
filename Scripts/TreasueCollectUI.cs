﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreasueCollectUI : MonoBehaviour {

    public EventAnimation eventAnim;
    public Text text;

    // Use this for initialization
    void Start () {
        eventAnim.StartListening();

    }


    public void SetText(float TreasureValue)
    {
        text.text = "+$" + TreasureValue;
    }
	
	


}
