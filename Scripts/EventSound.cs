﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSound : MonoBehaviour {

    public string EventName;
    public AudioSource sound;

	// Use this for initialization
	void Start () {

        EventManager.StartListening(EventName, PlaySound);

	}
	
	
    void PlaySound()
    {
        Debug.Log("Playsound" + sound);
        sound.volume = SM.audioVolume;
        sound.Play();
    }

}
