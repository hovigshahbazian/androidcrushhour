﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SteeringWheelSenseSlider : MonoBehaviour {


    private Slider slider;
    private float SensitivityValue;

	// Use this for initialization
	void Start () {      
        slider = GetComponent<Slider>();
        SensitivityValue = SteeringWheel.SteeringWheelSensitivity / 50f;
        slider.value = SensitivityValue;
        slider.onValueChanged.AddListener(SetSensitivityValue);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    static public void SaveData()
    {
        PlayerPrefs.SetFloat("SteerSense", SteeringWheel.SteeringWheelSensitivity);
    }


    static public void LoadData()
    {
        SteeringWheel.SteeringWheelSensitivity =  PlayerPrefs.GetFloat("SteerSense");

    }

    public void SetSensitivityValue(float val)
    {
        SteeringWheel.SteeringWheelSensitivity = (val * 50f) + 1;
        Debug.Log("Sensitivity set" + SteeringWheel.SteeringWheelSensitivity);
        SaveData();
    }


}
