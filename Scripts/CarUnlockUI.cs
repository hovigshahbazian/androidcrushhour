﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CarUnlockUI : MonoBehaviour {


    public Image backPanel;
    public Text requirementText;
    public Image crushCan;
    public int RequiredCans;
    public bool Unlocked;
	// Use this for initialization
	void Start () {

        requirementText.text = "You need " + RequiredCans + " Crush Cans to Unlock";
        /*
        if (Unlocked)
        {
            Disable();
        }*/
     
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void Disable()
    {
        Debug.Log("Ui Disabled");
        backPanel.enabled = false;
        requirementText.enabled = false;
        crushCan.enabled = false;

    }
    public void Enable()
    {
        backPanel.enabled = true;
        requirementText.enabled = true;
        crushCan.enabled = true;

    }
}
