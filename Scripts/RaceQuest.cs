﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceQuest : MonoBehaviour {

    public string RaceCompleteEvent;
    public RacerLocation racerLocation;
    public int PlayerFinalRanking;

	// Use this for initialization
	void Start () {
        EventManager.StartListening(RaceCompleteEvent, ShowPlayerRanking);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowPlayerRanking()
    {
        PlayerFinalRanking = racerLocation.currentRank;

        if (PlayerFinalRanking == 1)
        {
            EventManager.TriggerEvent("FirstPlace");
        }
    }



}
