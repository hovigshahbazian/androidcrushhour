﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowDownPowerUp : MonoBehaviour {


    public float SlowDownValue;
    public float SlowTime = 5;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(PickUp(other));
        }
    }



    IEnumerator PickUp(Collider player)
    {
        PlayerCarController playerController = player.GetComponentInParent<PlayerCarController>();
        if (playerController != null)
        {

            //GetComponent<Renderer>().enabled = false;
           // GetComponent<Collider>().enabled = false;

            EventManager.TriggerEvent("SlowGrab");
            playerController.CurrentPowerUp = PlayerCarController.PowerUps.Slow;

            yield return new WaitForSeconds(SlowTime);
            EventManager.TriggerEvent("SlowEnd");
            playerController.CurrentPowerUp = PlayerCarController.PowerUps.None;
        }
    }

    public void SlowDown(SplineWalker walker)
    {

        walker.curVelocity -= SlowDownValue;
        walker.MaximumVelocity -= SlowDownValue;
    }


    public void Revert(SplineWalker walker)
    {
       // walker.curVelocity += SlowDownValue;
        walker.MaximumVelocity += SlowDownValue;
    }



}
