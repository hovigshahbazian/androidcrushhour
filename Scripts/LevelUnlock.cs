﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUnlock : MonoBehaviour {

	public enum UnlockRequirements { Score, CrushedCars, Diamonds};

	private Button button;
	public Text UnlockText;
    //public LevelGameData preLevelData;
    public Stage preLevelIndex;
	public int ScoreUnlock;
	private int score;
	public UnlockRequirements unlockType;


	// Use this for initialization
	void Start () {

		button = GetComponent<Button>();


		switch (unlockType) {
		case UnlockRequirements.Score:
			score = GameDataManager.instance.loadedLevelDataMap[preLevelIndex].Score;
			break;
		case UnlockRequirements.Diamonds:
			score = GameDataManager.instance.loadedLevelDataMap[preLevelIndex].RoadRageScore;
			break;
		case UnlockRequirements.CrushedCars:
			score = GameDataManager.instance.loadedLevelDataMap[preLevelIndex].CrushCansCollected;
			break;
		}
			
		if(score >= ScoreUnlock){
			button.interactable = true;
			UnlockText.enabled = false;

		}
		else{
			button.interactable = false;
			UnlockText.enabled = true;

		}


	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
