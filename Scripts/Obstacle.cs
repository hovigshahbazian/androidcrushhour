﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

    public bool isTrigger = true;
    public bool OnlyOnce = false;
	// Use this for initialization
	void Start () {
  
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void OnTriggerEnter(Collider col)
    {
        if (isTrigger)
        {
            if (col.CompareTag("Player"))
            {
                // Debug.Log("Fish Hit");
                PlayerCarController controller = col.GetComponentInParent<PlayerCarController>();
                if (controller != null)
                {
                    if (controller.CollisionEventsEnabled)
                    {
                        col.transform.parent.SendMessage("Damage");
                    }
                }
            }

            if (col.CompareTag("Racer"))
            {
                col.transform.parent.SendMessage("SlowDown");
            }
        }
    }
    public void OnCollisionEnter(Collision col)
    {


       // Debug.Log("Collided with: " + col.transform.name);
        if (!isTrigger)
        {

            {
                // Debug.Log("Fish Hit");
                PlayerCarController controller = col.transform.GetComponentInParent<PlayerCarController>();
                if (controller != null)
                {
                    if (controller.CollisionEventsEnabled)
                    {
                        controller.GetComponent<PlayerHealthController>().Damage() ;
                    }
                }
            }

            if (col.transform.CompareTag("Racer"))
            {
                col.transform.parent.SendMessage("SlowDown");
            }

        }
    }
}
