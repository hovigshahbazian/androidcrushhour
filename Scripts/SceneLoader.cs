﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class SceneLoader : MonoBehaviour {


    public Animator clip;
    public Slider loadingBar;
    public Text LoadingText;

    // Use this for initialization
    void Start () {

        EventManager.StartListening("RestartScene", RestartScene);

        EventManager.StartListening("GoToMainMenu", GotoMenu);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void RestartScene()
    {
        StartCoroutine(LoadStageAsync(SceneManager.GetActiveScene().name));
    }

    public void GotoMenu()
    {
        StartCoroutine(LoadMainMenuAsync());
    }

    public void LoadStageScene(string str)
    {
        StartCoroutine(LoadStageAsync(str));
    }

    public void LoadSelectCar()
    {
        StartCoroutine("LoadCarSelectAsync");
    }

    public IEnumerator LoadStageAsync(string stageName)
    {
        clip.SetTrigger("Loading");
        loadingBar.gameObject.SetActive(true);

        AsyncOperation async = SceneManager.LoadSceneAsync(stageName);

        while (!async.isDone)
        {
            float progress = Mathf.Clamp01(async.progress / 0.9f);
            loadingBar.value = progress;
            LoadingText.text = (progress * 100f).ToString("F0") + "%";
            yield return null;
        }
    }

    public IEnumerator LoadMainMenuAsync()
    {
        clip.SetTrigger("Loading");
        loadingBar.gameObject.SetActive(true);
        AsyncOperation async = SceneManager.LoadSceneAsync("Main Menu");
        while (!async.isDone)
        {
            float progress = Mathf.Clamp01(async.progress / 0.9f);
            loadingBar.value = progress;
            LoadingText.text = ((int)progress * 100).ToString("F0") + "%";
            yield return null;

        }
    }

    public IEnumerator LoadCarSelectAsync()
    {
        clip.SetTrigger("Loading");
        loadingBar.gameObject.SetActive(true);
        AsyncOperation async = SceneManager.LoadSceneAsync("CarSelect");
        while (!async.isDone)
        {
            float progress = Mathf.Clamp01(async.progress / 0.9f);
            loadingBar.value = progress;
            LoadingText.text = ((int)progress * 100).ToString("F0") + "%";
            yield return null;

        }
    }


}
