﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SideQuestRewardUI : MonoBehaviour {

    public enum QuestNumber { One, Two, Three}

    public bool RewardGranted = false;
    public bool QuestComplete = false;
    public int rewardamount = 5;
    public Image crushCanImage;
    public Text rewardAmountText;
    public Text QuestStateText;
    private GM gameMananger;
    public QuestNumber quest;

	// Use this for initialization
	void Start () {
        gameMananger = GameObject.Find("GM").GetComponent<GM>();

        if(quest == QuestNumber.One)
        {
            QuestComplete = gameMananger.QuestOneComplete;
        }
        else if(quest == QuestNumber.Two)
        {
            QuestComplete = gameMananger.QuestTwoComplete;
        }
        else if( quest == QuestNumber.Three)
        {
            QuestComplete = gameMananger.QuestThreeComplete;
        }
       

        if (QuestComplete)
        {
            QuestStateText.text = "Complete";
            crushCanImage.enabled = true;
            rewardAmountText.enabled = true;
            rewardAmountText.text =  "+" + rewardamount;
            GameDataManager.instance.playerData.CrushCans += 5;
            GameDataManager.instance.Save();
        }
        else
        {
            QuestStateText.text = "Incomplete";
            crushCanImage.enabled = false;
            rewardAmountText.enabled = false;
        }

}
	
	// Update is called once per frame
	void Update () {
		
	}
}
