﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPowerUp : MonoBehaviour
{
    public float SpeedUpValue;
    public float SpeedTime = 5;
    public Renderer render;
    public Collider col;
    public AudioSource revSound;

    void OnEnable()
    {
        render.enabled = true;
        col.enabled = true;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.tag);
        if(other.CompareTag("Player"))
        {
           // Debug.Log("Speed Up");
            StartCoroutine(PickUp(other));

        }
    }


    IEnumerator PickUp(Collider player)
    {
        PlayerCarController playerController = player.GetComponentInParent<PlayerCarController>();
        if (playerController != null)
        {
            revSound.Play();
            render.enabled = false;
            col.enabled = false;
            EventManager.TriggerEvent("SpeedUpGrab");
            SpeedUp(playerController.walker);
            playerController.CurrentPowerUp = PlayerCarController.PowerUps.Speed;

            yield return new WaitForSeconds(SpeedTime);
            revSound.Stop();
            EventManager.TriggerEvent("SpeedUpEnd");
            Revert(playerController.walker);
            playerController.CurrentPowerUp = PlayerCarController.PowerUps.None;
        }
    }

    public void SpeedUp(SplineWalker walker)
    {
        walker.curVelocity += SpeedUpValue;
       // walker.Accleration += 0.018f;
        walker.MaximumVelocity += SpeedUpValue;

       
    }


    public void Revert(SplineWalker walker)
    {
       walker.curVelocity -= SpeedUpValue;
       // walker.Accleration -= 0.018f;
        walker.MaximumVelocity -= SpeedUpValue;
       
      
    }
}
