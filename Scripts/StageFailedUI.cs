﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StageFailedUI : MonoBehaviour {

    public GameObject SFUI;
    [SerializeField]
    private GM GameMananger;
    public Button RestartButton;
    public Button MainMenuButton;

    // Use this for initialization
    void Start () {
        EventManager.StartListening("PlayerCrash", ShowFailScreen);
        GameMananger = GameObject.Find("GM").GetComponent<GM>();

        if (RestartButton != null)
        {
            RestartButton.onClick.AddListener(GameMananger.Restart);
            RestartButton.onClick.AddListener(PlayButtonSound);
        }

        if (MainMenuButton != null)
        {
            MainMenuButton.onClick.AddListener(GameMananger.ReturnToMainMenu);
            MainMenuButton.onClick.AddListener(PlayButtonSound);
        }

    }

    // Update is called once per frame
    public void PlayButtonSound()
    {
        SM.instance.PlaySound(SM.Sound.Menu_Select);
    }

    public void ShowFailScreen()
    {
        StartCoroutine(ShowEndScreen());
    }

    IEnumerator ShowEndScreen()
    {
        yield return new WaitForSeconds(1);
        SFUI.SetActive(true);
    }
}
