﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestEventCounterUI : MonoBehaviour {

    public SideQuestEvent questEvent;
    private Text counterText;
    private int eventC;
    private int eventG;
	// Use this for initialization
	void Start () {
        counterText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

        eventC = questEvent.EventCounter;
        eventG = questEvent.EventGoal;

        counterText.text = eventC + "/" + eventG + " " + questEvent.QuestName;


	}



}
