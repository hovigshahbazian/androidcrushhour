﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadRageUI : MonoBehaviour {


    public EventAnimation[] EventAnims;

	// Use this for initialization
	void Start () {
		foreach( EventAnimation e in EventAnims)
        {
            e.StartListening();
        }
	}
	
	
}
